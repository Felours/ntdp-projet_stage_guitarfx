/***************** Server for audio project's application *****************/
// Autors : Michel Buffa (core), Nouriel Azria (changments for the project)
// Description : Back-end of the audio context application (pedalboard-MT5)
// Version : 1.0.0
/**************************************************************************/

// Select port from console
var prompt = require('prompt');

// Define properties
var properties = [
{
  name: 'port', 
  validator: /^[0-9\s\-]+$/
}
];

// Start prompt
prompt.start();

// Get choice from user and start the server
prompt.get(properties, function (err, result) {
	if (err) { return onErr(err); }

	// We need to use the express framework: have a real web server that knows how to send mime types etc.  
	var express=require('express');  
	  
	// Init globals variables for each module required  
	var app = express()  
	  , http = require('http')  
	  , server = http.createServer(app);
	  //, io = require('socket.io').listen(server);  
	  
	// Indicate where static files are located    
	app.use(express.static(__dirname + './../'));    
	  
	// launch the http server on given port  
	if(result.port === undefined || result.port == ''){
		server.listen(8080);  
		console.log('Port selected: ' + 8080);
	}
	else{
		server.listen(result.port);
		console.log('Port selected: ' + result.port);
	}
	  
	// routing  
	app.get('/', function (req, res) {
	  res.sendfile(__dirname + '/index.html');  
	});

});

// The error prompt function
function onErr(err) {
	console.log(err);
	return 1;
}  
  
// Events of the server (in case something should be implemented)
/*io.sockets.on('connection', function (socket) {  
  
});  */