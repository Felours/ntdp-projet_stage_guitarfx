# A pedal board for guitar players with graph based interface, built upon the Web Audio API. # 
Plug your guitar on your computer, open the WebApp and the guitar sound will be processed in real time.

We use it regularly with a Mac Book Pro + an apogee jam sound card plugged on the USB port. A demo will be soon available online, but installing it is an easy process.

This project has been inspired by [the Guitar FX Chrome Application](https://chrome.google.com/webstore/detail/guitarfx/nbjaofogegcpkeobcnpdkkpdioogeajj), that is not open source. So, we decided to write a similar one from scratch, using Web Audio and the JsPlumb JavaScript library.

![GTV1: main windows picture](http://i.imgur.com/gwbVXZR.jpg)
![GTV1: Bank Manager picture](http://i.imgur.com/RuGrrCu.jpg)

Features
--------

- A Graph based GUI: create a new preset, rename it, use the drop down menu to add effects to the preset, link effects together.

- So far, these effects have been implemented: overdrive, distorsion, gain/volume, delay, reverb, autoWah, graphic equalizer, chorus, and an amp simulator. The amp sim is  based on a cross compiled version of [the MDA Combo amp simulator](http://sourceforge.net/projects/mda-vst/) (originally written in C++, MDA combo is an open source VST plugin. It has been cross compiled and adapted in JavaScript / scriptProcessor node [using Jari Kleimola tools](http://www.researchgate.net/publication/282157532_DAW_Plugins_for_Web_Browsers)). Other effects will be added regularly.

- Click on a on effect to show its dashboard at the bottom of the page, with the different controllers (knobs, switches, presets)

- Double click on an effect to remove it, double click on a link to remove it.

- Bank manager. A bank is a set of presets. You can create/delete/rename banks from the Bank Manager window. You can copy presets from one bank to another using drag'n'drop. 

- Automatic save of the application state/presets/parameters each time you change something. Save is done in an IndexedDB datastore, client-side.

Server setup
-----------

To run the server, you will need nodeJS and some node modules. Just run `npm install` from the 'server' folder to download the modules, then run `node server.js`. You can edit the file in order to change the port (8080 by default)

Client setup
-----------

Just open http://localhost:8080 or change the port to the one you set in server.js

Limitations
-----------
So far there is some latency, and the sound of some effects can be greatly improved. We're working on it.

Other resources
-----------

* UML diagrams : https://drive.google.com/open?id=0B3WFp-IrSi9fcUFOQXg5dTVUZXM&authuser=0