
/* Class PedalGate */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalGate(id){

    // Heritage
    Pedal.call(this, "Gate");
    this.setNomClass("PedalGate");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    var pedalGate;
    try{
        pedalGate = new Gate(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalGate);

    // Creer le potentiometre 'Release'
    var param = new PotentiometreRelease();
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Threshold'
    param = new PotentiometreThreshold();
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

     try{
        param = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        param = new PotentiometreBoutonEtat();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalGate.prototype = new Pedal();
//PedalGate.prototype.restoreAudioPedal = restoreAudioPedalGate;