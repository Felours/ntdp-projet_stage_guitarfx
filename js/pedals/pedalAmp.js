
/* Class PedalAmp */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalAmp(id){

    // Heritage
    Pedal.call(this, "Amp");
    this.setNomClass("PedalAmp");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    var pedalAmp;
    try{
        pedalAmp = new Amp(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalAmp);
    var param;
    //ajouter potentiometreDrive
    try{
        param = new PotentiometreDrive(pedalAmp.getParametterDrive());
    }
    catch(err){
        param = new PotentiometreDrive();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Bias'
    try{
        param = new PotentiometreBias(pedalAmp.getParametterBias());
    }
    catch(err){
        param = new PotentiometreBias();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Mid'

    try{
        param = new PotentiometreProcess(pedalAmp.getParametterProcess());
    }
    catch(err){
        param = new PotentiometreProcess();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'HPFFreq'

    try{
        param = new PotentiometreHPFFreq(pedalAmp.getParametterHPFFreq());
    }
    catch(err){
        param = new PotentiometreHPFFreq();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param)

    // Creer le potentiometre 'HPFReso'

     try{
        param = new PotentiometreHPFReso(pedalAmp.getParametterHPFReso());
    }
    catch(err){
        param = new PotentiometreHPFReso();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param)

    // Creer le potentiometre 'Boost'
     try{
        param = new PotentiometreMaster(pedalAmp.getParametterMaster());
    }
    catch(err){
        param = new PotentiometreMaster();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param)


    // Creer le potentiometre 'Type'

    try{
        param = new PotentiometreType(pedalAmp.getParametterType());
    }
    catch(err){
        param = new PotentiometreType();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    try{
        param = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        param = new PotentiometreBoutonEtat();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalAmp.prototype = new Pedal();
//PedalAmp.prototype.restoreAudioPedal = restoreAudioPedalAmp;
