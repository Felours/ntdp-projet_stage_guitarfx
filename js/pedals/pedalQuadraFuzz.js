

/* Class pedalQuadraFuzz */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalQuadraFuzz(id){

    // Heritage
    Pedal.call(this, "QuadraFuzz");
    this.setNomClass("PedalQuadraFuzz");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    var pedalQuadraFuzz;
    try{
        pedalQuadraFuzz = new QuadraFuzz(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalQuadraFuzz);

    // Creer le potentiometre 'Low'
    var potentiometer;
    try{
        potentiometer = new PotentiometreFreq(pedalQuadraFuzz.getParametterLow(),"Low");
    }
    catch(err){
        potentiometer = new PotentiometreFreq();
    }
    potentiometer.modifierGPId(gbpid + potentiometer.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(potentiometer);


    // Creer le potentiometre 'Mid1'

    try{
        potentiometer = new PotentiometreFreq(pedalQuadraFuzz.getParametterMid1(),"Mid1");
    }
    catch(err){
        potentiometer = new PotentiometreFreq();
    }
    potentiometer.modifierGPId(gbpid + potentiometer.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(potentiometer);

    // Creer le potentiometre 'Mid2'

    try{
        potentiometer = new PotentiometreFreq(pedalQuadraFuzz.getParametterMid2(),"Mid2");
    }
    catch(err){
        potentiometer = new PotentiometreFreq();
    }
    potentiometer.modifierGPId(gbpid + potentiometer.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(potentiometer);

    // Creer le potentiometre 'High'

    try{
        potentiometer = new PotentiometreFreq(pedalQuadraFuzz.getParametterHigh(),"High");
    }
    catch(err){
        potentiometer = new PotentiometreFreq();
    }
    potentiometer.modifierGPId(gbpid + potentiometer.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(potentiometer);

    // Creer le potentiometre 'Drive'

    try{
        potentiometer = new PotentiometreDrive(pedalQuadraFuzz.getParametterDrive());
    }
    catch(err){
        potentiometer = new PotentiometreDrive();
    }
    potentiometer.modifierGPId(gbpid + potentiometer.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(potentiometer);

    // Creer le potentiometre 'Output'

    try{
        potentiometer = new PotentiometreVolume(pedalQuadraFuzz.getParametterOutput());
    }
    catch(err){
        potentiometer = new PotentiometreVolume();
    }
    potentiometer.modifierGPId(gbpid + potentiometer.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(potentiometer);
    

    try{
        potentiometer = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        potentiometer = new PotentiometreBoutonEtat();
    }
    potentiometer.modifierGPId(gbpid + potentiometer.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(potentiometer);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalQuadraFuzz.prototype = new Pedal();
//PedalOverdrive.prototype.restoreAudioPedal = restoreAudioPedalOverdrive;
