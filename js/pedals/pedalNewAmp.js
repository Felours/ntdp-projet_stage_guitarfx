
/* Class PedalAmp */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalNewAmp(id){

    // Heritage
    Pedal.call(this, "NewAmp");
    this.setNomClass("PedalNewAmp");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    var pedalNewAmp = new NewAmp(audioCtx);


     //ajouter potentiometreVolume
        param = new PotentiometreVolume(pedalNewAmp.getParametterVolume());
        pedalNewAmp.addPotentiometre("volume", param);

    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Stocker l'audio
    this.setAudioPedal(pedalNewAmp);
    var param;

    //ajouter potentiometreMaster
   
        param = new PotentiometreMaster(pedalNewAmp.getParametterMaster());
                pedalNewAmp.addPotentiometre("master", param);

  
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

     //ajouter potentiometreDrive
        param = new PotentiometreDrive(pedalNewAmp.getParametterDrive());
        pedalNewAmp.addPotentiometre("Drive", param);
   
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    //ajouter potentiometreBass
        param = new PotentiometreBass(pedalNewAmp.getParametterBass());
        pedalNewAmp.addPotentiometre("Bass", param);

   
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

     //ajouter potentiometreMid
        param = new PotentiometreMid(pedalNewAmp.getParametterMiddle());
        pedalNewAmp.addPotentiometre("Middle", param);
   
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    //ajouter PotentiometreTreb
        param = new PotentiometreTreb(pedalNewAmp.getParametterTreble());
        pedalNewAmp.addPotentiometre("Treble", param);
   
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);


//ajouter potentiometreReverb
        param = new PotentiometreReverb(pedalNewAmp.getParametterReverb());
        pedalNewAmp.addPotentiometre("Reverb", param);
   
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);



    //ajouter potentiometrePresence
        param = new PotentiometrePresence(pedalNewAmp.getParametterPresence());
        pedalNewAmp.addPotentiometre("Presence", param);

    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Preset' définit le type d amplis

        param = new PotentiometrePreset(pedalNewAmp.getParametterPreset());
  
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);
    // A COMPLERER POUR TOUS LES POTENTIOMETRES !!!

     // Ajouter le potentiometre

   
        param = new PotentiometreBoutonEtat(false);
    
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();
}
PedalNewAmp.prototype = new Pedal();
//PedalNewAmp.prototype.restoreAudioPedal = restoreAudioPedalAmp;
