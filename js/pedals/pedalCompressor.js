

/* Class PedalCompressor */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalCompressor(id){

    // Heritage
    Pedal.call(this, "Compressor");
    this.setNomClass("PedalCompressor");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    // Fournir l'audio
    var pedalCompressor;
    try{
        pedalCompressor = new Compressor(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalCompressor);

    // Creer le potentiometre 'Threshold'
    var param;
    try{
        param = new PotentiometreThreshold(pedalCompressor.getParametterThreshold());
    }
    catch(err){
        param = new PotentiometreThreshold();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Knee'
    try{
        param = new PotentiometreKnee(pedalCompressor.getParametterKnee());
    }
    catch(err){
        param = new PotentiometreKnee();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Ratio'
    try{
        param = new PotentiometreRatio(pedalCompressor.getParametterRatio());
    }
    catch(err){
        param = new PotentiometreRatio();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

     // Creer le potentiometre 'Reduction'
    try{
        param = new PotentiometreReduction(pedalCompressor.getParametterReduction());
    }
    catch(err){
        param = new PotentiometreReduction();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

 // Creer le potentiometre 'Attack'
    try{
        param = new PotentiometreAttack(pedalCompressor.getParametterAttack());
    }
    catch(err){
        param = new PotentiometreAttack();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);


 // Creer le potentiometre 'Release'
    try{
        param = new PotentiometreRelease(pedalCompressor.getParametterRelease());
    }
    catch(err){
        param = new PotentiometreRelease();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);


    // Ajouter le potentiometre
    try{
        param = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        param = new PotentiometreBoutonEtat();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalCompressor.prototype = new Pedal();
