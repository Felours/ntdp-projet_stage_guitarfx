

/* Class PedalCabinet */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalCabinet(id){

    // Heritage
    Pedal.call(this, "Cabinet");
    this.setNomClass("PedalCabinet");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    var pedalCabinet;
    try{
        pedalCabinet = new Cabinet(audioCtx);
    }
    catch(err){console.log(err.toString());}


    this.setAudioPedal(pedalCabinet);

    // Creer le potentiometre 'Room'
    var param = new PotentiometreRoom();
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Volume'
    param = new PotentiometreVolume(1);
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    try{
        param = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        param = new PotentiometreBoutonEtat();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);
    
    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalCabinet.prototype = new Pedal();
//PedalCabinet.prototype.restoreAudioPedal = restoreAudioPedalCabinet;