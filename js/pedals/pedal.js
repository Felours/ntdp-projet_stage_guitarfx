
/* Class BasePedal */
/* Description : Class representant les informations primaires d'une pedal */
/* Arguments : type - le nom d'une pedal */
function BasePedal(type){

    // Implementer Serialize
    Serialize.call(this,"BasePedal");

    // --- Attributs
    //
    this.m_type = type;	// Type de pedal (nom)
    var m_audioPedal;	// Instance de la classe de noeuds d'audio

    // --- Methodes
    //

    // --- Methode getType
    //
    this.getType = function(){
        return(this.m_type);
    };

    // --- Methode setType
    //
    this.setType = function(type){
        this.m_type = type;
    };

    // --- Methode getAudioPedal
    //
    this.getAudioPedal = function(){
        return m_audioPedal;
    };

    // --- Methode setAudioPedal
    //
    this.setAudioPedal = function(ap){
        m_audioPedal = ap;
    };

}
BasePedal.prototype = new Serialize();

/* Class PedalDebut */
/* Description : Class representant le pedal de debut. Permet egalement d'ajouter une gestion audio */
/* Arguments : type - le nom d'une pedal */
function PedalDebut(type){

    // Heritage
    BasePedal.call(this, type);
    this.setNomClass("PedalDebut");

    // Ajout de l'input
    this.setAudioPedal(input);


    // --- Methodes
    //

    // --- Methode recupererFluxAudio
    // --- Description : Permet de recuperer le flux audio associe au pedal (input)
    //
    this.recupererFluxAudio = function(){

        // Recuperer l'instance de la classe audio
        var audioPedal = this.getAudioPedal();
        if(audioPedal === undefined)
            audioPedal = input;
        if(audioPedal === undefined)
            return -1;

        // Renvoyer le flux
        return audioPedal;

    };

    // --- Methode connecterAudio
    // --- Description : Permet de lier le flux audio du pedal au flux audio du pedal donnee
    // --- Argument : pedal - Instance du pedal vers lequel on souhaite lier le flux audio
    //
    this.connecterAudio = function(pedal){

        // Verifier si l'argument est valide
        if(pedal === undefined)
            return -1;
        if(!(pedal instanceof BasePedal))
            return -1;

        // Recuperer le flux audio du pedal courant
        var FACourant = this.recupererFluxAudio();

        // Recuperer le flux audio du pedal parametre
        var FAPedalParametre = pedal.recupererFluxAudio();

        // Verifier si les flux audio sont valides
        if(FACourant === undefined || FACourant == -1)
            return -1;
        if(FAPedalParametre === undefined || FAPedalParametre == -1)
            return -1;

        // Connecter le flux courant a celui donnee en parametre selon le type du pedal
        if(pedal instanceof Pedal){

            // Tenter de recuperer le flux input
            var inputPedalParametre = FAPedalParametre.getInput();

            // Verifier si l'input est valide
            if(inputPedalParametre === undefined)
                return -1;

            FACourant.connect(inputPedalParametre);

        }
        else
            FACourant.connect(FAPedalParametre);

    };

    // --- Methode deconnecterAudio
    // --- Description : Permet de separer le flux audio du pedal du flux audio du pedal donnee
    // --- Argument : pedal - Instance du pedal de laquel on souhaite separer le flux audio
    //
    this.deconnecterAudio = function(pedal){

        // Verifier si l'argument est valide
        if(pedal === undefined)
            return -1;
        if(!(pedal instanceof BasePedal))
            return -1;

        // Recuperer le flux audio du pedal courant
        var FACourant = this.recupererFluxAudio();

        // Recuperer le flux audio du pedal parametre
        var FAPedalParametre = pedal.recupererFluxAudio();

        // Verifier si les flux audio sont valides
        if(FACourant === undefined || FACourant == -1)
            return -1;
        if(FAPedalParametre === undefined || FAPedalParametre == -1)
            return -1;

        // Connecter le flux courant a celui donnee en parametre selon le type du pedal
        if(pedal instanceof Pedal){

            // Tenter de recuperer le flux input
            var inputPedalParametre = FAPedalParametre.getInput();

            // Verifier si l'input est valide
            if(inputPedalParametre === undefined)
                return -1;

            // Deconnecter le noeud audio
            try{
                FACourant.disconnect(inputPedalParametre);
            }
            catch(err){ console.log(err.toString()); }

        }
        else
        // Deconnecter le noeud audio
            try{
                FACourant.disconnect(FAPedalParametre);
            }
            catch(err){ console.log(err.toString()); }

    };

}
PedalDebut.prototype = new BasePedal();

/* Class PedalFin */
/* Description : Class representant le pedal de fin. Permet egalement d'ajouter une gestion audio */
/* Arguments : type - le nom d'une pedal */
function PedalFin(type){

    // Heritage
    BasePedal.call(this, type);
    this.setNomClass("PedalFin");

    // Ajout de l'output
    this.setAudioPedal(output);


    // --- Methodes
    //

    // --- Methode recupererFluxAudio
    // --- Description : Permet de recuperer le flux audio associe au pedal (output)
    //
    this.recupererFluxAudio = function(){

        // Recuperer l'instance de la classe audio
        var audioPedal = this.getAudioPedal();
        if(audioPedal === undefined)
            audioPedal = output;
        if(audioPedal === undefined)
            return -1;

        // Renvoyer le flux
        return audioPedal;

    };

}
PedalFin.prototype = new BasePedal();


/* Class Pedal */
/* Description : Class representant les informations d'une pedal reelle */
/* Arguments : type - le nom d'une pedal */
function Pedal(type){

    // Heritage
    BasePedal.call(this, type);
    this.setNomClass("Pedal");

    // --- Attributs
    //
    this.m_actif = true;	// Pedal actif ou non (TODO : activer ou desactiver le pedal par l'interface)
    this.m_potentiometres = []; // Liste des potentiometres du pedal

    // --- Methodes
    //

    // --- Methode getActivation
    //
    this.getActivation = function(){
        return(this.m_actif);
    };

    // --- Methode getPotentiometres
    //
    this.getPotentiometres = function(){
        return(this.m_potentiometres);
    };

    // --- Methode changerActivation
    //
    this.changerActivation = function(){

        if(this.m_actif)
            this.m_actif = false;
        else
            this.m_actif = true;

    };

    // --- Methode ajouterPotentiometre
    //
    this.ajouterPotentiometre = function(potentiometre){
        this.m_potentiometres.push(potentiometre);
    };

    // --- Methode retirerPotentiometre
    //
    this.retirerPotentiometre = function(potentiometre){

        // Trouver l'indice du potentiometre
        var index = this.m_potentiometres.indexOf(potentiometre);

        // Si trouve, retirer de la liste
        if (index > -1) {
            this.m_potentiometres.splice(index, 1);
        }
    };

    // --- Methode lierAncrePedalPotentiometres
    // --- Description : Permet de fournir aux potentiometres le lien montant vers l'instance du pedal dans le but de pouvoir interagir avec l'audio (de la classe de Maxime)
    //
    this.lierAncrePedalPotentiometres = function(){

        // Recuperer la liste des potentiometres
        var listePotentiometres = this.m_potentiometres;

        // Iterer sur chaque potentiometre du pedal
        for(var i=0; i<listePotentiometres.length; i++)
            // Fournir au potentiometre le lien montant vers l'instance
            try{
                listePotentiometres[i].lierAncrePedalPotentiometre(this);
            }
            catch(err){ console.log(err.toString); }

    };

    // --- Methode recupererFluxAudio
    // --- Description : Permet de recuperer le flux audio associe au pedal (de la classe de Maxime)
    //
    this.recupererFluxAudio = function(){

        // Recuperer l'instance de la classe audio
        var audioPedal = this.getAudioPedal();
        if(audioPedal === undefined)
            return -1;

        // Renvoyer le flux
        return audioPedal;

    };

    // --- Methode connecterAudio
    // --- Description : Permet de lier le flux audio du pedal au flux audio du pedal donnee
    // --- Argument : pedal - Instance du pedal vers lequel on souhaite lier le flux audio
    //
    this.connecterAudio = function(pedal){

        // Verifier si l'argument est valide
        if(pedal === undefined)
            return -1;
        if(!(pedal instanceof BasePedal))
            return -1;

        // Recuperer le flux audio du pedal courant
        //var FACourant = this.recupererFluxAudio().getOutput();
        var FACourant = undefined;
        try{
            FACourant = this.recupererFluxAudio().getOutput();
        }
        catch(err){
            //console.log(err.toString());
        }

        // Recuperer le flux audio du pedal parametre
        var FAPedalParametre = pedal.recupererFluxAudio();

        // Verifier si les flux audio sont valides
        if(FACourant === undefined || FACourant == -1)
            return -1;
        if(FAPedalParametre === undefined || FAPedalParametre == -1)
            return -1;

        // Connecter le flux courant a celui donnee en parametre selon le type du pedal
        if(pedal instanceof Pedal){

            // Tenter de recuperer le flux input
            var inputPedalParametre = FAPedalParametre.getInput();

            // Verifier si l'input est valide
            if(inputPedalParametre === undefined)
                return -1;

            FACourant.connect(inputPedalParametre);

        }
        else
            FACourant.connect(FAPedalParametre);

    };

    // --- Methode deconnecterAudio
    // --- Description : Permet de separer le flux audio du pedal du flux audio du pedal donnee
    // --- Argument : pedal - Instance du pedal de laquel on souhaite separer le flux audio
    //
    this.deconnecterAudio = function(pedal){

        // Verifier si l'argument est valide
        if(pedal === undefined)
            return -1;
        if(!(pedal instanceof BasePedal))
            return -1;

        // Recuperer le flux audio du pedal courant
        var FACourant = this.recupererFluxAudio().getOutput();

        // Recuperer le flux audio du pedal parametre
        var FAPedalParametre = pedal.recupererFluxAudio();

        // Verifier si les flux audio sont valides
        if(FACourant === undefined || FACourant == -1)
            return -1;
        if(FAPedalParametre === undefined || FAPedalParametre == -1)
            return -1;

        // Deonnecter le flux courant de celui donnee en parametre selon le type du pedal
        if(pedal instanceof Pedal){

            // Tenter de recuperer le flux input
            var inputPedalParametre = FAPedalParametre.getInput();

            // Verifier si l'input est valide
            if(inputPedalParametre === undefined)
                return -1;

            // Deconnecter le noeud audio
            try{
                FACourant.disconnect(inputPedalParametre);
            }
            catch(err){ 
                console.log(err.toString()); 
            }

        }
        else{
            // Deconnecter le noeud audio
            try{
                FACourant.disconnect(FAPedalParametre);
            }
            catch(err){ 
                // MB: for the pedal at the end of the chain there is no
                // "next" pedal, so do not display the error message.
                if(pedal.m_nomClass !== "PedalFin")
                    console.log(err.toString()); 
            }
        }

    };

    // --- Methode restoreAudioPedal
    // --- Description : Methode permettant de restaurer l'etat de l'instance de l'objet audio pedal selon les valeurs des potentiometres
    //
    this.restoreAudioPedal = function(){

        // Recuperer les potentiometres
        var v_potentiometres = this.m_potentiometres;

        // Restaurer l'etat de l'audio selon chaque valeur des potentiometres
        var v_potentiometre, v_gPotentiometre, v_valeur;
        for(var i=0; i<v_potentiometres.length; i++){

            // Recuperer le potentiometre
            v_potentiometre = v_potentiometres[i];

            // Recuperer le gPotentiometre associee
            v_gPotentiometre = v_potentiometre.getGPotentiometre();

            // Recuperer la valeur (stockee) du gPotentiometre associe
            v_valeur = v_gPotentiometre.getValeurReelle();

            // Verifier si la valeur est correcte
            if(v_valeur !== undefined)
                v_potentiometre.setValueAudio(v_valeur);

        }

    };

}
Pedal.prototype = new BasePedal();
