
/* Class PedalChorus */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalChorus(id){

    // Heritage
    Pedal.call(this, "Chorus");
    this.setNomClass("PedalChorus");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    var pedalChorus;
    try{
        pedalChorus = new Chorus(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalChorus);

    // Creer le potentiometre 'FeedBack'
    var param;
    try{
        param = new PotentiometreFeedBack(pedalChorus.getParametterFeedback());
    }
    catch(err){
        param = new PotentiometreFeedBack();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);


    // Creer le potentiometre 'Rate'
    var param;
    try{
        param = new PotentiometreRate(pedalChorus.getParametterRate());
    }
    catch(err){
        param = new PotentiometreRate();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Delay'
    try{
        param = new PotentiometreTime(pedalChorus.getParametterDelay());
    }
    catch(err){
        param = new PotentiometreTime();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Depth'
    try{
        param = new PotentiometreDepth(pedalChorus.getParametterDepth());
    }
    catch(err){
        param = new PotentiometreDepth();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

     // Creer le potentiometre 'Type'

    try{
        param = new PotentiometreType(pedalChorus.getParametterType());
    }
    catch(err){
        param = new PotentiometreType();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Ajouter le Bouton
    param = new PotentiometreBoutonEtat(false);
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();


}
PedalChorus.prototype = new Pedal();
//PedalChorus.prototype.restoreAudioPedal = restoreAudioPedalChorus;