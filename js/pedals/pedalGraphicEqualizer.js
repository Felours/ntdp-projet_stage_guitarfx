
/* Class PedalGraphicEQ */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalGraphicEQ(id){

    // Heritage
    Pedal.call(this, "GraphicEQ");
    this.setNomClass("PedalGraphicEQ");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    var allFilters = [];
    var param;

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    var pedalGraphicEq;
    try{
        pedalGraphicEq = new GraphicEQ(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalGraphicEq);

    allFilters = pedalGraphicEq.getFilters();

    for (var i = 0; i <= allFilters.length - 1; i++) {
         // Creer le potentiometre 'Filter'
        try{
            param = new PotentiometreEQ(pedalGraphicEq.getParametterFilter(),pedalGraphicEq.getValFilter(i));
        }
        catch(err){
            param = new PotentiometreEQ();
        }
        param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

        // Ajouter le potentiometre
        this.ajouterPotentiometre(param);
   
    };

    try{
        param = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        param = new PotentiometreBoutonEtat();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalGraphicEQ.prototype = new Pedal();
//PedalGraphicEQ.prototype.restoreAudioPedal = restoreAudioPedalGraphicEQ;
