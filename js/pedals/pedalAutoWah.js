

/* Class PedalWah */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalWah(id){ /* TODODO */
// Heritage
    Pedal.call(this, "Wah");
    this.setNomClass("PedalWah");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    // Fournir l'audio
    var pedalWah;
    try{
        pedalWah = new AutoWah(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalWah);

    // Creer le potentiometre 'FeedBack'
    var param;
    try{
        param = new PotentiometreVolume(pedalWah.getParametterVolume());
    }
    catch(err){
        param = new PotentiometreVolume();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Filter'
    try{
        param = new PotentiometreFrequency(pedalWah.getParametterFrequency());
    }
    catch(err){
        param = new PotentiometreFrequency();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Time'
    try{
        param = new PotentiometreQ(pedalWah.getParametterQ());
    }
    catch(err){
        param = new PotentiometreQ();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Ajouter le Bouton
    param = new PotentiometreBoutonEtat(false);
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalWah.prototype = new Pedal();
//PedalWah.prototype.restoreAudioPedal = restoreAudioPedalWah;
