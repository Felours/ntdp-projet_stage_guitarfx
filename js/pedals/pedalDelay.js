

/* Class PedalDelay */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalDelay(id){

    // Heritage
    Pedal.call(this, "Delay");
    this.setNomClass("PedalDelay");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    // Fournir l'audio
    var pedalDelay;
    try{
        pedalDelay = new Delay(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalDelay);

    // Creer le potentiometre 'FeedBack'
    var param;
    try{
        param = new PotentiometreFeedBack(pedalDelay.getParametterFeedback());
    }
    catch(err){
        param = new PotentiometreFeedBack();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Filter'
    try{
        param = new PotentiometreFilter(pedalDelay.getParametterFilter());
    }
    catch(err){
        param = new PotentiometreFilter();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Time'
    try{
        param = new PotentiometreTime(pedalDelay.getParametterDelay());
    }
    catch(err){
        param = new PotentiometreTime();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Volume'
    try{
        param = new PotentiometreVolume(pedalDelay.getParametterVolume());
    }
    catch(err){
        param = new PotentiometreVolume();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Ajouter le potentiometre
    try{
        param = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        param = new PotentiometreBoutonEtat();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalDelay.prototype = new Pedal();
//PedalDelay.prototype.restoreAudioPedal = restoreAudioPedalDelay;
