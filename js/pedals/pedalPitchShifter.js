
/* Class PedalPitch */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalPitch(id){

    // Heritage
    Pedal.call(this, "Pitch");
    this.setNomClass("PedalPitch");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/


    var pedalPitch;
    try{
        pedalPitch = new Pitch(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalPitch);

    // Creer le potentiometre 'Gain'
    var param = new PotentiometreGain(0, 1, 1, 0.01);
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Pitch'
    param = new PotentiometrePitch();
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Mode'
    var vals = ['Octave UP', 'Octave Down'];
    param = new PotentiometreMode(vals);
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Type'
    vals = ['Whammy'];
    param = new PotentiometreType(vals);
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);
    
    try{
        param = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        param = new PotentiometreBoutonEtat();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalPitch.prototype = new Pedal();
//PedalPitch.prototype.restoreAudioPedal = restoreAudioPedalPitch;
