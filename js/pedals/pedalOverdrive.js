

/* Class PedalOverdrive */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalOverdrive(id){

    // Heritage
    Pedal.call(this, "Overdrive");
    this.setNomClass("PedalOverdrive");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    var pedalOverdrive;
    try{
        pedalOverdrive = new Overdrive(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalOverdrive);

    // Creer le potentiometre 'Tone'
    var param;
    try{
        param = new PotentiometreTone(pedalOverdrive.getParametterTone());
    }
    catch(err){
        param = new PotentiometreTone();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Drive'
    try{
        param = new PotentiometreDrive(pedalOverdrive.getParametterDrive());
    }
    catch(err){
        param = new PotentiometreDrive();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Volume'
    try{
        param = new PotentiometreVolume(pedalOverdrive.getParametterVolume());
    }
    catch(err){
        param = new PotentiometreVolume();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre
    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Type'
    /*var vals = ['Vintage', 'Modern'];
    param = new PotentiometreType(vals);
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);*/

    try{
        param = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        param = new PotentiometreBoutonEtat();
    }
    param.modifierGPId(gbpid + param.getNom()); // Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalOverdrive.prototype = new Pedal();
//PedalOverdrive.prototype.restoreAudioPedal = restoreAudioPedalOverdrive;
