

/* Class PedalDistorsion */
/* Description : Class fille de Pedal permettant de fournir la structure interne du pedal (les potentiometres associes) */
/* Arguments : id - l'id de la structure contenant le pedal (pour creer l'id de chacun des potentiometres internes)  */
function PedalRat(id){

    // Heritage
    Pedal.call(this, "Distorsion Rat");
    this.setNomClass("PedalRat");

    // Recuperer l'id du GBasePedal (pour creer l'id des GPotentiometre)
    var gbpid = id + '-';

    /******* Ajout des potentiometres internes *******/
    /*************************************************/

    var pedalRat;
    try{
        pedalRat = new Rat(audioCtx);
    }
    catch(err){console.log(err.toString());}

    // Stocker l'audio
    this.setAudioPedal(pedalRat);

    // Creer le potentiometre 'FeedBack'
    var param;
    try{
        param = new PotentiometreTone(pedalRat.getParametterTone());
    }
    catch(err){
        param = new PotentiometreTone();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Filter'
    try{
        param = new PotentiometreDrive(pedalRat.getParametterDrive());
    }
    catch(err){
        param = new PotentiometreDrive();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Creer le potentiometre 'Time'
    try{
        param = new PotentiometreVolume(pedalRat.getParametterVolume());
    }
    catch(err){
        param = new PotentiometreVolume();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Ajouter le potentiometre
    try{
        param = new PotentiometreBoutonEtat(false);
    }
    catch(err){
        param = new PotentiometreBoutonEtat();
    }
    param.modifierGPId(gbpid + param.getNom());	// Renseigner l'id du Graphique du potentiometre

    // Ajouter le potentiometre
    this.ajouterPotentiometre(param);

    // Lier les GPotentiometres au pedal (pour pouvoir interagir avec l'audio de la classe de Maxime)
    this.lierAncrePedalPotentiometres();

}
PedalRat.prototype = new Pedal();
//PedalRat.prototype.restoreAudioPedal = restoreAudioPedalDistorsion;
