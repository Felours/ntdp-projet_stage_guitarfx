/* Classes GBasePedal */
/* =================== */

/* Class Preset */
/* Description : Class regroupant un ensemble de GPedals */
/* Argument : id - id du preset,
 idCategorie - id de la categorie dont est issu le preset */
function Preset(id, idCategorie){

    // Implementer Serialize
    Serialize.call(this,"Preset");

    // --- Attributs
    //
    this.m_id = id;	// L'id (unique) d'un preset
    this.m_idCategorie = idCategorie;	// L'id de la categorie dont est issu le preset
    this.m_nom = "Default empty preset";	// Le nom d'un preset (sera affiche)

    this.m_gPedals = [];	// Tableau contenant les instances des m_gPedals a afficher

    // --- Methodes
    //

    // --- Methode getId
    //
    this.getId = function(){
        return(this.m_id);
    };

    // --- Methode getIdCategorie
    //
    this.getIdCategorie = function(){
        return(this.m_idCategorie);
    };

    // --- Methode getNom
    //
    this.getNom = function(){
        return(this.m_nom);
    };

    // --- Methode setNom
    //
    this.setNom = function(nom){
        this.m_nom = nom;
    };

    // --- Methode getGPedals
    //
    this.getGPedals = function(){
        return(this.m_gPedals);
    };

    // --- Methode setGPedals
    //
    this.setGPedals = function(gPedals){
        this.m_gPedals = gPedals;
    };

    // --- Methode creerGBasePedal
    // --- Description : Methode permettant de creer une structure contenant un GBasePedal et le pedal associe (avec tous les traitements)
    // --- Arguments : type - Le nom du type de pedal a creer
    // --- Return : gBasePedal - une instance de la gBasePedal creee
    //
    this.creerGBasePedal = function(type){

        // Recuperer un id non existant et minimal
        var id = getIdLibre(this.m_gPedals, '-', this.getIdCategorie()+'-'+this.getId());

        // Creer une div d'affichage
        var divPedal = $("<div></div>").attr('class','divPedal');
        divPedal.attr('id', id);

        // Creer le pedal et le graphique associe selon le type, tout en indiquant le CSS associee
        var pedal, gPedal;

        switch(type){

            // --- S'il s'agit d'un pedal de debut
            case nomPDeb :
                pedal = new PedalDebut(type);
                divPedal.addClass('divPedalDeb');
                gPedal = new GPedalDebut(divPedal.get()[0], pedal, this);
                break;

            // --- S'il s'agit d'un pedal de fin
            case nomPFin :
                pedal = new PedalFin(type);
                divPedal.addClass('divPedalFin');
                gPedal = new GPedalFin(divPedal.get()[0], pedal, this);
                break;

            // --- S'il s'agit d'un pedal quelconque
            default :
                pedal =	this.definirPedal(type, id);	// Creer le pedal selon le type souhaite (permet de creer la structure des potentiometres de maniere adequate)
                divPedal.addClass('divPedalNormal');
                gPedal = new GPedal(divPedal.get()[0], pedal, this);

        }

        // Renseigner l'id au gBasePedal
        gPedal.setId(id);

        // Construire l'image de la div
        var infoImg = constructImgPedal(type, divPedal, gPedal,
            function() {

                // --- jsPlumb ---
                // ---------------
                jspInstance.ready(function() {

                    // Rendre le graphique draggable uniquement dans le conteneur
                    jspInstance.draggable($(".divPedal"), {
                        // Le conteneur
                        containment:conteneurPedals
                    });

                });
                // --- /jsPlumb ---
                // ----------------

                // Ajouter les endpoints au gPedal
                ajouterEndPoints(gPedal);

            }
        );

        // Ajouter la div dans l'affichage
        divPedal.appendTo(conteneurPedals);

        // Indiquer la position initiale du gBasePedal
        // var top = divPedal.get()[0].getBoundingClientRect().top;
        // var left = divPedal.get()[0].getBoundingClientRect().left;

        var top = divPedal.get()[0].top;
        var left = divPedal.get()[0].left;

        // var top = divPedal.outerHeight();
        // var left = divPedal.outerWidth();

        //var sectionPedalsHauteur = sectionPedals.height();//sectionPedals.get()[0].scrollHeight;
        //var sectionPedalsLargeur = sectionPedals.width();//sectionPedals.get()[0].scrollWidth;
        //.outerHeight() .outerWidth();

        gPedal.setPosition(left, top);

        // Ajouter le pedal a la liste
        presetCourant.getGPedals().push(gPedal);

        // Renseigner le pedal courant
        if(type != nomPDeb || type != nomPFin)
            gPedalCourant = gPedal;

        // Indiquer l'ajout du pedal
        console.log("Pedal " + type + " cree");

        // Retourner le gPedal cree
        return gPedal;

    };

    // --- Methode definirPedal
    // --- Description : Cree et renvois l'instance d'un pedal selon le type souhaite
    // --- Arguments : type - type de pedal souhaite, id - l'id servant a la creation des ids des potentiometres internes
    // --- Retour : Instance pedal
    //
    this.definirPedal = function(type, id){

        // Initialiser le nom de la classe a appeler
        var nomClasseCreationPedal = "Pedal";

        // Verifier si la classe associe a la creation existe
        if (typeof window[nomClasseCreationPedal + type] == 'function')
        // Construire le nom de la classe a appeler selon le type souhaite
            nomClasseCreationPedal = nomClasseCreationPedal + type;

        // Indiquer console
        console.log("Appel de la classe " + nomClasseCreationPedal);

        // Appeler la classe du pedal souhaite et le renvoyer
        return new window[nomClasseCreationPedal](id);

    };

    // --- Methode pedalExiste
    // --- Description : Indique si un pedal existe ou pas (selon la div)
    // -- Return : boolean
    //
    this.pedalExiste = function(div){

        // Recuperer le GBasePedal
        var gPedal = this.getGPedalFromDiv(div);

        // Verifier si le gPedal existe
        if(gPedal != -1){

            // Trouver l'indice du gPedal dans le tableau
            var index = this.getGPedals().indexOf(gPedal);

            // Si trouve, indiquer qu'il existe
            if (index > -1)
                return true;
        }

        // Indiquer qu'il n'existe pas
        return false;
    };

    // --- Methode getGPedalFromDiv
    // --- Description : Recuperer le GPedal correspondant au div donne en potentiometre
    // --- Return : Instance GBPedal
    //
    this.getGPedalFromDiv = function(div){

        // Scanner les GPedals existants
        var GPB, GBdiv;
        for(var i=0; i<this.getGPedals().length; i++){

            // Recuperer le GPedal
            GPB = this.getGPedals()[i];

            // Recuperer la div du GPedal
            GBdiv = GPB.getDiv();

            // Verifier si la div correspond
            if(GBdiv === div){
                return GPB;
            }
        }

        // Renvoyer une erreur
        return -1;

    };

    // --- Methode getGPedalFromId()
    // --- Description : Recupere un GBasePedal par son id
    // --- Return : Instance GBPedal
    //
    this.getGPedalFromId = function(id){

        // Scanner les GPedals existants
        var GP, GPId;
        for(var i=0; i<this.getGPedals().length; i++){

            // Recuperer le GPedal
            GP = this.getGPedals()[i];

            // Recuperer l'id du GPedal
            GPId = GP.getId();

            // Verifier si la div correspond
            if(GPId === id){
                return GP;
            }
        }

        // Renvoyer une erreur
        return -1;

    };


    // --- Methode retirerGBB
    // Description : Retire le GBasePedal graphiquement (et effectivement du preset)
    //
    this.retirerGBB = function(div){

        // Recuperer le GBasePedal
        var gbp = this.getGPedalFromDiv(div);

        // Retirer les predecesseurs (mutuellement)
        var predecs = gbp.getPredecesseurs();	// Liste des ids
        var gbpPre;
        for(var i=0; i<predecs.length; i++){

            // Recuperer le gBasePedal par son id
            gbpPre = this.getGPedalFromId(predecs[i]);

            // Verifier si le GBasePedal existe
            if(gbpPre !== -1){

                // Retirer le gPedal du predecesseur
                gbpPre.retirerSuccesseur(gbp.getId());

                // Retirer le predecesseur du gPedal (optionnel, permet de laisser propre)
                gbp.retirerPredecesseur(predecs[i]);

            }

        }

        // Retirer les successeurs (mutuellement)
        var success = gbp.getSuccesseurs();	// Liste des ids
        var gbpSucc;

        for(i=0; i<success.length; i++){

            // Recuperer le gBasePedal par son id
            gbpSucc = this.getGPedalFromId(success[i]);

            // Verifier si le GBasePedal existe
            if(gbpSucc !== -1){

                // Retirer le gPedal du successeur
                gbpSucc.retirerPredecesseur(gbp.getId());

                // Retirer le successeur du gPedal (optionnel, permet de laisser propre)
                gbp.retirerSuccesseur(success[i]);

            }

        }

        // Retirer les connexions visuelles
        jspInstance.detachAllConnections($(div));
        jspInstance.removeAllEndpoints($(div));
        jspInstance.detach($(div));
        jspInstance.remove(div);	// Important pour retirer completement de l'instance de jsPlumb!!
        $(div).remove();

        // Retirer le gPedal de la liste des gPedals globale
        this.retirerGPedal(gbp);

    };

    // --- Methode retirerGPedal
    // Description : Retirer le gPedal de la liste des gPedals globale (retirement effectif de la liste)
    //
    this.retirerGPedal = function(gPedal){

        // Trouver l'indice du gPedal dans le tableau
        var index = this.getGPedals().indexOf(gPedal);

        // Si trouve, retirer de la liste
        if (index > -1) {
            this.getGPedals().splice(index, 1);
        }

    };

    // --- Methode retirerLienGBB
    // Description : Retire le lien GBasePedal (lien effectif par les tableaux successeur et predeccesseur)
    //
    this.retirerLienGBB = function(src, trg){

        // Recuperer GBB de la div src
        var GBBsrc = this.getGPedalFromDiv(src);

        // Recuperer GBB de la div trg
        var GBBtrg = this.getGPedalFromDiv(trg);

        // Retirer la source du target
        GBBtrg.retirerPredecesseur(GBBsrc.getId());

        // Retirer le target de la source
        GBBsrc.retirerSuccesseur(GBBtrg.getId());

    };

    // --- Methode initialiserVuePedals
    // --- Description : Initialiser la vue des pedals (Preset debut et preset fin)
    //
    this.initialiserVuePedals = function(){

        // Creer un pedal de debut
        //
        this.creerGBasePedal(nomPDeb);

        // Creer un pedal de fin
        //
        this.creerGBasePedal(nomPFin);

    }

    // --- Methode deconnecterAudioPreset
    // --- Description : Deconnecte entierement le graphe audio interne du preset
    //
    this.deconnecterAudioPreset = function(){

        // Recuperer tous les gPedals du preset
        var v_gPedals = this.m_gPedals;

        // Deconnecter l'audio de chaque pedal
        var v_gPedal;
        for(var i=0; i<v_gPedals.length; i++){

            // Recuperer le gPedal
            v_gPedal = v_gPedals[i];

            // Deconnecter le pedal
            try{
                v_gPedal.deconnecterToutesConnexionsAudio();
            }
            catch(err){ console.log(err.toString()); }

        }

    };

    // --- Methode connecterAudioPreset
    // --- Description : Connecte entierement le graphe audio interne du preset
    //
    this.connecterAudioPreset = function(){

        // Recuperer tous les gPedals du preset
        var v_gPedals = this.m_gPedals;

        // Deconnecter l'audio de chaque pedal
        var v_gPedal;
        for(var i=0; i<v_gPedals.length; i++){

            // Recuperer le gPedal
            v_gPedal = v_gPedals[i];

            // Deconnecter le pedal
            v_gPedal.connecterToutesConnexionsAudio();

        }

    };

}


Preset.prototype = new Serialize();
