/* Main application file */
/* Projet MT5 pedals */
/* Author : Nouriel AZRIA, under supervision of Michel Buffa */

/* ---------------------------------------------------------------------------------------- */


// --- Methode restoreAudioPedalGain
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
/*function restoreAudioPedalGain(){
	
}

// --- Methode restoreAudioPedalReverb
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalReverb(){
	
}

// --- Methode restoreAudioPedalCabinet
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalCabinet(){
	
}

// --- Methode restoreAudioPedalOverdrive
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalOverdrive(){
	
}

// --- Methode restoreAudioPedalDelay
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalDelay(){
	
	// Tenter de creer l'instance de la classe associee et le fournir au pedal
	try{
		//var v_AudioPedal = new Delay(audioCtx);
		//this.setAudioPedal(v_AudioPedal);
	}
	catch(err){}

}

// --- Methode restoreAudioPedalAmp
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalAmp(){
	
}

// --- Methode restoreAudioPedalFilter
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalFilter(){
	
}

// --- Methode restoreAudioPedalGate
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalGate(){
	
}

// --- Methode restoreAudioPedalWah
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalWah(){
	
	// Tenter de creer l'instance de la classe associee et le fournir au pedal
	try{
		var v_AudioPedal = new AutoWah(audioCtx);
		this.setAudioPedal(v_AudioPedal);
	}
	catch(err){}

}

// --- Methode restoreAudioPedalGraphicEQ
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalGraphicEQ(){
	
}

// --- Methode restoreAudioPedalPitch
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalPitch(){
	
}

// --- Methode restoreAudioPedalChorus
// --- Description : Methode permettant de restaurer l'instance de l'objet audio pedal selon le type de pedal
// 
function restoreAudioPedalChorus(){
	
}

*/

/************ Fin classes filles de la classe Pedal ************/



/* ********************************************************* DEBUT TRAITEMENT ********************************************************* */
/* ************************************************************************************************************************************ */
/* ************************************************************************************************************************************ */
/* ************************************************************************************************************************************ */

/* Variables globales */
/* ================== */

/* Variables d'elements courants */
/*********************************/
var categoriesPresets = []; // Liste des categories existantes
var categorieCourante;	// Categorie courante choisie (pour traitement d'ajout d'un preset)
var presetCourant;	// Preset courant choisi
var gPedalCourant; // Graphique pedal courant (pour traitements des elements independants du gPedal mais qui le concerne (ex : potentiometres associes))


/* Variables d'AudioContext */
/****************************/

	// Source et destination audio
    var input;	// Flux audio input (après association avec l'Audio Context)
    var output;	// Flux audio sortant
    var streamEntrant;	// Flux audio input (avant association avec l'Audio Context)



/* Variables concernant le GUI pop-up gestion categories et presets */
/********************************************************************/
var dataStorage = new DataStorage("pedalboard-mt5");	// Classe permettant de sauvegarder les elements sur IndexedDB (ou localStorage en fallback)
var listeCategories = $("#listeCategories");		// Conteneur de la liste des categories (sert comme GUI pour la gestion)
var buttonNouvelleCategorie = $("#buttonNouvelleCategorie"); // Button creation de nouvelle categorie
var cssElementInputNomCategorie = "cssElementInputNomCategorie";	// Class CSS pour identifier l'element modifiable contenant le nom d'une categorie dans le GUI
var cssElementCategorie = "cssElementCategorie";	// Class CSS pour identifier une categorie dans le GUI
var cssElementNomCategorie = "cssElementNomCategorie";	// Class CSS pour identifier l'element contenant le nom affichable d'une categorie dans le GUI
var cssElementButtonModifierCategorie = "cssElementButtonModifierCategorie";	// Class CSS pour identifier l'element de modification du nom d'une categorie dans la GUI
var cssElementButtonSupprimerCategorie = "cssElementButtonSupprimerCategorie";	// Class CSS pour identifier l'element de suppression d'une categorie dans la GUI


var listePresetsGlobaux = $("#listePresetsGlobaux");	// Conteneur de la liste des presets globaux draggables
var cssElementInputNomPreset = "cssElementInputNomPreset";	// Class CSS pour identifier l'element modifiable contenant le nom d'un preset dans le GUI
var cssElementPreset = "cssElementPreset";	// Class CSS pour identifier un prest dans le GUI
var cssElementNomPreset = "cssElementNomPreset";	// Class CSS pour identifier l'element contenant le nom affichable d'un preset dans le GUI
var cssElementButtonModifierPreset = "cssElementButtonModifierPreset";	// Class CSS pour identifier l'element de modification du nom d'un Preset dans la GUI
var cssElementButtonSupprimerPreset = "cssElementButtonSupprimerPreset";	// Class CSS pour identifier l'element de suppression d'un Preset dans la GUI


var listePresetsCategorieChoisie = $("#listePresetsCategorieChoisie");	// Conteneur de la liste des presets droppables par categorie choisie
var idCategorieChoisie;	// Contient l'id de la categorie a afficher (permet de savoir vers quelle categorie dropper un preset)
var listePresetsCategorieChoisieVide = true;	// Permet de detecter si la liste du listePresetsCategorieChoisie est vide ou non
var cssElementButtonSupprimerPresetCategorie = "cssElementButtonSupprimerPresetCategorie";	// Class CSS pour identifier l'element de suppression d'un Preset issu d'une categorie dans la GUI
var cssElementButtonSupprimerPresetCategorieImporte = "cssElementButtonSupprimerPresetCategorieImporte";	// Class CSS pour identifier l'element de suppression d'un Preset issu d'une categorie externe (importe) dans la GUI
var cssElementPresetCategorie = "cssElementPresetCategorie";	// Class CSS pour identifier un prest d'une categorie dans le GUI
var cssElementPresetCategorieImporte = "cssElementPresetCategorieImporte";	// Class CSS pour identifier un prest importe d'une categorie externe dans le GUI
var cssElementNomPresetCategorie = "cssElementNomPresetCategorie";	// Class CSS pour identifier l'element contenant le nom affichable d'un preset issu d'une categorie dans le GUI
var listePresetsCategorieChoisieTextDefaut = $("<span id='listePresetsCategorieChoisieTextDefaut'>Please select a preset bank</span>");	// Texte affichable par defaut (a afficher/desafficher selon besoin)
var titrePresetsDeCategorie = "#titrePresetsDeCategorie";	// Titre de la liste d'affichage des presets de la categorie choisie
var contenuTitrePresetsDeCategorie = "Banks";	// Titre affiche dans la page (associe a titrePresetsDeCategorie)


var iconElementButtonModifier = "<span class='glyphicon glyphicon-pencil'></span>";	// Icon de modification
var iconElementButtonSupprimer = "<span class='glyphicon glyphicon-remove'></span>";	// Icon de supression


/* Variables concernant le GUI de selection des categories et presets */
/**********************************************************************/
var affichageListePresets = "#affichageListePresets";	// Conteneur de la liste des liens des presets
var categorieChoix = "#categorieChoix"; // Liste de selection des categories 
var conteneurLienPreset = "conteneurLienPreset";	// Class CSS du conteneur du lien d'un preset
var numeroLienPreset = "numeroLienPreset";	// Class CSS du numero (s'affichant juste avant) le lien du preset
var lienPreset = "lienPreset";	// Class CSS du lien dans la liste des presets d'une categorie


/* Variables contenant les sections et selecteurs affichables dans la page */
/***************************************************************************/
var nomCategorieInitiale = "Factory Bank 1";	// Le nom donnee a la categorie initiale
var conteneurPedals = $("#affichagePedals"); // Conteneur des pedals
var conteneurPotentiometres = $("#affichagePotentiometres"); // Conteneur des potentiometres
var sectionPedals = $("#sectionPedals"); // Section contenant l'affichage des pedals (sert pour le resize)
var jspInstance = jsPlumb.getInstance(); // Une instance de jsPlumb
var nomPDeb = "debut", nomPFin = "fin"; // Le nom des pedals speciaux
var srcImgs = "./imgs/effects/";	// Lien dynamique vers le dossier des images

var listeChoixPedal = "#pedalChoix";	// Liste contenant le nom des pedals pouvant etre crees
var nomPresetAffichable = "#nomPresetAffichable";	// Le text contenant le nom du preset a afficher dans la section des pedals
var nomPresetModifiable = "#nomPresetModifiable";	// Le text contenant le nom du preset a afficher dans la section des pedals
var buttonNouveauPreset = '#buttonNouveauPreset';	// Bouton permettant de creer un nouveau preset
var boutonSauvegarde = '#buttonSauvegarde';	// Bouton permettant de sauvegarder
var boutonRestaurer = '#buttonRestaurer';	// Bouton permettant de restaurer


/* Definition des elements jsPlumb */
/* =============================== */

	/* Anchors */
	/* ======= */
	var anchorSortie = [ "Right", { shape:"Square", anchorCount:150 }];
	var anchorEntree = [ "Left", { shape:"Square", anchorCount:150 }];

	/* Style connexion */
	/* =============== */
	var overlayStyle = [ "Arrow", { foldback:0, location:1, width:25 } ]; // Le style "fleche"

	/* Endpoint */
	/* ======== */
	var endpointEntree = {
		endpoint:["Dot", { radius: 8}],
		maxConnections : -1,
		isSource:false,
		isTarget:true,
		paintStyle: { fillStyle: 'green' },
		anchor : anchorEntree
	};

	var endpointSortie = {
		endpoint:["Dot", { radius: 8}],
		maxConnections : -1,
		isSource:true,
		isTarget:false,
		paintStyle: { fillStyle: 'blue' },
		anchor : anchorSortie
	};

	/* Variable d'etat (Correction bug JS Plumb double evenement) */
	/* ========================================================== */
	var JSPconnexionMoved = false;	// Variable d'etat permettant de corriger le bug du declanchement de l'evenement "connexion" de JS Plumb lors d'un simple "connexionMoved"

/* /Fin definition des elements jsPlumb */
/* ==================================== */


// --- Fonction getCategoriePresetsId()
// Description : Recupere une categorie de presets par son id
//
function getCategoriePresetsId(id){

	// Recuperer la liste des elements
	var tab = categoriesPresets;

	// Scanner les categories existants
	var CP, CPId;
	for(var i=0; i<tab.length; i++){
		
		// Recuperer l'element
		CP = tab[i];

		// Recuperer l'id du GPedal
		CPId = CP.getId();

		// Verifier si la div correspond
		if(CPId == id){
			return CP;
		}
	}

	// Renvoyer une erreur
	return -1;

}

// --- Fonction creerCategoriePresets
// --- Description : Permet de creer une categorie de presets (Objet le plus haut de la hierarchie)
// --- Retour : Instance de CategoriePresets
// 
function creerCategoriePresets(){

	// Recuperer le tableau
	var tab = categoriesPresets;

	// Recuperer un id non existant et minimal
	var id = getIdLibre(tab);

	// Creer une nouvelle instance
	categoriePresets = new CategoriePresets(id);

	// Ajouter a la liste
	tab.push(categoriePresets);

	// Renvoyer le categoriePreset
	return categoriePresets;

}

// --- Fonction supprimerCategoriePresets (TODO)
// --- Description : Permet de supprimer une categorie de presets ainsi que les relations chez les autres categories
// 
function supprimerCategoriePresets(id){

	// Recuperer la liste contenante
	var tab = categoriesPresets;

	// Chercher dans la liste
	for(var i=0; i<tab.length; i++){

		// Verifier si la categorie correspond
		if(tab[i].getId() == id)
			// Supprimer la categorie
			tab.splice(tab.indexOf(tab[i]), 1);
		// Sinon, enlever la relation si elle existe
		else 
			tab[i].retirerPresetImporte(id);

	}

}

// --- Fonction getGPotentiometreFromId
// Description : Recupere un GPotentiometre par son id issu d'un GBasePedal
// Arguments : gbb - instance d'un GBasePedal contenant le potentiometre
//			   id - id du GPotentiometre
//
function getGPotentiometreFromId(gbb, id){

	// Recuperer les potentiometres 
	var GPs;
	if(gbb !== undefined && id !== undefined){
		GPs = gbb.getPedal().getPotentiometres();

		// Scanner les GPotentiometres existants
		var GP, GPId;
		for(var i=0; i<GPs.length; i++){
			
			// Recuperer le GPotentiometre
			GP = GPs[i].getGPotentiometre();

			// Recuperer l'id du GPotentiometre
			GPId = GP.getId();

			// Verifier si la div correspond
			if(GPId === id){
				return GP;
			}
		}
	}

	// Renvoyer une erreur
	return -1;

}

// --- Fonction ajouterEndPoints
// --- Description : Ajouter au GBasePedal (ou fille) un end point jsPlumb
//
function ajouterEndPoints(GBPedal){

	// Verifier quel type de GBasePedal il s'agit
	// -------------------------------------------

	// console.log(GBPedal.getDiv());
	// console.log($(GBPedal.getDiv()).width());

	// Verifier s'il s'agit d'un GBasePedal
	if(GBPedal instanceof GBasePedal){

		// Recuperer la div graphique
		var divPedal = GBPedal.getDiv();

		// Cas d'un GBPedal de debut
		if(GBPedal instanceof GPedalDebut){

			// Ajouter un endpoint de sortie
			jspInstance.addEndpoint(divPedal, endpointSortie);

		}

		// Cas d'un GBPedal de fin	
		if(GBPedal instanceof GPedalFin){

			// Ajouter un endpoint d'entree
			jspInstance.addEndpoint(divPedal, endpointEntree);
			
		}

		// Cas d'un GBPedal normal
		if(GBPedal instanceof GPedal){
			
			// Ajouter un endpoint de sortie
			jspInstance.addEndpoint(divPedal, endpointSortie);

			// Ajouter un endpoint d'entree
			jspInstance.addEndpoint(divPedal, endpointEntree);

		}

	}

}

// --- Fonction initialiserStructure
// --- Description : Initialiser l'application
// --- Retour : categorie - l'instance de la categorie creee
//
function initialiserStructure(){

	// Creer une categorie initiale (non modifiable et non supprimable)
	var categoriePresets = creerCategoriePresets();

	// Creer un preset a la categorie
	var preset = categoriePresets.creerPreset();

	// Indiquer la categorie courante
	categorieCourante = categoriePresets;

	// Deconnecter la structure audio interne du preset avant changement
	try{
		presetCourant.deconnecterAudioPreset();
	}
	catch(err){ console.log(err.toString()); }

	// Indiquer le preset courant
	presetCourant = preset;

	// Reconnecter la structure audio interne du preset apres changement
	try{
		presetCourant.connecterAudioPreset();
	}
	catch(err){ console.log(err.toString()); }

	// Renseigner le nom du preset dans l'affichage
	var nomPresetChoisi = preset.getNom();
	$(nomPresetAffichable).text(nomPresetChoisi);
	$(nomPresetModifiable).val(nomPresetChoisi);

	// Initialiser les pedals
	presetCourant.initialiserVuePedals();

	// Renvoyer l'instance de la catagorie creee
	return categoriePresets;

}

// --- function constructImgPedal
// Description : Fonction permettant de construire l'image d'une div
// 
function constructImgPedal(nomPedal, div, gPedal, CB){

	// Creer une instance image
	var img = new Image();

	// Indiquer le lien dynamique
	img.src = srcImgs + nomPedal + ".png";

	// Construire quand l'image s'est chargee
	img.onload = function(){

		// Renseigner l'image
		div.css('background-image', 'url('+ this.src +')');

		// Renseigner les dimensions
		div.css("width", this.width);
		div.css("height", this.height);

		// Appeler la fonction callback (construire les endpoints)
		if(CB !== undefined)
			CB();

	};

}

// --- Fonction saveStructure
// Description : Sauvegarder l'etat de la structure
//
function saveStructure() {
	console.log("Save structure");

	// Verifier si la classe de sauvegarde est bien reconnue
	if(dataStorage !== undefined){

		// Sauvegarder le tableau contenant les categories
		dataStorage.addValue("categoriesPresets", JSON.stringify(categoriesPresets));

	}

	// Verifier si le localhost est reconnu (pour IE sous local)
	/*if(localStorage !== undefined)
		// Sauvegarder le tableau contenant les categories
		localStorage.categoriesPresets = JSON.stringify(categoriesPresets);*/
}

// --- Fonction loadStructure
// Description : Restaurer l'etat de la structure
//
function loadStructure() {

	// Verifier si la classe de sauvegarde est bien reconnue
	if(dataStorage !== undefined){

		dataStorage.getValue("categoriesPresets", function(value){

			// Effacer l'affichage courant de pedals (et reinitialiser jsPlumb)
			reinitialiserAffichagePedals();

			// Recuperer le tableau des categories
			categoriesPresets = JSON.parse(value);

			// Redonner au classes leur signfication (grace a la classe Serialize et sa methode)
			restaurerClasses(categoriesPresets);

			// Reconstruire les liens d'ancrage des elements
			restaurerAncrageElements();

			// Reconstruire les classes d'audio de tous les pedals
			restaurerAudioPedals();

			// Recuperer les elements courants
			recupererElementsCourants();

			// Reconstruire le graphique des gBasePedals
			restaurergBasePedals();

			// Reconstruire la structure jsPlumb
			restaurerJSPlumbPedals();

			// Redimensionner les composants de la fenetre
			redimensionnerConteneursFenetre();

			// Renseigner les valeurs affichables des lists du GUI gestion
			construireGUIGestion();

			// Regenerer la liste des liens de la categorie choisie
			relisterLiensPresets($(categorieChoix).get()[0]);

			// Renseigner le nom du preset dans l'affichage
			renseignerNomPresetAffichage();

		});

	} 

	// Verifier si le localhost est reconnu (pour IE sous local)
	/*if(localStorage !== undefined)
		// Verifier si le localStorage a sauvegarde les categories
		if(localStorage.categoriesPresets) {

			// Effacer l'affichage courant de pedals (et reinitialiser jsPlumb)
			reinitialiserAffichagePedals();

			// Recuperer du localStorage le tableau des categories
			categoriesPresets = JSON.parse(localStorage.categoriesPresets);

			// Redonner au classes leur signfication (grace a la classe Serialize et sa methode)
			restaurerClasses(categoriesPresets);

			// Reconstruire les liens d'ancrage des elements
			restaurerAncrageElements();

			// Reconstruire les classes d'audio de tous les pedals
			restaurerAudioPedals();

			// Recuperer les elements courants
			recupererElementsCourants();

			// Reconstruire le graphique des gBasePedals
			restaurergBasePedals();

			// Reconstruire la structure jsPlumb
			restaurerJSPlumbPedals();

			// Redimensionner les composants de la fenetre
			redimensionnerConteneursFenetre();

			// Renseigner les valeurs affichables des lists du GUI gestion
			construireGUIGestion();

			// Regenerer la liste des liens de la categorie choisie
			relisterLiensPresets($(categorieChoix).get()[0]);

			// Renseigner le nom du preset dans l'affichage
			renseignerNomPresetAffichage();

		}*/
}

// --- Fonction renseignerNomPresetAffichage
// --- Description : Renseigne le nom du preset courant dans l'affichage dans la section des pedals
//
function renseignerNomPresetAffichage(){

	// Verifier si le preset courant est renseigne
	if(presetCourant !== undefined && presetCourant instanceof Preset){
		var nomPresetChoisi = presetCourant.getNom();
		$(nomPresetAffichable).text(nomPresetChoisi);
		$(nomPresetModifiable).val(nomPresetChoisi);
	}

}

// --- Fonction restaurerVuePreset
// Description : Restaure la vue du preset courant (Vue avec les pedals et le JS Plumb)
//
function restaurerVuePreset() {

	// Reinitialiser l'affichage des pedals
	reinitialiserAffichagePedals();

	// Reconstruire le graphique des gBasePedals
	restaurergBasePedals();

	// Reconstruire ancrage entre les potentiometres et les pedals associees, ainsi que l'audio (de la classe de Maxime)
	restaurerAncragePotentiometresPedals();

	// Reconstruire la structure jsPlumb
	restaurerJSPlumbPedals();

	// Redimensionner les composants de la fenetre
	redimensionnerConteneursFenetre();

	// Renseigner le nom du preset
	renseignerNomPresetAffichage();

}

// --- Fonction construireGUIGestion
// Description : Construire l'interface graphique de gestion
//
function construireGUIGestion() {

	//--- Gestion du GUI pop-up ---//
	//-----------------------------//

	// Construire la partie contenant la gestion des categories
	construireGUICategoriesPopUp();

	// Construire la partie contenant la gestion des presets
	construireGUIPresetsPopUp();

	// Reinitialiser l'affichage des presets d'une categorie
	reinitialiserGUIAffichagePresetsCategorie();

	//--- Gestion du GUI de selection ---//
	//-----------------------------------//
	construireGUISelection();
	

}

// --- Fonction reinitialiserGUIAffichagePresetsCategorie
// Description : Permet de remettre l'affichage des presets d'une categorie dans la GUI de la fenetre pop-up
//
function reinitialiserGUIAffichagePresetsCategorie() {

	// Reinitialiser la liste
	listePresetsCategorieChoisie.empty();
	listePresetsCategorieChoisieVide = true;

	// Reinitialiser l'id de la categorie choisie
	idCategorieChoisie = undefined;

	// Remettre le titre de base
	$(titrePresetsDeCategorie).html(contenuTitrePresetsDeCategorie);
	$(titrePresetsDeCategorie).css("margin-bottom", "0%");

	// Remettre l'affichage par defaut
	listePresetsCategorieChoisieTextDefaut.appendTo(listePresetsCategorieChoisie);

}

// --- Fonction construireGUIPresetsPopUp
// Description : Construire l'interface graphique de la gestion des presets dans la fenetre pop-up
//
function construireGUIPresetsPopUp() {

	// Recuperer l'instance de la liste des elements
	var tabElements = categoriesPresets;

	// Reinitialiser la liste
	listePresetsGlobaux.empty();

	// Afficher les presets de toutes les categories
	var tabPresets;
	var conteneur, identifiant, ENom, elementNom, elementNomModifiable, elementModification, elementSuppression;
	for(var j=0; j<tabElements.length; j++){

		// Recuperer les presets de la categorie
		tabPresets = tabElements[j].getPresets();

		// Verifier si les elements ont ete recuperes correctement
		if(tabPresets !== undefined){

			// Ajouter une ligne pour chaque preset
			for(var i=0; i< tabPresets.length; i++){

				// Recuperer le nom de l'element
				ENom = tabPresets[i].getNom();

				// Creer un element conteneur
				conteneur = $("<div></div>");

				// Renseigner les attributs du conteneur
				conteneur.attr('class', cssElementPreset);	// La classe identifiante du conteneur et de ses elements
				identifiant = tabElements[j].getId() + '-' + tabPresets[i].getId();
				conteneur.attr('id', identifiant);	// L'id identifiante

				// Creation du champ de nom affichable
				elementNom = $("<span></span>");

				// Renseigner les attributs du champ nom
				elementNom.attr('class', cssElementNomPreset);	// La classe identifiante de l'element contenant le nom
				elementNom.text(ENom);	// Le nom qu'il presente

				// Creation du champ de nom modifiable
				elementNomModifiable = $("<input type='text'></input>");

				// Renseigner les attributs du champ du nom modifiable
				elementNomModifiable.attr('class', cssElementInputNomPreset); 
				elementNomModifiable.val(ENom);
				elementNomModifiable.css('display', 'none'); 	// Cacher l'element en temps normal

				// Creation d'un element button de modification
				elementModification = $("<button type='button'></button>");

				// Renseigner les attributs du button modification
				elementModification.attr('class',cssElementButtonModifierPreset);	// La classe identifiante de l'element contenant le button

				// Ajouter l'icon
				$(iconElementButtonModifier).appendTo(elementModification);

				// Creation d'un element button de supression
				elementSuppression = $("<button type='button'></button>");

				// Renseigner les attributs du button supression
				elementSuppression.attr('class',cssElementButtonSupprimerPreset);	// La classe identifiante de l'element contenant le button

				// Ajouter l'icon
				if(tabPresets.length > 1)	// On evite de donner la possibilite de supprimer un preset unique de la categorie
					$(iconElementButtonSupprimer).appendTo(elementSuppression);

				// Ajout les elements dans le conteneur
				elementNom.appendTo(conteneur);
				elementNomModifiable.appendTo(conteneur);
				if(j != 0 || !(j == 0 && i == 0))	// Ajouter le bouton de suppression seulement si ce n'est pas le preset initial de base
					elementSuppression.appendTo(conteneur);
				elementModification.appendTo(conteneur);

				// Ajout dans la liste des categories
				conteneur.appendTo(listePresetsGlobaux);

				// Rendre l'element draggable
				$('.'+cssElementPreset).draggable({
					revert: "invalid", 
					helper: 'clone', 
					scroll: false, 
					appendTo:  listePresetsCategorieChoisie,

					// Modifier le style du clone
					start: function(e, ui){
						$(ui.helper).css("width", listePresetsGlobaux.css("width"));  //.addClass("ui-draggable-helper");     
				    }

				});

			}

		}

	}

}

// --- Fonction construireGUICategoriesPopUp
// Description : Construire l'interface graphique de la gestion des categories dans la fenetre pop-up
//
function construireGUICategoriesPopUp() {

	// Recuperer l'instance de la liste des elements
	var tabElements = categoriesPresets;

	// Reinitialiser la liste
	listeCategories.empty();

	// Ajouter une ligne pour chaque categorie
	var conteneur, ENom, elementNom, elementNomModifiable, elementModification, elementSuppression;
	for(var i=1; i< tabElements.length; i++){	// 1 car on ne souhaite pas supprimer la categorie initiale globale

		// Recuperer le nom de l'element
		ENom = tabElements[i].getNom();

		// Creer un element conteneur
		conteneur = $("<div></div>");

		// Renseigner les attributs du conteneur
		conteneur.attr('class', cssElementCategorie);	// La classe identifiante du conteneur et de ses elements
		conteneur.attr('id', tabElements[i].getId());	// L'id identifiante

		// Creation du champ de nom affichable
		elementNom = $("<span></span>");

		// Renseigner les attributs du champ nom
		elementNom.attr('class', cssElementNomCategorie);	// La classe identifiante de l'element contenant le nom
		elementNom.text(ENom);	// Le nom qu'il presente

		// Creation du champ de nom modifiable
		elementNomModifiable = $("<input type='text'></input>");

		// Renseigner les attributs du champ du nom modifiable
		elementNomModifiable.attr('class', cssElementInputNomCategorie); 
		elementNomModifiable.val(ENom);
		elementNomModifiable.css('display', 'none'); 	// Cacher l'element en temps normal

		// Creation d'un element button de modification
		elementModification = $("<button type='button'></button>");

		// Renseigner les attributs du button modification
		elementModification.attr('class',cssElementButtonModifierCategorie);	// La classe identifiante de l'element contenant le button

		// Ajouter l'icon
		$(iconElementButtonModifier).appendTo(elementModification);

		// Creation d'un element button de supression
		elementSuppression = $("<button type='button'></button>");

		// Renseigner les attributs du button supression
		elementSuppression.attr('class',cssElementButtonSupprimerCategorie);	// La classe identifiante de l'element contenant le button

		// Ajouter l'icon
		$(iconElementButtonSupprimer).appendTo(elementSuppression);

		// Ajout les elements dans le conteneur
		elementNom.appendTo(conteneur);
		elementNomModifiable.appendTo(conteneur);
		elementSuppression.appendTo(conteneur);
		elementModification.appendTo(conteneur);

		// Ajout dans la liste des categories
		conteneur.appendTo(listeCategories);

	}

}

// --- Fonction construireGUISelection
// Description : Construire l'interface graphique de la selection des categories et presets
//
function construireGUISelection() {

	// Recuperer l'instance de la liste des elements
	var tabElements = categoriesPresets;

	// Recuperer la liste de selection
	var listeChoixCategorie = $(categorieChoix);

	// Vider la liste (sauf celui de defaut)
	listeChoixCategorie.find('option').remove(); //'option:not(:first)'

	// Ajouter chaque element dans la liste des categories
	var option, id, Enom;
	for(i=0; i< tabElements.length; i++){

		// Recuperer le nom de l'element
		ENom = tabElements[i].getNom();

		// Recuperer l'id de l'element
		id = tabElements[i].getId();

		// Creer le representant de la categorie a la liste
		option = $("<option>" + ENom + "</option>");

		// Renseigner les attributs du representant
		option.attr('value',id);	// La valeur contenant l'id de la categorie

		// Ajouter le representant a la liste
		listeChoixCategorie.append(option);

	}

	// Refreshe la liste (car utilisation de Bootstrap-select)
	listeChoixCategorie.selectpicker('refresh');

}

// --- Fonction construireGUIPresetsDeCategoriePopUp
// Description : Construire l'interface graphique affichant les presets d'une categorie choisie dans la fenetre pop-up
// Argument : idCategorie - L'id d'une categorie dont on souhaite afficher les presets
//
function construireGUIPresetsDeCategoriePopUp(idCategorie) {

	// Verifier si l'argument est valide
	if(idCategorie === undefined)
		return;

	// Recuperer l'instance de la categorie
	var laCategorie = getCategoriePresetsId(idCategorie);

	// Verifier si la categorie a ete trouvee
	if(laCategorie === undefined || laCategorie == -1)
		return;

	// Reinitialiser la liste
	listePresetsCategorieChoisie.empty();

	// Indiquer que la liste n'est pas vide
	listePresetsCategorieChoisieVide = false;

	// Renseigner l'id de la categorie choisie
	idCategorieChoisie = idCategorie;

	// Modifier le titre
	$(titrePresetsDeCategorie).html(contenuTitrePresetsDeCategorie + " " + laCategorie.getNom());
	$(titrePresetsDeCategorie).css("margin-bottom", "2%");

	// Recuperer les presets de la categorie
	var tabPresets;
	tabPresets = laCategorie.getPresets();

	// Verifier si les elements ont ete recuperes correctement
	var conteneur, identifiant, ENom, elementNom, elementSuppression;
	if(tabPresets !== undefined){

		// Ajouter une ligne pour chaque preset
		for(var i=0; i< tabPresets.length; i++){

			// Recuperer le nom de l'element
			ENom = tabPresets[i].getNom();

			// Creer un element conteneur
			conteneur = $("<div></div>");

			// Renseigner les attributs du conteneur
			conteneur.attr('class', cssElementPresetCategorie);	// La classe identifiante du conteneur et de ses elements
			identifiant = laCategorie.getId() + '-' + tabPresets[i].getId() + "-presetCategorie";
			conteneur.attr('id', identifiant);	// L'id identifiante

			// Creation du champ de nom affichable
			elementNom = $("<span></span>");

			// Renseigner les attributs du champ nom
			elementNom.attr('class', cssElementNomPresetCategorie);	// La classe identifiante de l'element contenant le nom
			elementNom.text(ENom);	// Le nom qu'il presente

			// Creation d'un element button de supression
			elementSuppression = $("<button type='button'></button>");

			// Renseigner les attributs du button supression
			elementSuppression.attr('class',cssElementButtonSupprimerPresetCategorie);	// La classe identifiante de l'element contenant le button

			// Ajouter l'icon
			$(iconElementButtonSupprimer).appendTo(elementSuppression);

			// Ajout les elements dans le conteneur
			elementNom.appendTo(conteneur);

			if(tabPresets.length > 1)	// On evite de donner la possibilite de supprimer un preset unique de la categorie
				elementSuppression.appendTo(conteneur);

			// Ajout dans la liste des categories
			conteneur.appendTo(listePresetsCategorieChoisie);

		}

	}


	// Recuperer les presets importes
	tabPresets = laCategorie.getPresetsImportes();

	// Verifier si les elements ont ete recuperes correctement
	var idCategorieImportee, idPresetImporte, categorieImportee, presetImporte;
	if(tabPresets !== undefined){

		// Ajouter une ligne pour chaque preset importe
		for(var i=0; i< tabPresets.length; i++){

			// Recuperer les ids
			idCategorieImportee = tabPresets[i][0];
			idPresetImporte = tabPresets[i][1];

			// Recuperer la categorie dont est issue le preset importe
			categorieImportee = getCategoriePresetsId(idCategorieImportee);

			// Verifier si la categorie a bien ete recuperee
			if(categorieImportee !== undefined && categorieImportee != -1){

				// Recuperer le preset importe
				presetImporte = categorieImportee.getPresetById(idPresetImporte);

				// Verifier si le preset a bien ete recupere
				if(presetImporte !== undefined && presetImporte != -1){

					// Recuperer le nom de l'element
					ENom = presetImporte.getNom();

					// Creer un element conteneur
					conteneur = $("<div></div>");

					// Renseigner les attributs du conteneur
					conteneur.attr('class', cssElementPresetCategorie);	// La classe identifiante du conteneur et de ses elements
					conteneur.attr('class', cssElementPresetCategorieImporte);	// La classe identifiante du contenu d'un preset importe
					identifiant = idCategorieImportee + '-' + idPresetImporte + "-presetCategorie-Importe";
					conteneur.attr('id', identifiant);	// L'id identifiante

					// Creation du champ de nom affichable
					elementNom = $("<span></span>");

					// Renseigner les attributs du champ nom
					elementNom.attr('class', cssElementNomPresetCategorie);	// La classe identifiante de l'element contenant le nom
					elementNom.text(ENom);	// Le nom qu'il presente

					// Creation d'un element button de supression
					elementSuppression = $("<button type='button'></button>");

					// Renseigner les attributs du button supression
					elementSuppression.attr('class',cssElementButtonSupprimerPresetCategorieImporte);	// La classe identifiante de l'element contenant le button

					// Ajouter l'icon
					$(iconElementButtonSupprimer).appendTo(elementSuppression);

					// Ajout les elements dans le conteneur
					elementNom.appendTo(conteneur);
					elementSuppression.appendTo(conteneur);

					// Ajout dans la liste des categories
					conteneur.appendTo(listePresetsCategorieChoisie);

				}

			}

		}

	}

}

// --- Fonction recupererElementsCourants
// Description : Restaure l'etat des elements courants (TODO : Remplacer par des try catch et creer si jamais non existants)
//
function recupererElementsCourants() {

	// Recuperer l'instance de la liste des elements
	var tabElements = categoriesPresets;

	// Verifier si la liste des elements existe bien
	if(tabElements !== undefined){

		// Verifier si le 1er elements a un preset
		if(tabElements[0].getPresets()[0] !== undefined){

			// Deconnecter la structure audio interne du preset avant changement
			try{
				presetCourant.deconnecterAudioPreset();
			}
			catch(err){ 
				console.log("index.js:recupererElementsCourants:presetCourant undefined... maybe it's normal if the current pedalboard had no previous preset...")
				console.log(err.toString());
			}

			// Recuperer et indiquer l'element courant
			presetCourant = tabElements[0].getPresets()[0];

			// Reconnecter la structure audio interne du preset apres changement
			try{
				presetCourant.connecterAudioPreset();
			}
			catch(err){ console.log(err.toString()); }

			// Verifier si l'element a un pedal
			if(presetCourant.getGPedals()[0] !== undefined)
				// Recuperer et indiquer l'element courant
				gPedalCourant = presetCourant.getGPedals()[0];

		}

	}

}

// --- Fonction restaurerJSPlumbPedals
// Description : Restaure l'etat du jsPlumb d'apres la structure du tableau des gBasePedals
//
function restaurerJSPlumbPedals() {

	// Recuperer le tableau des GPedals contenant la structure de jsPlumb
	var tabGPedals = presetCourant.getGPedals();

	// Ajouter les endpoints au gPedals
	for( var j=0; j<tabGPedals.length; j++)
		ajouterEndPoints(tabGPedals[j]);

	// Recuperer et recreer les liens jsPlumb de chaque instance de gBasePedal
	var gbp, tabSucc, srcId, elem, elemSucc;
	var endpointsSrc, endpointSrc, endpointsTarget, endpointTarget;
	for(var i=0; i<tabGPedals.length; i++){

		// Recuperer l'instance de gBasePedal
		gbp = tabGPedals[i];

		// Recuperer l'id du graphique
		srcId = gbp.getId();

		// Recuperer l'element
		//elem = $("#"+srcId).get()[0];

		// Recuperer les endpoints du source
		endpointsSrc = jspInstance.getEndpoints(srcId);	//.getEndpoints(elem);

		// Verifier quel endpoint est le point de sortie
		if(endpointsSrc[0].isSource)
			endpointSrc = endpointsSrc[0];
		else
			if(endpointsSrc[1] !== undefined)
				endpointSrc = endpointsSrc[1];

		// Verifier que le gBasePedal n'est pas le pedal de fin
		if(!(gbp instanceof GPedalFin)){

			// Recuperer la liste des successeurs
			tabSucc = gbp.getSuccesseurs();
			for(var j=0; j<tabSucc.length; j++){

				// Recuperer l'element successif
				//elemSucc = $("#"+tabSucc[j]).get()[0];

				// Recuperer les endpoints du target
				endpointsTarget = jspInstance.getEndpoints(tabSucc[j]);	//.getEndpoints(elemSucc);

				// Verifier quel endpoint est le point de rentree
				if(endpointsTarget[0].isTarget)
					endpointTarget = endpointsTarget[0];
				else
					if(endpointsTarget[1] !== undefined)
						endpointTarget = endpointsTarget[1];

				// Connecter les graphiques par jsPlumb
				jspInstance.connect({source:endpointSrc, target:endpointTarget, overlays: [overlayStyle]});
				//jspInstance.repaintEverything();

			}

		}

	}

	// Restaurer les evenements de jsPlumb
	restaurerEvenementsJSP();

}

// --- Fonction restaurerEvenementsJSP
// Description : Restaure les evenements du jsPlumb
// 
function restaurerEvenementsJSP(){

	// Rajouter les evenements externes
	jspInstance.bind("connection", jspEventConnecion);
	jspInstance.bind("click", jspEventClick);
	jspInstance.bind("connectionDetached", jspEventDetached);
	jspInstance.bind("connectionMoved", jspEventMoved);

}

// --- Fonction restaurergBasePedals
// Description : Restaure l'etat d'un gBasePedal (apres rechargement)
//
function restaurergBasePedals() {

	// Recuperer le tableau des GPedals a reconstruire
	var tabGPedals = presetCourant.getGPedals();

	// Traiter chaque gPedal
	var gbp;
	for(var i=0; i<tabGPedals.length; i++){

		// Recuperer le gPedal courant
		gbp = tabGPedals[i];

		// Construire le graphique associe (div)
		var graphique = $("<div></div>").attr('id', gbp.getId()).attr('class','divPedal');

		// Renseigner la position du graphique
		graphique.css('top', gbp.getPosition().getY());
		graphique.css('left', gbp.getPosition().getX());

		// Ajouter le CSS selon le type
		var type = gbp.getPedal().getType();
		switch(type){

			// --- S'il s'agit d'un pedal de debut
			case nomPDeb : 
				graphique.addClass('divPedalDeb');
				break;

			// --- S'il s'agit d'un pedal de fin
			case nomPFin :
				graphique.addClass('divPedalFin');
				break;

			// --- S'il s'agit d'un pedal normal
			default :
				// Ajouter le css normal
				graphique.addClass('divPedalNormal');

		}

		// Redonner au gBasePedal l'instance du graphique
		gbp.setDiv(graphique.get()[0]);

		// Reconstruire l'image du graphique
		constructImgPedal(type, graphique, gbp,
			function() {

				// --- jsPlumb ---
				// ---------------
				jspInstance.ready(function() {

					// Rendre le graphique draggable uniquement dans le conteneur
					jspInstance.draggable($(".divPedal"), {
						// Le conteneur
						containment:conteneurPedals
					});

					// Ajouter les endpoints
					//ajouterEndPoints(gPedal);


				});
				// --- /jsPlumb ---
				// ----------------
				
				// Redessiner tous les elements jsPlumb (pour remettre a jour les endpoints)
				jspInstance.repaintEverything();

			}
		);

		// Ajouter les endpoints au gPedal
		//ajouterEndPoints(gbp); // Exporte vers restaurerJSPlumbPedals

		// Ajouter la div dans l'affichage
		graphique.appendTo(conteneurPedals);

	}

}

// --- Fonction restaurerAncrageElements
// --- Description : Restaure l'ancreage general des elements de l'application
//
function restaurerAncrageElements(){

	// Reconstruire les liens d'ancrage entre les presets et les gPedals
	restaurerAncrageGPedalsPresets();

	// Reconstruire lien d'ancrage entre les potentiometres et les pedals associees, ainsi que l'audio (de la classe de Maxime)
	restaurerAncragePotentiometresPedals();

}

// --- Fonction restaurerAncrageGPedalsPresets
// --- Description : Restaure l'ancreage entre les GPedals et les presets qui les relient
//
function restaurerAncrageGPedalsPresets(){

	// Recuperer le tableau des categories
	var categories = categoriesPresets;

	// Verifier si le tableau est valide
	if(categories === undefined)
		return;

	// Recuperer chaque categorie
	var uneCategorie;
	for(var i=0; i<categories.length; i++){

		// Recuperer la categorie
		uneCategorie = categories[i];

		// Recuperer les presets
		var presets = uneCategorie.getPresets();

		// Recuperer chaque preset de la categorie
		var unPreset;
		for(var j=0; j<presets.length; j++){

			// Recuperer le preset
			unPreset = presets[j];

			// Recuperer les gPedals
			var gPedals = unPreset.getGPedals();

			// Renseigner a chaque gPedal l'ancrage vers le preset qui le contient
			var gPedal;
			for(var k=0; k<gPedals.length; k++){

				// Recuperer le gPedal
				gPedal = gPedals[k];

				// Renseigner l'ancrage vers le preset contenant
				gPedal.setAncrePreset(unPreset);

			}
				
		}

	}

}

// --- Fonction restaurerAncragePotentiometresPedals (TODO reconstruction de l'objet de Maxime)
// --- Description : Restaure l'ancre entre les Potentiometres et le pedal (apres rechargement) dans le but de pouvoir interagir avec l'audio (de la classe de Maxime)
//
function restaurerAncragePotentiometresPedals() {

	// Recuperer le tableau des categories
	var categories = categoriesPresets;

	// Verifier si le tableau est valide
	if(categories === undefined)
		return;

	// Recuperer chaque categorie
	var uneCategorie;
	for(var i=0; i<categories.length; i++){

		// Recuperer la categorie
		uneCategorie = categories[i];

		// Recuperer les presets
		var presets = uneCategorie.getPresets();

		// Recuperer chaque preset de la categorie
		var unPreset;
		for(var j=0; j<presets.length; j++){

			// Recuperer le preset
			unPreset = presets[j];

			// Recuperer les gPedals
			var gPedals = unPreset.getGPedals();

			// Renseigner a chaque potentiometre l'ancrage vers le pedal qui le contient
			var gPedal, v_pedal;
			for(var k=0; k<gPedals.length; k++){

				// Recuperer le gPedal
				gPedal = gPedals[k];

				// Lier les potentiometres des pedals au pedals qui les contiennent
				try{

					// Recuperr le pedal associe
					v_pedal = gPedal.getPedal();

					// Lier les potentiometre du pedal au pedal associe
					v_pedal.lierAncrePedalPotentiometres();

				}
				catch(err){ // Pas de traitement car au depart l'ancrage n'existe pas
				}

			}
				
		}

	}

}

// --- Fonction restaurerAudioPedals
// --- Description : Restaure les objets audio de tous les pedals (en cas de chargement de la structure, ou ces objets n'existe plus)
// 
function restaurerAudioPedals(){

	// Recuperer le tableau des categories
	var categories = categoriesPresets;

	// Verifier si le tableau est valide
	if(categories === undefined)
		return;

	// Recuperer chaque categorie
	var uneCategorie;
	for(var i=0; i<categories.length; i++){

		// Recuperer la categorie
		uneCategorie = categories[i];

		// Recuperer les presets
		var presets = uneCategorie.getPresets();

		// Recuperer chaque preset de la categorie
		var unPreset;
		for(var j=0; j<presets.length; j++){

			// Recuperer le preset
			unPreset = presets[j];

			// Recuperer les gPedals
			var gPedals = unPreset.getGPedals();

			// Renseigner a chaque gPedal l'ancrage vers le preset qui le contient
			var gPedal, v_pedal;
			for(var k=0; k<gPedals.length; k++){

				// Recuperer le gPedal
				gPedal = gPedals[k];

				// Recuperer l'instance de pedal associe
				v_pedal = gPedal.getPedal();

				// Reconstruire l'audio du pedal (TODODO)
				try{
					v_pedal.restoreAudioPedal();
				}
				catch(err){ console.log(err.toString()); }

			}
				
		}

	}
	
}

// --- Fonction reinitialiserAffichagePedals
// Description : Reinitialise l'etat de jsPlumb et de l'affichage courant des pedals
//
function reinitialiserAffichagePedals() {

	// Reinitialiser jsPlumb
	reinitialiserJSPlumb();

	// Reinitialiser l'affichage du conteneur des pedals
	conteneurPedals.empty();

	// Reinitialiser l'affichage du conteneur des potentiometres
	resetConteneurPotentiometres();

}

// --- Fonction reinitialiserJSPlumb
// Description : Reinitialise l'etat de jsPlumb
function reinitialiserJSPlumb() {

	// Reinitialiser jsPlumb
	jspInstance.reset();

}

/* Evenements globaux */
/* ------------------ */

// --- Initialisation de la page --- //
// --------------------------------- //
$(document).ready(function(){

	// --- Calculer la taille des conteneurs selon la taille de l'ecran (corriger bug de zoom)--- //
	var hauteurEcran = $(document).height();
	var largeurEcran = $(document).width();

	$("#sectionGenerale").css('height', hauteurEcran);
	$("#sectionGenerale").css('width', largeurEcran);

	// Initialiser l'Audio Context
	initialiseAudioContext();

	// Verifier si la classe de sauvegarde est bien reconnue
	if(dataStorage === undefined) 
		dataStorage = new DataStorage("pedalboard-mt5");


	// Verifier si l'application a deja ete initialisee
	//dataStorage.deleteValue("categoriesPresets");	// Commenter/decommenter en cas de tests, pour nettoyer la sauvegarde
	dataStorage.getValue("categoriesPresets", function(value){ 

		// Restaurer l'etat de l'application si elle existe
		if(value !== undefined) {
			loadStructure();
		}
		// Sinon
		else{

			// Initialiser l'application
			initialiserStructure();

			// Modifier le nom de la categorie
			categoriesPresets[0].setNom(nomCategorieInitiale);

			// Renseigner les valeurs affichables des lists du GUI gestion
			construireGUIGestion();

			// Regenerer la liste des liens de la categorie choisie
			relisterLiensPresets($(categorieChoix).get()[0]);

		}

	});

	// ---- TRAITEMENT EVENEMENTS ---- //
	// ------------------------------- //

	// Indiquer la procedure a suivre en cas de changement de taille de la fenetre
	$(window).resize(function(){

		// Redimensionner les composants de la fenetre
		redimensionnerConteneursFenetre();

		// Repeindre tous les elements du jsPlumb
		jspInstance.repaintEverything();

		// Redimensionner les autres composants de l'application
		redimensionnerComposantsApplication();
	});


	// --- Evenement drop d'un gPedal
	// 
	conteneurPedals.on("mouseup", ".divPedal", function(){

		// Recuperer le div 
		var div = this;

		// Recuperer le GBasePedal
		var gbp = presetCourant.getGPedalFromDiv(div);

		// Recuperer la nouvelle position (relative au conteneur)
		var top = div.offsetTop; // var top = div.getBoundingClientRect().top;
		var left = div.offsetLeft; // var left = div.getBoundingClientRect().left;

		// Modifier la position
		gbp.setPosition(left, top);

		// MB
		saveStructure();

	});

	// --- Evenement modification des GPotentiometre de type basiques
	// 
	conteneurPotentiometres.delegate(".gPotentiometreInfos .gPotentiometre", 'change', function(){

		// Recuperer l'instance du gPotentiometre
		var gpara = getGPotentiometreFromId(gPedalCourant, this.id);
		
		// Verifier si la recherche de l'instance a reussi
		if(gpara !== -1){

			// Recuperer la valeur souhaitee selon le type du graphique (/* TODO */ a enrichi si besoin d'autres types) (autre solution : Verifier le type de gpara)
			var valeur;
			if(this.checked === undefined)
				valeur = this.value;
			else
				valeur = this.checked;
			
			// Renseigner la valeur
			gpara.setValeurTraite(valeur);

		}

	});

	// --- Evenement choix de categorie de la liste
	// 
	$(categorieChoix).on('change', function(){
		
		// Relister l'affichage des liens 
		relisterLiensPresets(this);

		// Recuperer le lien vers le premier preset de la categorie choisie
		//var lienVersPreset = $(affichageListePresets).find('.' + conteneurLienPreset, '#' + categorieCourante.getId() + "-0");
		var lienVersPreset = $(affichageListePresets).find('.' + conteneurLienPreset).first().find('.' + lienPreset);

		// Afficher le premier preset de la categorie choisie
		affichagePresetDeCategorieParIdCompose(lienVersPreset[0]);

	});

	// --- Evenement droppable du conteneur des presets issus d'une categorie
	//
	listePresetsCategorieChoisie.droppable({

		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		accept: '.'+cssElementPreset,

		// Gerer l'evenement de droppage des balises des presets draggables
		drop: function( event, ui ) {

			// Verifier si la liste est vide ou non
			if(!listePresetsCategorieChoisieVide){

				// Recuperer l'id du preset a ajouter
				var id = ui.draggable.clone()[0].id;

				// Verifier si l'id a ete recupere
				if(id === undefined)
					return;

				// Recuperer l'id de la categorie et du preset
				var idCategorie = id.split('-')[0];
				var idPreset = id.split('-')[1];

				// Verifier si les ids ont ete recuperes
				if(idCategorie === undefined || idPreset === undefined || (idCategorie === undefined && idPreset === undefined))
					return;

				// Recuperer la categorie
				var categorieChoisie = getCategoriePresetsId(idCategorieChoisie);

				// Verifier si la categorie a ete trouvee
				if(categorieChoisie === undefined || categorieChoisie == -1)
					return;

				// Ajouter le preset 
				categorieChoisie.ajouterPresetImporte(idCategorie, idPreset);

				// Regenerer la vue de la fenetre pop-up
				construireGUIPresetsDeCategoriePopUp(idCategorieChoisie);

				// Regenerer la liste des liens de la categorie choisie
				relisterLiensPresets($(categorieChoix).get()[0]);

			}

		}

	});

});

// --- Fonction relisterLiensPresets
// Description : Reliste l'affichage des liens des presets d'une categorie
// TODODO2
//
function relisterLiensPresets(liste) {

	// Recuperer l'element selectionne
	var elementSelectionne = $(liste).find('option:selected');

	// Recuperer l'id de la categorie
	var idCategorie = elementSelectionne.val();

	// Recuperer la categorie
	var categorie = getCategoriePresetsId(idCategorie);

	// Verifier si la categorie a ete trouvee
	var listePresets;
	if(categorie != -1){

		// Indiquer la categorie courante
		categorieCourante = categorie;

		// Recuperer le conteneur de la liste
		var conteneurListePresets = $(affichageListePresets);

		// Vider la liste
		conteneurListePresets.empty();

		// Recuperer la liste des presets
		listePresets = categorie.getPresets();

		// Ajouter les presets dans le conteneur
		var conteneurLien, numeroLien, lienSelectionPreset, idPreset, nomPreset;
		var elementNomModifiable, elementModification;
		var elementSupression;
		for(var i=0; i< listePresets.length; i++){

			// Recuperer l'id du preset
			idPreset = listePresets[i].getId();

			// Recuperer le nom du preset
			nomPreset = listePresets[i].getNom();

			// Creer le conteneur
			conteneurLien = $("<div></div>");

			// Renseigner les attributs du conteneur
			conteneurLien.attr('class', conteneurLienPreset);

			// Creer le numero du lien
			numeroLien = $("<span>" + i + ".</span>");

			// Renseigner les attributs du numero
			numeroLien.attr('class', numeroLienPreset);

			// Ajouter le lien representant le preset
			lienSelectionPreset = $("<a>" + nomPreset + "</a>");

			// --------- AJOUT ELEMENTS MODIFICATION --------- //

			// Creation du champ de nom modifiable
			elementNomModifiable = $("<input type='text'></input>");

			// Renseigner les attributs du champ du nom modifiable
			elementNomModifiable.attr('class', cssElementInputNomPreset); 
			elementNomModifiable.val(nomPreset);
			elementNomModifiable.css('display', 'none'); 	// Cacher l'element en temps divPedalNormal

			// Creation d'un element button de modification
			elementModification = $("<button type='button'></button>");

			// Renseigner les attributs du button modification
			elementModification.attr('class',cssElementButtonModifierPreset);	// La classe identifiante de l'element contenant le button

			// Ajouter l'icon
			$(iconElementButtonModifier).appendTo(elementModification);

			// --------- /FIN AJOUT ELEMENTS MODIFICATION --------- //

			// --------- AJOUT ELEMENTS SUPRESSION --------- //

			// Creation d'un element button de supression
			elementSupression = $("<button type='button'></button>");

			// Renseigner les attributs du button supression
			elementSupression.attr('class',cssElementButtonSupprimerPresetCategorie);	// La classe identifiante de l'element contenant le button

			// Ajouter l'icon
			$(iconElementButtonSupprimer).appendTo(elementSupression);

			// --------- /FIN AJOUT ELEMENTS SUPRESSION --------- //

			// Renseigner les attributs du lien
			lienSelectionPreset.attr('id', idCategorie+'-'+idPreset);
			lienSelectionPreset.attr('class', lienPreset);

			// Ajouter le numero et le lien au conteneur
			numeroLien.appendTo(conteneurLien);
			lienSelectionPreset.appendTo(conteneurLien);

			// Ajouter l'element de supression seulement s'il y a plusieurs preses disponibles dans la categorie
			if(listePresets.length > 1)
				elementSupression.appendTo(conteneurLien);

			// Ajouter les elements de modification
			elementNomModifiable.appendTo(conteneurLien);
			elementModification.appendTo(conteneurLien);

			// Ajouter le tout a la liste affichee
			conteneurListePresets.append(conteneurLien);

		}

		// Recuperer la liste des presets importes
		listePresets = categorie.getPresetsImportes();

		// Ajouter les presets importes dans le conteneur
		var idCategorieImportee, idPresetImporte, categorieImportee, presetImporte, numLien;
		for(var j=0; j< listePresets.length; j++){

			// Recuperer l'id de la categorie importee
			idCategorieImportee = listePresets[j][0];

			// Recuperer l'id du preset importe
			idPresetImporte = listePresets[j][1];

			// Tenter de recuperer la categorie
			categorieImportee = getCategoriePresetsId(idCategorieImportee);

			// Verifier si la categorie a ete recuperee
			if(categorieImportee !== undefined || categorieImportee != -1){

				// Tenter de recuperer le preset importe
				presetImporte = categorieImportee.getPresetById(idPresetImporte);

				// Verifier si le preset a ete recupere
				if(presetImporte !== undefined || presetImporte != -1){

					// Recuperer le nom du preset
					nomPreset = presetImporte.getNom();

					// Creer le conteneur
					conteneurLien = $("<div></div>");

					// Renseigner les attributs du conteneur
					conteneurLien.attr('class', conteneurLienPreset);

					// Calculer le numero du lien dans la liste
					numLien = i + parseInt(j);

					// Creer le numero du lien
					numeroLien = $("<span>" + numLien + ".</span>");

					// Renseigner les attributs du numero
					numeroLien.attr('class', numeroLienPreset);

					// Ajouter le lien representant le preset
					lienSelectionPreset = $("<a>" + nomPreset + "</a>");

					// Renseigner les attributs du lien
					lienSelectionPreset.attr('id', idCategorieImportee+'-'+idPresetImporte+'-num'+numLien+'-'+idCategorie);
					lienSelectionPreset.attr('class', lienPreset);

					// --------- AJOUT ELEMENTS MODIFICATION --------- //

					// Creation du champ de nom modifiable
					elementNomModifiable = $("<input type='text'></input>");

					// Renseigner les attributs du champ du nom modifiable
					elementNomModifiable.attr('class', cssElementInputNomPreset); 
					elementNomModifiable.val(nomPreset);
					elementNomModifiable.css('display', 'none'); 	// Cacher l'element en temps divPedalNormal

					// Creation d'un element button de modification
					elementModification = $("<button type='button'></button>");

					// Renseigner les attributs du button modification
					elementModification.attr('class',cssElementButtonModifierPreset);	// La classe identifiante de l'element contenant le button

					// Ajouter l'icon
					$(iconElementButtonModifier).appendTo(elementModification);

					// --------- /FIN AJOUT ELEMENTS MODIFICATION --------- //

					// --------- AJOUT ELEMENTS SUPRESSION --------- //

					// Creation d'un element button de supression
					elementSupression = $("<button type='button'></button>");

					// Renseigner les attributs du button supression
					elementSupression.attr('class',cssElementButtonSupprimerPresetCategorieImporte);	// La classe identifiante de l'element contenant le button

					// Ajouter l'icon
					$(iconElementButtonSupprimer).appendTo(elementSupression);

					// --------- /FIN AJOUT ELEMENTS SUPRESSION --------- //

					// Ajouter le numero et le lien au conteneur
					numeroLien.appendTo(conteneurLien);
					lienSelectionPreset.appendTo(conteneurLien);

					// Ajouter l'element de supression
					elementSupression.appendTo(conteneurLien);

					// Ajouter les elements de modification
					elementNomModifiable.appendTo(conteneurLien);
					elementModification.appendTo(conteneurLien);

					// Ajouter le tout a la liste affichee
					conteneurListePresets.append(conteneurLien);

				}

			}

		}
		
	}

}

// --- Gestion evenement click sur bouton modifier nom preset dans l'affichage des liens de la fenetre globale
// --- Description : Gere la modification d'un preset d'une categorie
//
$(affichageListePresets).on('click', '.'+cssElementButtonModifierPreset, function() {

	// Recuperer le conteneur
	var divPreset = $(this).parent('.'+conteneurLienPreset);

	// Recuperer la lien contenant le nom affichable
	var lienNomAffichable = divPreset.find('.'+lienPreset);

	// Recuperer la balise contenant le nom modifiable
	var baliseNomModifiable = divPreset.find('.'+cssElementInputNomPreset);

	// Changer la visibilite des balises
	lienNomAffichable.css('display', 'none');
	baliseNomModifiable.css('display', 'inline');

});

// --- Gestion evenement validation de modifier d'un preset par le champ input (fenetre globale)
// --- Description : Gere la modification reelle du nom d'un preset en validant l'input text dans la fenetre globale (liens des presents d'une categorie)
//
$(affichageListePresets).on('keypress', '.'+cssElementInputNomPreset, function(event) {

	// Verifier si la touche est 'Enter'
	if(event.which == 13 || event.keyCode == 13){

		// Traiter la validation
		traitementEventValidationChampNomPreset(this, 0);

	}

});

// --- Gestion evenement validation de modifier d'un preset par le champ input (fenetre globale))
// --- Description : Gere la modification reelle du nom d'un preset en validant l'input text par blur (click sur autre endroit et perde de focalisation du champ) dans la fenetre globale (liens des presents d'une categorie)
//
$(affichageListePresets).on('blur', '.'+cssElementInputNomPreset, function(event) {

	// Traiter la validation
	traitementEventValidationChampNomPreset(this, 0);

});


// --- Function traitementEventValidationChampNomPreset
// --- Description : Traite la validation du nom d'un preset dans la fenetre globale ou la fenetre pop-up par l'utilisateur
// --- Argument : obj - Instance de l'objet 'this' recuperee par l'evenement,
// 				  typeFenetre - Type de fenetre par laquelle la modification a ete lancee (0 = fenetre principale, 1 = fenetre pop-up).
// 
function traitementEventValidationChampNomPreset(obj, typeFenetre) {

	// Verifier si le type de la fenetre a ete renseigne
	if(typeFenetre === undefined)
		return;
	if(isNaN(typeFenetre))
		return;
	if(typeFenetre<0 || typeFenetre>1)
		return;

	// Recuperer les elements selon la fenetre par laquelle la modification es lancee
	var lienPresetChoisi, divPreset, idRecup;
	if(typeFenetre === 0){

		// Recuperer le conteneur
		divPreset = $(obj).parent('.'+conteneurLienPreset);

		// Recuperer l'id de la categorie et du preset
		lienPresetChoisi= divPreset.find('.'+lienPreset);
		idRecup = lienPresetChoisi.get()[0].id;

	}
	else if(typeFenetre === 1){

		// Recuperer le conteneur
		divPreset = $(obj).parent('.'+cssElementPreset);

		// Recuperer l'id de la categorie et du preset
		idRecup = divPreset.get()[0].id;

	}
	var idCategorie = idRecup.split('-')[0];
	var idPreset = idRecup.split('-')[1];

	// Recuperer la balise contenant le nom affichable
	var baliseNomAffichable;
	if(typeFenetre === 1)
		baliseNomAffichable = divPreset.find('.'+cssElementNomPreset);

	// Recuperer la balise contenant le nom modifiable
	var baliseNomModifiable = divPreset.find('.'+cssElementInputNomPreset);

	// Recuperer la categorie
	var categorie = getCategoriePresetsId(idCategorie);

	// Verifier si la categorie existe
	if(categorie !== -1){

		// Recuperer le preset
		var preset = categorie.getPresetById(idPreset);

		// Verifier si le preset existe
		var nouveauNom;
		if(preset !== -1){

			// Recuperer le nouveau nom
			nouveauNom = baliseNomModifiable.val();

			// Verifier si le nom est valide
			if(nouveauNom.length <=0)
				nouveauNom = preset.getNom();

			// Changer le nom de la categorie
			preset.setNom(nouveauNom);

			// Changer le nom affichable
			if(typeFenetre === 0)
				lienPresetChoisi.text(nouveauNom);
			else if(typeFenetre === 1)
				baliseNomAffichable.text(nouveauNom);

			// Regenerer la liste des presets dans la fenetre pop-up
			construireGUIPresetsPopUp();

			// Reinitialiser l'affichage des presets d'une categorie dans le pop-up (au cas ou elle n'est pas vide)
			reinitialiserGUIAffichagePresetsCategorie();

			// Regenerer la liste des liens de la categorie choisie
			if(typeFenetre === 1){
				var listeCategoriesChoix = $(categorieChoix);
				relisterLiensPresets(listeCategoriesChoix.get()[0]);
			}

			// Modifier le nom du preset dans les informations affichees dans la fenetre principale
			renseignerNomPresetAffichage();

		}

	}

	// Changer la visibilite des balises
	if(typeFenetre === 0)
		lienPresetChoisi.css('display', 'inline');
	else if(typeFenetre === 1)
		baliseNomAffichable.css('display', 'inline');
	baliseNomModifiable.css('display', 'none');

	// Sauvegarder la structure
	saveStructure();

}

// --- Fonction redimensionnerConteneursFenetre
// Description : Redimensionne les conteneurs selon la taille de la fenetre
//
function redimensionnerConteneursFenetre() {

	// --- Calculer la taille des conteneurs selon la taille de l'ecran --- //
	var hauteurEcran = Math.floor(window.innerHeight);	//innerHeight pour recuperer la hauteur reelle
	var largeurEcran = Math.floor(window.innerWidth);	//innerWidth pour recuperer la largeur reelle

	// Indiquer a la section generale la taille de l'ecran
	$("#sectionGenerale").css('height', hauteurEcran);
	$("#sectionGenerale").css('width', largeurEcran);

	// Recuperer la taille maximale des graphiques des pedals (pour tenter de reduire la taille de l'affichage)
	var hauteurMaxPedals = 0, largeurMaxPedals = 0, hauteurPedalCourant = 0, largeurPedalCourant = 0;
	for(var i=0; i<presetCourant.getGPedals().length; i++){

		// Recuperer la position du pedal courant
		hauteurPedalCourant = presetCourant.getGPedals()[i].getPosition().getY() + $(presetCourant.getGPedals()[i].getDiv()).outerHeight();
		largeurPedalCourant = presetCourant.getGPedals()[i].getPosition().getX() + $(presetCourant.getGPedals()[i].getDiv()).outerWidth();

		// Recuperer la taille si elle est plus grande
		if(hauteurMaxPedals<hauteurPedalCourant)
			hauteurMaxPedals = hauteurPedalCourant;

		if(largeurMaxPedals<largeurPedalCourant)
			largeurMaxPedals = largeurPedalCourant;
	}

	// Recuperer la hauteur et largeur de la section des pedals
	var sectionPedalsHauteur = sectionPedals.height();//sectionPedals.get()[0].scrollHeight;
	var sectionPedalsLargeur = sectionPedals.width();//sectionPedals.get()[0].scrollWidth;

	// Verifier quel point est le plus eloigne
	if(sectionPedalsHauteur<hauteurMaxPedals){
		sectionPedalsHauteur = hauteurMaxPedals;
		sectionPedals.css('overflow-y', 'auto');
	}
	else
		sectionPedals.css('overflow-y', 'hidden');

	if(sectionPedalsLargeur<largeurMaxPedals){
		sectionPedalsLargeur = largeurMaxPedals;
		sectionPedals.css('overflow-x', 'auto');
	}
	else
		sectionPedals.css('overflow-x', 'hidden');

	// Redimensionner le conteneur des pedals pour s'agrandir au cas de zoom in
	conteneurPedals.css('height',sectionPedalsHauteur);
	conteneurPedals.css('width',sectionPedalsLargeur); 

}

// --- Fonction redimensionnerComposantsApplication
// Description : Redimensionne les composants generaux de la fenetre
//
function redimensionnerComposantsApplication() {

	/** REDIMENSIONNER LE MENU **/
	/****************************/

	// Recuperer le button menu
	var menu = $(".zoneMenu");

	// Recuperer la hauteur du conteneur du menu
	var hHeader = $("header").css('height');

	// Redimensionner le button du menu
	menu.css('height', hHeader);
	menu.css('top', 0);
	menu.css('bottom', 0);

}

// === Evenements jsPlumb === //
// ========================== //
jspInstance.ready(function() {
	//console.log("Il marche");
	//jsPlumb.setContainer($("#affichagePedals"));
	//jsPlumb.setContainer(document.getElementById("affichagePedals"));

	// --- Gestion evenement doubleclick sur un pedal qui n'est ni celui du debut, ni celui de la fin
	//
	conteneurPedals.delegate(".divPedalNormal", "dblclick", function() {//affichagePedals

		// Verifier si la div existe (corriger bug double demande, fonctionne sans car passe de on a delegate en d'hors du ready se trouvant dans ajouterPedal)
		var pExiste = presetCourant.pedalExiste(this);

		// Recuperer l'instance de la div
		var tmpThis = this;

		// Demander confirmation de suppression du pedal
		if(pExiste)
			bootbox.confirm("Remove Effect?", function(result) {
				
				// Detacher si la demande est confirmee
				if(result){

					/* Retirer toutes les connexions audio */
					/***************************************/

					// Recuperer la gPedal
					var gPedal = presetCourant.getGPedalFromDiv(tmpThis);

					// Retirer toutes les connexions d'audio de la pedal
					try{
						gPedal.deconnecterToutesConnexionsAudio();
					}
					catch(err){ console.log(err.toString()); }

					// Retirer le GBasePedal
					presetCourant.retirerGBB(tmpThis);

					// Effacer la vue du conteneur des potentiometres
					resetConteneurPotentiometres();

					/* Retirer la gPedal */
					/*********************/
					var pedalSource = gPedal.getPedal().getAudioPedal();
					if(pedalSource != undefined){ 
						pedalSource.removeEffect(); 
			        }

			        // MB
					saveStructure();

				}

			});

	});

});

// Evenement creation de connexion
// 
jspInstance.bind("connection", jspEventConnecion);
function jspEventConnecion(connInfo, originalEvent) {

    // Connecter le lien
    if(JSPconnexionMoved){
    	JSPconnexionMoved = false;
		connexionLienJSP(connInfo, false);
	}
	else
		connexionLienJSP(connInfo, true);


    // MB
	saveStructure();

}

// --- Function connexionLienJSP
// --- Description : Gere la connexion du lien JS Plumb entre les pedales
// --- Argument : connInfo : Represente la connexion qui connecte les endpoints des pedales,
// 				  attach : Indique si on doit lancer l'attachement de l'overlay de la connexion
// 
function connexionLienJSP(connInfo, attach){


	// Recuperer le GPedal source
    var GPS = presetCourant.getGPedalFromDiv(connInfo.source);

    // Recuperer le GPedal target
    var GPT = presetCourant.getGPedalFromDiv(connInfo.target);

    // Supprimer la connexion (fireEvent pour stopper la propagation d'evenement)
	if(attach === undefined || attach !== false)
		// Ajouter le style fleche pour la connexion
	    if(typeof(overlayStyle) !== 'undefined')
	    	jspInstance.select(connInfo).addOverlay(overlayStyle);

    // Ajouter le lien (de class) du target dans le source
    GPS.ajouterSuccesseur(GPT.getId());

    // Ajouter le lien (de class) du source dans le target
    GPT.ajouterPredecesseur(GPS.getId());

    // Lier l'audio entre les pedals
    var pedalSource = GPS.getPedal();
    var pedalTarget = GPT.getPedal();
    pedalSource.connecterAudio(pedalTarget); /* TODO Bug connexion Filter */

}

// Evenement retirement d'une connexion
// 
jspInstance.bind("connectionDetached", jspEventDetached);
function jspEventDetached(conn, epe){
	
	// Verifier si la connexion existe reellement [CORRECTION BUG CLICK SUR ENDPOINT SANS CONNEXION]
	if(conn.source != undefined && conn.target != undefined){

		// Deconnecter le lien
		deconnexionLienJSP(conn, false);

	}


    // MB
	saveStructure();


}

// Evenement deplacement d'une connexion d'une pedale A vers pedale B
// 
jspInstance.bind("connectionMoved", jspEventMoved);
function jspEventMoved(conn, epe){

	// Indiquer au variable d'etat qu'on souhaite faire un deplacenent et non une creation
	JSPconnexionMoved = true;

	// Creer le tableau contenant la connexion originale
	var connexionOriginale = {source:$('#'+conn.originalSourceId)[0], target:$('#'+conn.originalTargetId)[0]};

	// Creer le tableau contenant la nouvelle connexion
	//var connexionNouvelle = {source:$('#'+conn.newSourceId)[0], target:$('#'+conn.newTargetId)[0]};

	// Verifier si la connexion existe reellement [CORRECTION BUG CLICK SUR ENDPOINT SANS CONNEXION]
	if(connexionOriginale.source != undefined && connexionOriginale.target != undefined){
		
		// Deconnecter le lien
		deconnexionLienJSP(connexionOriginale, false);

	}

	// Verifier si la connexion existe reellement [CORRECTION BUG CLICK SUR ENDPOINT SANS CONNEXION]
	/*if(connexionNouvelle.source != undefined && connexionNouvelle.target != undefined){
		
		// Connecter le lien
		connexionLienJSP(connexionNouvelle, false);

	}*/


    // MB
	saveStructure();


}

// --- Gestion evenement click sur connexion (suppression effective)
// 
jspInstance.bind("click", jspEventClick);
function jspEventClick(conn) {

	// Verifier si la connexion existe reellement [CORRECTION BUG CLICK SUR ENDPOINT SANS CONNEXION]
	if(conn.source != undefined && conn.target != undefined)

		// Demander confirmation de suppression de la connexion (Bug connu : Sous Firefox, bootbox declanche un bug qui empeche la connexion de l'audio 1/2 fois)
		//bootbox.confirm("Êtes vous sûr de vouloir supprimer la connexion?", function(result) {
			
			// Detacher si la demande est confirmee
			//if(result){

				// Deconnecter le lien
				deconnexionLienJSP(conn);
				
			//}

		//}); 
		//
		
    // MB
	saveStructure();


}

// --- Function deconnexionLienJSP
// --- Description : Gere la deconnexion des liens JS Plumb entre les pedales
// --- Argument : conn : Represente la connexion qui connecte les endpoints des pedales,
// 				  detach : Indique si on doit lancer le detachement de la connexion (pour eviter de detacher une connexion non attachee)
// 
function deconnexionLienJSP(conn, detach){

	// Verifier si la connexion existe reellement [CORRECTION BUG CLICK SUR ENDPOINT SANS CONNEXION]
	if(conn.source === undefined || conn.target === undefined || (conn.source === undefined && conn.target === undefined))
		return -1;

	// Recuperer la source
	var src = conn.source;

	// Recuperer la destination
	var trg = conn.target;

	// Retirer le lien respectif
	presetCourant.retirerLienGBB(src, trg);

	// Supprimer la connexion (fireEvent pour stopper la propagation d'evenement)
	if(detach === undefined || detach === true)
		jspInstance.detach(conn, {fireEvent:false});

	/**** Gerer la deconnexion de l'audio ****/
	/*****************************************/

	// Recuperer le GPedal source
	var GPS = presetCourant.getGPedalFromDiv(src);

	// Recuperer le GPedal target
	var GPT = presetCourant.getGPedalFromDiv(trg);

	// Lier l'audio entre les pedals
	var pedalSource = GPS.getPedal();
	var pedalTarget = GPT.getPedal();

	try{
		pedalSource.deconnecterAudio(pedalTarget);

		// Reconnecter toutes les connexions suivantes (correction bug Firefox - car disconnect deconnect TOUS les noeuds suivants)
		GPS.connecterToutesConnexionsAudio();
	}
	catch(err){ console.log(err.toString()); }

}

// === /Fin Evenements jsPlumb === //
// =============================== //


// --- Gestion evenement click sur un lien d'un preset d'une categorie
//
$(affichageListePresets).on('click', '.'+lienPreset, function() {

	// Traiter l'affichage du preset par l'id du lien
	affichagePresetDeCategorieParIdCompose(this);

});

// --- Function affichagePresetDeCategorieParIdCompose
// --- Description : Traite l'affichage d'un preset issu d'une categorie grace a l'id compose (id de la categorie - id du preset) du lien vers le preset
// --- Argument : lienVersPreset - Le lien vers le preset a afficher
// 
function affichagePresetDeCategorieParIdCompose(lienVersPreset) {

	// Recuperer l'id du lien
	var idLien = lienVersPreset.id;

	// Recuperer l'id de la categorie et du preset
	var idCat = idLien.split('-')[0];
	var idPres = idLien.split('-')[1];

	// Verifier si les id sont recuperes
	if(idCat !== undefined && idPres !== undefined){

		// Verifier si l'id de la categorie correspond a celui de la categorie courante
		var categorieChoisie;
		if(categorieCourante !== undefined && categorieCourante instanceof CategoriePresets)
			if(categorieCourante.getId() == idCat)
				categorieChoisie = categorieCourante;
			// Sinon, trouver la categorie
			else
				categorieChoisie = getCategoriePresetsId(idCat);
		// Sinon, trouver la categorie
		else
			categorieChoisie = getCategoriePresetsId(idCat);

		// Verifier si la categorie a ete trouvee
		var categorieCouranteTmp;
		if(categorieChoisie !== undefined || categorieChoisie != -1){

			// Indiquer que la categorie courante est la categorie choisie et temporiser la courante en cas de preset non existant
			categorieCouranteTmp = categorieCourante;
			categorieCourante = categorieChoisie;

			// Recuperer le preset de la categorie courante
			var preset = categorieCourante.getPresetById(idPres);

			// Verifier si le preset a ete trouve
			if(preset != -1){

				// Deconnecter la structure audio interne du preset avant changement
				try{
					presetCourant.deconnecterAudioPreset();
				}
				catch(err){ console.log(err.toString()); }

				// Indiquer le preset courant
				presetCourant = preset;

				// Restaurer la vue du preset
				restaurerVuePreset();

				// Reconnecter la structure audio interne du preset apres changement
				presetCourant.connecterAudioPreset();

				// --- Modifier le style du lien --- //
				// --------------------------------- //

				// Recuperer le contneur
				var conteneur = $(affichageListePresets);

				// Verifier si le conteneur existe et modifier le style des liens
				if(conteneur !== undefined){

					// Recuperer les liens
					var liensFilles = conteneur.find('.' + lienPreset);

					// Verifier si les liens sont trouvees
					if(liensFilles !== undefined)
						for(var i=0; i<liensFilles.length; i++)
							$(liensFilles[i]).css('font-weight', 'normal');

				}

				// Modifier le style du lien choisi
				$(lienVersPreset).css('font-weight', 'bold');

				// Renseigner le nom du preset dans l'affichage
				var nomPresetChoisi = preset.getNom();
				$(nomPresetAffichable).text(nomPresetChoisi);
				$(nomPresetModifiable).val(nomPresetChoisi);

			}
			// En cas d'un preset non existant, recuperer l'ancienne categorie courante 
			else
				categorieCourante = categorieCouranteTmp;

		}
		
	}

}

// --- Gestion evenement creation d'un nouveau pedal lors d'un click sur un nom de pedal de la liste
// Alert : L'evenement 'click' sur un element 'Option' n'est pas pris en charge sous Chrome, donc obligatoir de passer a 'change'
//
$(listeChoixPedal).on('change', function() {

	// Recuperer le type
	var type = this.options[this.selectedIndex].text;

	// Ajouter le pedal
	presetCourant.creerGBasePedal(type);

    // MB: reset the menu to initial state after a selection has been done
	$(listeChoixPedal).get(0).selectedIndex = 0;

	// MB
	saveStructure();

});

// --- Gestion evenement click sur bouton creation d'un nouveau preset
//
$(buttonNouveauPreset).on('click', function() {

	// Recuperer la liste des categories
	var tab = categoriesPresets;

	// Recuperer la categorie Courante si elle existe
	var categorieChoisie = categorieCourante;
	if(categorieChoisie === undefined)
		// Verifier si la liste existe
		if(tab !== undefined && tab[0] !== undefined)
			// Recuperer la categorie initiale
			categorieChoisie = tab[0];

	// Verifier si la recuperation a reussi
	if(categorieChoisie !== undefined && categorieChoisie instanceof CategoriePresets){

		// Creer un nouveau preset
		var preset = categorieChoisie.creerPreset();

		// Deconnecter la structure audio interne du preset avant changement
		try{
			presetCourant.deconnecterAudioPreset();
		}
		catch(err){ console.log(err.toString()); }

		// Indiquer le nouveau preset en tant que preset courant
		presetCourant = preset;

		// Regenerer l'affichage du preset
		reinitialiserAffichagePedals();
		presetCourant.initialiserVuePedals();

		// Restaurer les evenements de jsPlumb
		restaurerEvenementsJSP();

		// Regenerer la liste des liens de la categorie choisie
		relisterLiensPresets($(categorieChoix).get()[0]);

		// Regenerer la liste des presets dans la fenetre pop-up
		construireGUIPresetsPopUp();

		// Reinitialiser l'affichage des presets d'une categorie dans le pop-up (au cas ou elle n'est pas vide)
		reinitialiserGUIAffichagePresetsCategorie();

	}


    // MB
	saveStructure();


});

// --- Gestion evenement click sur bouton sauvegarde
//
$(boutonSauvegarde).on('click', function() {

	// Appeler la fonction de sauvegarde
	saveStructure();

});


// --- Gestion evenement click sur bouton restaurer
//
$(boutonRestaurer).on('click', function() {

	// Appeler la fonction de restauration
	loadStructure();

});

// --- Gestion evenement click sur bouton Nouvelle categorie
// --- Description : Gere la creation d'une nouvelle categorie
//
$(buttonNouvelleCategorie).on('click', function() {

	// Reinitialiser l'affichage des pedals
	reinitialiserAffichagePedals();

	// Initialiser la categorie a creer
	var cp = initialiserStructure();

	// // Creer une nouvelle categorie
	// var cp = creerCategoriePresets();

	// // Creer un preset a la categorie
	// var preset = cp.creerPreset();

	// // Indiquer les elements courants (TOTO : MODIFIER APRES RELOCALISATION DES FONCTIONS CONCERNANT LES PRESETS DANS LA CLASSE PRESET)
	// categorieCourante = cp;
	// presetCourant = preset;

	// // Initialiser le preset 
	// presetCourant.initialiserVuePedals();

	// Restaurer les evenements de jsPlumb
	restaurerEvenementsJSP();

	// Regenerer la GUI de la liste des categories
	construireGUIGestion();

	// Renseigner le nom du preset dans l'affichage
	renseignerNomPresetAffichage();

	// --- Mettre a jour la liste des categories selectionnables --- //
	// ------------------------------------------------------------- //
	
	// Recuperer la liste affichable des categories
	var listeCategoriesChoix = $(categorieChoix);

	// Recuperer l'id de la categorie et modifier la categorie selectionnee de la liste
	var catId = cp.getId();
	
	// Recuperer l'option a selectionner
	listeCategoriesChoix.find("option[value='" + catId + "']").attr('selected','selected');

	//$('select[name=selValue]').val(1);
	$(listeCategoriesChoix).selectpicker('refresh');

	// Regenerer la liste des liens de la categorie choisie
	relisterLiensPresets(listeCategoriesChoix.get()[0]);


    // MB
	saveStructure();


});

// --- Gestion evenement click sur un nom categorie (fenetre Pop-up)
// --- Description : Gere l'affichage des presets d'une categorie (dans fenetre pop-up)
//
listeCategories.on('click', '.'+ cssElementNomCategorie, function() {
	
	// Recuperer le conteneur
	var divCategorie = $(this).parent('.'+cssElementCategorie);

	// Recuperer l'id de la categorie
	var idCategorie = divCategorie.get()[0].id;

	// Generer la liste des presets de la categorie
	construireGUIPresetsDeCategoriePopUp(idCategorie);

});

// --- Gestion evenement click sur bouton supprimer categorie
// --- Description : Gere la suppression d'une categorie
//
listeCategories.on('click', '.'+cssElementButtonSupprimerCategorie, function() {

	// Recuperer le conteneur
	var divCategorie = $(this).parent('.'+cssElementCategorie);

	// Recuperer l'id de la categorie
	var idCategorie = divCategorie.get()[0].id;

	// Initialiser la variable contenant la liste des liens des categories 
	var listeCategoriesChoix = $(categorieChoix);

	// Verifier si la categorie est la categorie courante
	if(categorieCourante !== undefined && categorieCourante instanceof CategoriePresets)
		if(categorieCourante.getId() == idCategorie){

			// Recuperer l'instance de la categorie initiale
			var CI = categoriesPresets;

			// Verifier s'il existe
			if(CI !== undefined && CI[0] !== undefined){

				// Rensigner la categorie courante
				categorieCourante = CI[0];

				// Recuperer un preset initial
				if(CI[0].getPresets()[0] !== undefined && CI[0].getPresets()[0] instanceof Preset){

					// Deconnecter la structure audio interne du preset avant changement
					try{
						presetCourant.deconnecterAudioPreset();
					}
					catch(err){ console.log(err.toString()); }

					// Rensigner le courant
					presetCourant = CI[0].getPresets()[0];

					// Reconnecter la structure audio interne du preset apres changement
					presetCourant.connecterAudioPreset();

					// Selectionner la 1ere categorie avant regeneration de la liste
					$(categorieChoix + " option:first").prop('selected', 'selected');
					listeCategoriesChoix.selectpicker('refresh');

					// Regenerer la liste des liens de la categorie choisie
					relisterLiensPresets(listeCategoriesChoix.get()[0]);

				}

			}

		}

	// Supprimer la categorie
	supprimerCategoriePresets(idCategorie);

	// Regenerer la GUI de la liste des categories
	//construireGUIGestion(); //

	// Construire la partie contenant la gestion des categories
	construireGUICategoriesPopUp();

	// Construire la partie contenant la gestion des presets
	construireGUIPresetsPopUp();

	// Reinitialiser l'affichage des presets d'une categorie
	reinitialiserGUIAffichagePresetsCategorie();

	// Recuperer l'option representant la categorie supprimee de la liste
	var optionCategorieSupp = $(categorieChoix).find("option[value='"+idCategorie+"']");
	optionCategorieSupp.remove();
	listeCategoriesChoix.selectpicker('refresh');

	// Restaurer la vue du preset
	restaurerVuePreset();


    // MB
	saveStructure();


});


// --- Gestion evenement click sur bouton modifier categorie
// --- Description : Gere la modification d'une categorie
//
listeCategories.on('click', '.'+cssElementButtonModifierCategorie, function() {

	// Recuperer le conteneur
	var divCategorie = $(this).parent('.'+cssElementCategorie);

	// Recuperer la balise contenant le nom affichable
	var baliseNomAffichable = divCategorie.find('.'+cssElementNomCategorie);

	// Recuperer la balise contenant le nom modifiable
	var baliseNomModifiable = divCategorie.find('.'+cssElementInputNomCategorie);

	// Changer la visibilite des balises
	baliseNomAffichable.css('display', 'none');
	baliseNomModifiable.css('display', 'inline');
	

    // MB
	saveStructure();


});

// --- Gestion evenement validation de modifier d'une categorie par le champ input
// --- Description : Gere la modification reelle d'une categorie en validant l'input text par selection d'un bouton clavier
//
listeCategories.on('keypress', '.'+cssElementInputNomCategorie, function(event) {

	// Verifier si la touche est 'Enter'
	if(event.which == 13 || event.keyCode == 13){

		// Traiter la validation
		traitementEventValidationNomCategorie(this);

	}

});

// --- Gestion evenement validation de modifier d'une categorie par le champ input
// --- Description : Gere la modification reelle d'une categorie en validant l'input text par blur (click ailleurs)
//
listeCategories.on('blur', '.'+cssElementInputNomCategorie, function(event) {

	// Traiter la validation
	traitementEventValidationNomCategorie(this);

});

// --- Function traitementEventValidationNomCategorie
// --- Description : Traite la validation du nom d'une categorie par l'utilisateur
// --- Argument : obj - Instance de l'objet 'this' recuperee par l'evenement
// 
function traitementEventValidationNomCategorie(obj) {

	// Recuperer le conteneur
	var divCategorie = $(obj).parent('.'+cssElementCategorie);

	// Recuperer l'id de la categorie
	var idCategorie = divCategorie.get()[0].id;

	// Recuperer la balise contenant le nom affichable
	var baliseNomAffichable = divCategorie.find('.'+cssElementNomCategorie);

	// Recuperer la balise contenant le nom modifiable
	var baliseNomModifiable = divCategorie.find('.'+cssElementInputNomCategorie);

	// Recuperer la categorie
	var categorie = getCategoriePresetsId(idCategorie);

	// Verifier si la categorie existe
	var nouveauNom;
	if(categorie !== -1){

		// Recuperer le nouveau nom
		nouveauNom = baliseNomModifiable.val();

		// Verifier si le nom est valide
		if(nouveauNom.length <=0)
			nouveauNom = categorie.getNom();

		// Changer le nom de la categorie
		categorie.setNom(nouveauNom);

		// Changer le nom affichable
		baliseNomAffichable.text(nouveauNom);

		// Modifier le nom de l'option representant la categorie modifiee
		var listeCategoriesChoix = $(categorieChoix);
		var optionCategorieSupp = $(categorieChoix).find("option[value='"+idCategorie+"']");
		optionCategorieSupp.text(nouveauNom);
		listeCategoriesChoix.selectpicker('refresh');

		// Reinitialiser l'affichage des presets d'une categorie dans le pop-up (au cas ou elle n'est pas vide)
		reinitialiserGUIAffichagePresetsCategorie();

	}

	// Changer la visibilite des balises
	baliseNomAffichable.css('display', 'inline');
	baliseNomModifiable.css('display', 'none');

	// Sauvegarder la structure (TODODO3)
	saveStructure();

}

// --- Gestion evenement click sur le nom du preset affichable sur la section pedals
// --- Description : Gere la modification du nom d'un preset
//
$(nomPresetAffichable).on('click', function() {

	// Recuperer la balise contenant le nom modifiable
	var baliseNomModifiable = $(nomPresetModifiable);

	// Changer la visibilite des balises
	$(this).css('display', 'none');
	baliseNomModifiable.css('display', 'inline');
	

});

// --- Gestion evenement validation de modifier du nom d'un preset par le champ input
// --- Description : Gere la modification reelle du nom d'un preset en validant l'input text
//
$(nomPresetModifiable).on('keypress', function(event) {

	// Verifier si la touche est 'Enter'
	if(event.which == 13 || event.keyCode == 13){

		// Traiter la validation
		traitementEventValidationNomPreset(this);

	}

});

// --- Gestion evenement validation de modifier du nom d'un preset par le champ input
// --- Description : Gere la modification reelle du nom d'un preset en validant l'input text par blur (click sur autre endroit et perde de focalisation du champ)
//
$(nomPresetModifiable).on('blur', function(event) {

	// Traiter la validation
	traitementEventValidationNomPreset(this);

});

// --- Function traitementEventValidationNomPreset
// --- Description : Traite la validation du nom d'un preset par l'utilisateur
// --- Argument : obj - Instance de l'objet 'this' recuperee par l'evenement
// 
function traitementEventValidationNomPreset(obj) {

	// Recuperer le preset
	var unPreset = presetCourant;

	// Recuperer la balise contenant le nom affichable
	var baliseNomAffichable = $(nomPresetAffichable);

	// Verifier si la categorie existe
	var nouveauNom;
	if(unPreset !== undefined && unPreset instanceof Preset){

		// Recuperer le nouveau nom
		nouveauNom = $(obj).val();

		// Verifier si le nom est valide
		if(nouveauNom.length <=0)
			nouveauNom = unPreset.getNom();

		// Changer le nom de la categorie
		unPreset.setNom(nouveauNom);

		// Changer le nom affichable
		baliseNomAffichable.text(nouveauNom);

		// Changer le nom modifiable
		$(obj).val(nouveauNom);

		// Recuperer le lien pour une mise a jour
		if(categorieCourante !== undefined && categorieCourante instanceof CategoriePresets)
			if(presetCourant !== undefined && presetCourant instanceof Preset){

				// Recuperer les ids
				var idCat = categorieCourante.getId();
				var idPres = presetCourant.getId();

				// Recuperer le lien
				var lien = $('#'+idCat+'-'+idPres);

				// Modifier le nom si lien trouve
				if(lien !== undefined)
					lien.text(nouveauNom);

			}

	}

	// Regenerer la liste des presets dans la fenetre pop-up
	construireGUIPresetsPopUp();

	// Reinitialiser l'affichage des presets d'une categorie dans le pop-up (au cas ou elle n'est pas vide)
	reinitialiserGUIAffichagePresetsCategorie();

	// Regenerer l'affichage de la liste des categories a selectionner et les liens
	regenererCategorieChoixSelonCategorieCorrespondants();
	relisterLiensPresets($(categorieChoix).get()[0]);

	// Changer la visibilite des balises
	baliseNomAffichable.css('display', 'inline');
	$(obj).css('display', 'none');

	// Sauvegarder la structure (TODODO3)
	saveStructure();

}

// --- Gestion evenement click sur bouton supprimer preset dans la fenetre deu pop-up
// --- Description : Gere la suppression d'un preset dans la fenetre deu pop-up
//
listePresetsGlobaux.on('click', '.'+cssElementButtonSupprimerPreset, function() {

	// Recuperer le conteneur
	var divPreset = $(this).parent('.'+cssElementPreset);

	// Recuperer l'id (contient l'id de la categories et du preset)
	var idRecup = divPreset.get()[0].id;

	// Recuperer l'id de la categorie et du preset
	var idCategorie = idRecup.split('-')[0];
	var idPreset = idRecup.split('-')[1];

	// Supprimer le preset et arrnger les vues
	supprimerPresetAffichageGUIByIds(idCategorie, idPreset);


    // MB
	saveStructure();


});

// --- Gestion evenement click sur bouton supprimer preset d'une categorie dans la fenetre du pop-up
// --- Description : Gere la suppression d'un preset issue d'une categorie dans la fenetre du pop-up
//
listePresetsCategorieChoisie.on('click', '.'+cssElementButtonSupprimerPresetCategorie, function() {

	// Recuperer le conteneur
	var divPreset = $(this).parent('.'+cssElementPresetCategorie);

	// Recuperer l'id (contient l'id de la categories et du preset)
	var idRecup = divPreset.get()[0].id;

	// Recuperer l'id de la categorie et du preset
	var idCategorie = idRecup.split('-')[0];
	var idPreset = idRecup.split('-')[1];

	// Supprimer le preset et arrnger les vues
	supprimerPresetAffichageGUIByIds(idCategorie, idPreset);


    // MB
	saveStructure();


});

// --- Fonction supprimerPresetAffichageGUIByIds
// --- Description : Permet de supprimer un preset selon les ids donnes en argument tout en arrangant les vues
// --- Argument : idCategorie - Contient l'id de la categorie dont est issu le preset a supprimer
// 				  idPreset - Contient l'id du preset a supprimer
//
function supprimerPresetAffichageGUIByIds(idCategorie, idPreset){

	// Verifier la validite des arguments
	if(idCategorie === undefined || idPreset === undefined || (idCategorie === undefined && idPreset === undefined))
		return;

	// Verifier si la categorie est la categorie courante
	var categorieEstCourante = false, presetEstCourant = false;	// Pour pouvoir arranger la verification.
	if(categorieCourante !== undefined && categorieCourante instanceof CategoriePresets)
		if(categorieCourante.getId() == idCategorie){

			// Indiquer que la categorie est courante
			categorieEstCourante = true;

			// Verifier si le preset est le preset courant
			if(presetCourant !== undefined && presetCourant instanceof Preset)
				if(presetCourant.getId() == idPreset)
					// Indiquer que le preset est courant
					presetEstCourant = true;

		}

	// Initialiser la variable contenant la liste visible des liens des presets de la categorie
	var listeCategoriesChoix = $(categorieChoix);

	// Decider de l'action selon la recherche precedante
	if(categorieEstCourante){

		// Supprimer le preset
		categorieCourante.supprimerPresetById(idPreset);

		// Verifier si le preset est le courant
		if(presetEstCourant){

			// Recuperer l'instance de la categorie initiale
			var CI = categoriesPresets;

			// Verifier s'il existe
			if(CI !== undefined && CI[0] !== undefined){

				// Rensigner la categorie courante
				categorieCourante = CI[0];

				// Recuperer un preset initial
				if(CI[0].getPresets()[0] !== undefined && CI[0].getPresets()[0] instanceof Preset){

					// Deconnecter la structure audio interne du preset avant changement
					try{
						presetCourant.deconnecterAudioPreset();
					}
					catch(err){ console.log(err.toString()); }

					// Rensigner le courant
					presetCourant = CI[0].getPresets()[0];

					// Reconnecter la structure audio interne du preset apres changement
					presetCourant.connecterAudioPreset();

					// Selectionner la 1ere categorie avant regeneration de la liste
					$(categorieChoix + " option:first").prop('selected', 'selected');
					listeCategoriesChoix.selectpicker('refresh');

				}

			}

		}

	}
	else{

		// Recuperer la categorie du preset
		var categorieChoisie = getCategoriePresetsId(idCategorie);

		// Verifier que la categorie a ete trouvee
		if(categorieChoisie !== undefined && categorieChoisie != -1){

			// Supprimer le preset
			categorieChoisie.supprimerPresetById(idPreset);

		}

	}

	// Regenerer la liste des presets dans la fenetre pop-up
	construireGUIPresetsPopUp();

	// Reinitialiser l'affichage des presets d'une categorie dans le pop-up (au cas ou elle n'est pas vide)
	reinitialiserGUIAffichagePresetsCategorie();

	// Regenerer la liste des liens de la categorie choisie
	relisterLiensPresets(listeCategoriesChoix.get()[0]);

	// Restaurer la vue du preset
	restaurerVuePreset();

}

// --- Gestion evenement click sur bouton supprimer preset d'une categorie dans la fenetre deu pop-up
// --- Description : Gere la suppression d'un preset issue d'une categorie dans la fenetre deu pop-up
//
listePresetsCategorieChoisie.on('click', '.'+cssElementButtonSupprimerPresetCategorieImporte, function() {

	// Recuperer le conteneur
	var divPreset = $(this).parent('.'+cssElementPresetCategorieImporte);

	// Recuperer l'id (contient l'id de la categories et du preset)
	var idRecup = divPreset.get()[0].id;

	// Recuperer l'id de la categorie et du preset
	var idCategorie = idRecup.split('-')[0];
	var idPreset = idRecup.split('-')[1];

	// Verifier si l'id de la categorie de laquelle il faut retirer le lien est indiquee
	if(idCategorieChoisie === undefined)
		return;

	// Recuperer la categorie du preset
	var categorieChoisie = getCategoriePresetsId(idCategorieChoisie);

	// Verifier si la categorie a ete trouvee
	if(categorieChoisie === undefined || categorieChoisie == -1 || !(categorieChoisie instanceof CategoriePresets) )
		return;

	// Retirer le lien du preset importe et arrnger les vues
	categorieChoisie.retirerPresetImporte(idCategorie, idPreset);


	/** RECONSTRUCTION VISUELLE DU PRESET DANS LE CAS OU LE PRESET ENLEVE EST COURRAMENT AFFICHE **/
	/**********************************************************************************************/

	// Verifier si la categorie est la categorie courante
	var categorieEstCourante = false, presetEstCourant = false;	// Pour pouvoir arranger la verification.
	if(categorieCourante !== undefined && categorieCourante instanceof CategoriePresets)
		if(categorieCourante.getId() == idCategorie){

			// Indiquer que la categorie est courante
			categorieEstCourante = true;

			// Verifier si le preset est le preset courant
			if(presetCourant !== undefined && presetCourant instanceof Preset)
				if(presetCourant.getId() == idPreset)
					// Indiquer que le preset est courant
					presetEstCourant = true;

		}


	// Decider de l'action selon la recherche precedante
	if(categorieEstCourante){

		// Verifier si le preset est le courant
		if(presetEstCourant){

			// Recuperer l'instance de la categorie initiale
			var CI = categoriesPresets;

			// Verifier s'il existe
			if(CI !== undefined && CI[0] !== undefined){

				// Rensigner la categorie courante
				categorieCourante = CI[0];

				// Recuperer un preset initial
				if(CI[0].getPresets()[0] !== undefined && CI[0].getPresets()[0] instanceof Preset){

					// Deconnecter la structure audio interne du preset avant changement
					try{
						presetCourant.deconnecterAudioPreset();
					}
					catch(err){ console.log(err.toString()); }

					// Rensigner le courant
					presetCourant = CI[0].getPresets()[0];

					// Reconnecter la structure audio interne du preset apres changement
					presetCourant.connecterAudioPreset();

				}

			}

			// Restaurer la vue du preset
			restaurerVuePreset();

		}

	}

	// Regenerer la liste des choix de la categorie et les liens des presets si la categorie selectionnee correspond
	regenererCategorieChoixSelonCategorieCorrespondants(idCategorieChoisie);

	// Reinitialiser l'affichage des presets d'une categorie dans le pop-up (au cas ou elle n'est pas vide)
	reinitialiserGUIAffichagePresetsCategorie();


    // MB
	saveStructure();


});

// --- Fonction regenererCategorieChoixSelonCategorieCorrespondants
// --- Description : Permet de regenerer la liste des choix des categories selon si l'id de la categorie correspond a celui selectionne dans la liste
// --- Argument : idCategorieAComparer - L'id de la categorie a comparer pour le listage
//
function regenererCategorieChoixSelonCategorieCorrespondants(idCategorieAComparer){

	// Recuperer l'id de la categorie selectionnee dans la liste (pour la verification de generation de la liste)
	var idCategorieSelectionnee = $(categorieChoix + " option:selected").attr('value'); 

	// Verifier si la categorie a modifier est celle qui est selectionnee dans la liste
	if(idCategorieAComparer == idCategorieSelectionnee){

		// Reconstruire le GUI
		construireGUIGestion();

		// Regenerer la liste des liens de la categorie choisie
		relisterLiensPresets($(categorieChoix).get()[0]);

	}
	// Generer juste la liste et l'affichage des presets dans le cas contraire
	else {

		// Regenerer la liste de la selection des presets
		construireGUISelection();

		// Repointer vers la categorie
		var listeCategoriesChoix = $(categorieChoix);
		$(categorieChoix + " option[value='" + idCategorieSelectionnee + "']").prop('selected', 'selected');
		listeCategoriesChoix.selectpicker('refresh');

	}

}

// --- Gestion evenement click sur bouton modifier nom preset
// --- Description : Gere la modification d'un preset par le pop-up
//
listePresetsGlobaux.on('click', '.'+cssElementButtonModifierPreset, function() {

	// Recuperer le conteneur
	var divPreset = $(this).parent('.'+cssElementPreset);

	// Recuperer la balise contenant le nom affichable
	var baliseNomAffichable = divPreset.find('.'+cssElementNomPreset);

	// Recuperer la balise contenant le nom modifiable
	var baliseNomModifiable = divPreset.find('.'+cssElementInputNomPreset);

	// Changer la visibilite des balises
	baliseNomAffichable.css('display', 'none');
	baliseNomModifiable.css('display', 'inline');
	

});

// --- Gestion evenement validation de modifier d'un preset par le champ input (fenetre pop-up)
// --- Description : Gere la modification reelle du nom d'un preset en validant l'input text dans la fenetre du pop-up
//
listePresetsGlobaux.on('keypress', '.'+cssElementInputNomPreset, function(event) {

	// Verifier si la touche est 'Enter'
	if(event.which == 13 || event.keyCode == 13){

		// Traiter la validation
		traitementEventValidationChampNomPreset(this, 1);
	}

});

// --- Gestion evenement validation de modifier d'un preset par le champ input (fenetre pop-up)
// --- Description : Gere la modification reelle du nom d'un preset en validant l'input text par blur (click sur autre endroit et perde de focalisation du champ) dans la fenetre du pop-up
//
listePresetsGlobaux.on('blur', '.'+cssElementInputNomPreset, function(event) {

	// Traiter la validation
	traitementEventValidationChampNomPreset(this, 1);



});

// --- Fonction resetConteneurPotentiometres
// Description : Retire tous les elements du conteneur des potentiometres
//
function resetConteneurPotentiometres() {
  
	// Retirer tous les elements
	conteneurPotentiometres.empty();

}

// --- Gestion evenement click sur un GBasePedal (pour afficher les potentiometres)
//
conteneurPedals.delegate(".divPedalNormal", 'click', function() {

	// Restaurer l'etat de l'affichage des potentiometres
	resetConteneurPotentiometres();

	// Recuperer le GBasePedal associe
	var gbp = presetCourant.getGPedalFromDiv(this);

	// Indiquer que le GBasePedal pedal est celui dont on a clique (sert aux evenements sur les potentiometres)
	gPedalCourant = gbp;

	// Creer un titre contenant le nom du pedal
	var titrePedal = $("<h2>" + gbp.getPedal().getType() + "</h2>").attr('class','nomPedal');

	// Creer la zone du header contenant le nom du pedal
	var headerPedal = $("<div></div>").attr('class','zoneHeaderPedal');

	// Ajouter le titre au header du pedal
	titrePedal.appendTo(headerPedal);

	// Ajouter header a l'affichage des potentiometres
	headerPedal.appendTo(conteneurPotentiometres);

	// Recuperer les potentiometres du GBasePedal
	var listeParams = gbp.getPedal().getPotentiometres();

	// --- Construction de l'affichage de chacun des potentiometres du pedal --- //
	// ---------------------------------------------------------------------- //
	var gPotentiometreInfos, nomPotentiometreInfo, gPotentiometreInfo;
	for(var i=0; i<listeParams.length; i++){

		// Creer un conteneur assemblant les elements du potentiometre
		gPotentiometreInfos = $("<div class='gPotentiometreInfos'></div>");

		// Recuperer le nom du potentiometre
		nomPotentiometreInfo = listeParams[i].getNom();

		// Recuperer le graphique du potentiometre
		gPotentiometreInfo = listeParams[i].getGPotentiometre().getGraphique();

		// Ajouter le nom du potentiometre dans les informations affichees
		$("<div class='nomPotentiometreInfo centerText'>" + nomPotentiometreInfo + "</div>").appendTo(gPotentiometreInfos);

		// Ajouter le graphique du potentiometre dans les informations affichees
		$(gPotentiometreInfo).appendTo(gPotentiometreInfos);

		// Ajouter le potentiometre dans le conteneur
		$(gPotentiometreInfos).appendTo(conteneurPotentiometres);

		/* Traiter les potentiometres speciaux */
		/*-------------------------------------*/
		if(listeParams[i] instanceof PotentiometreBoutonEtat)
			gPotentiometreInfos.addClass('gPotentiometreInfosDroite');

	}

	// Valider le Knob
	$(".knob").knob({/* TODO potentiometres et evenements */

		// Evenement Knob : Changer valeur
		'release' : function (v) { 

			// Recuperer l'id
			var id = this.i[0].id;

			// Recuperer l'instance de GPotentiometre
			var gp = getGPotentiometreFromId(gPedalCourant, id);
			
			// Modifier la valeur
			gp.setValeurTraite(v);

			
    // MB
	saveStructure();

		}
	});

});


/* ========================= Autres fonctions ========================== */
/* ===================================================================== */

// --- Fonction isIE
// Description : Permet de verifier si le navigateur courant est Internet Explorer ou pas
// Verification recuperee de http://stackoverflow.com/questions/19999388/jquery-check-if-user-is-using-ie
// 
function isIE(){

	// Verifier le navigateur
	var isIE = false;
	if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
	    isIE = true;   
	}

	// Renvoyer la valeur
	return isIE;
}

// --- Fonction getIdLibre
// --- Description : Fonction permettant de trouver un id non utilise, unique et le plus bas possible (Utilisation seulement pour des tableaux contenant un ensemble d'objets possedant la methode getId)
// --- Argument : tab - tableau contenant les elements
// 				  separateur - signe permettant de separer un id compose (OPTIONNEL MAIS PAS SANS ARGUMENT 3)
// 				  idPrefix - le prefix de l'id de l'element dont on souhaite creer un id (OPTIONNEL MAIS PAS SANS ARGUMENT 2)
//
function getIdLibre(tab, separateur, idPrefix){

	// Verifier si l'argument est valide
	if(tab !== undefined){

		// Verifier si les arguments optionnels sont corrects, sinon, envoyer un code d'erreur
		if(separateur === undefined && idPrefix !== undefined || separateur !== undefined && idPrefix === undefined)
			return -2;

		// Recuperer les id et l'id le plus eleve
		var idMax=0, tabIds = [], idRecup, idsSepares;
		for(var i=0; i<tab.length; i++){

			// Verifier si on utilise les arguments optionnels 
			if(separateur === undefined)
				// Recuperer l'id
				idRecup = tab[i].getId();
			else {

				// Separer l'id
				idsSepares = tab[i].getId().split(separateur);

				//Verifier si le separateur est valide, sinon, envoyer un code d'erreur
				if(idsSepares.length>1 && !isNaN(idsSepares[idsSepares.length-1]))
					// Recuperer l'id 
					idRecup = parseInt(idsSepares[idsSepares.length-1]);
				else
					return -3;

			}

			// Ajouter l'id dans le tableau des ids recuperes
			tabIds.push(idRecup);

			// Verifier si l'id est plus grand
			if(idMax<idRecup)
				idMax = idRecup;

		}

		// Chercher un id inferieur non existant
		for(i=0; i<=idMax; i++){

			// Verifier dans le tableau des id si l'element existe, sinon alors il est libre et on le renvoie
			if(tabIds.indexOf(i) <= -1){
				// Verifier si on utilise les arguments optionnels 
				if(idPrefix === undefined)
					return i;
				else
					return idPrefix+separateur+i;
			}

		}
		
		// Si aucun id libre et minimal a ete trouve, on renvoie un id incremente
		if(idPrefix === undefined)
			return i;
		else
			return idPrefix+separateur+i;

	}
	//Sinon, envoyer un code d'erreur
	else
		return -1;

}


