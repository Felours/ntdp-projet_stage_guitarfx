/* Class GBasePedal */
/* Description : Class representant le graphique d'un pedal */
/* Arguments : div - la div qui represente de maniere graphique le pedal
 pedal - instance de la classe Pedal descrivant le pedal
 ancrePreset - L'instance du preset contenante pour pouvoir monter dans la hierarchie */
function GBasePedal(div, pedal, ancrePreset) {

    // Implementer Serialize
    Serialize.call(this,"GBasePedal");

    // --- Attributs
    //
    this.m_id = undefined;	// L'id representant le gBasePedal
    var m_div = div;	// Le graphique (element div)
    this.m_pedal = pedal;
    this.m_position = new Position(0,0); // La position du graphique
    var m_ancrePreset = ancrePreset;

    //var m_dimension = new Dimension(10, 10); // Les dimensions du div
    //var m_img

    // --- Methodes
    //

    // --- Methode getId
    //
    this.getId = function(){
        return(this.m_id);
    };

    // --- Methode setId
    //
    this.setId = function(id){
        this.m_id = id;
    };

    // --- Methode getPedal
    //
    this.getPedal = function(){
        return(this.m_pedal);
    };

    // --- Methode getDiv
    //
    this.getDiv = function(){
        return(m_div);
    };

    // --- Methode setDiv
    //
    this.setDiv = function(div){
        m_div = div;
    };

    // --- Fonction setAncre
    // Description : Permet de faire un lien entre le GBB et le pedal (pour naviguer)
    //
    this.setAncre = function(){

        // Indiquer dans l'instance du pedal le lien vers le GBasePedal
        this.m_pedal.setAncre(this);

    };

    // --- Methode getPosition
    //
    this.getPosition = function(){
        return(this.m_position);
    };

    // --- Methode setPosition
    //
    this.setPosition = function(x, y){
        this.m_position.setPosition(x, y);
    };

    // --- Methode getAncrePreset
    // --- Description : Renvoie l'instance ancre du preset contenante
    // --- Retour : L'instance du preset contenant la pedal
    //
    this.getAncrePreset = function(){
        return(m_ancrePreset);
    };

    // --- Methode setAncrePreset
    // --- Description : Renseigne l'instance ancre du preset contenante
    //
    this.setAncrePreset = function(ancrePreset){
        m_ancrePreset = ancrePreset;
    };

    // --- Methode deconnecterToutesConnexionsAudio
    // --- Description : Permet de separer tous les flux audio du pedal des flux audio des pedals qui lui sont connectees
    //
    this.deconnecterToutesConnexionsAudio = function(){};

    // --- Methode connecterToutesConnexionsAudio
    // --- Description : Permet de connecter tous les flux audio du pedal des flux audio des pedals qui lui sont connectees
    //
    this.connecterToutesConnexionsAudio = function(){};

}
GBasePedal.prototype = new Serialize();

/* Class GPedal */
/* Description : Class representant le graphique d'un pedal reel */
/* Arguments : div - la div qui represente de maniere graphique le pedal
 pedal - instance de la classe Pedal descrivant le pedal
 ancrePreset - L'instance du preset contenante pour pouvoir monter dans la hierarchie*/
function GPedal(div, pedal, ancrePreset) {

    // Heritage
    GBasePedal.call(this, div, pedal, ancrePreset);
    this.setNomClass("GPedal");

    // --- Attributs
    //
    this.m_predecesseur = []; // Les GBasePedal (ou filles) qui precedent
    this.m_successeur = []; // Les GBasePedal (ou filles) qui suivent

    // --- Methodes
    //

    // --- Methode getPredecesseurs
    //
    this.getPredecesseurs = function(){
        return(this.m_predecesseur);
    };

    // --- Methode getSuccesseurs
    //
    this.getSuccesseurs = function(){
        return(this.m_successeur);
    };

    // --- Methode deconnecterToutesConnexionsAudio
    // --- Description : Permet de separer tous les flux audio du pedal des flux audio des pedals qui lui sont connectees
    //
    this.deconnecterToutesConnexionsAudio = function(){

        // Recuperer le preset contenant
        var presetContenant = this.getAncrePreset();

        // Verifier si le preset courant existe
        if(presetContenant === undefined)
            return -1;

        // Recuperer la div du gPedal
        var divGPedal = this.getDiv();

        // Verifier si la pedal existe
        var pExiste = presetContenant.pedalExiste(divGPedal);

        if(pExiste){

            /* Deconnecter toutes les connexions audio qui relient au successeurs */
            /**********************************************************************/

            // Recuperer le pedal associee au gPedal
            var pedal = this.getPedal();

            // Recuperer la liste des successeurs
            var successeurs = this.m_successeur;

            // Deconnecter les audios de chacun des gPedals
            var v_gPedalCourant, v_pedalCourant;
            for(var i=0; i<successeurs.length; i++){

                // Recuperer le v_gPedalCourant
                v_gPedalCourant = presetContenant.getGPedalFromId(successeurs[i]);

                // Verifier si le v_gPedalCourant a ete recupere
                if(v_gPedalCourant !== undefined || v_gPedalCourant != -1){

                    // Recuperer le pedal associee au gPedal courant
                    v_pedalCourant = v_gPedalCourant.getPedal();

                    // Deconnecter l'audio qui relie le pedal au pedal courant
                    try{
                        pedal.deconnecterAudio(v_pedalCourant);
                    }
                    catch(err){ console.log(err.toString()); }

                }

            }

            /* Deconnecter toutes les connexions audio qui relient les predecesseurs au pedal */
            /**********************************************************************************/

            // Recuperer la liste des predecesseurs
            var predecesseurs = this.m_predecesseur;

            // Deconnecter les audios de chacun des gPedals courants
            for(i=0; i<predecesseurs.length; i++){

                // Recuperer le v_gPedalCourant
                v_gPedalCourant = presetContenant.getGPedalFromId(predecesseurs[i]);

                // Verifier si le v_gPedalCourant a ete recupere
                if(v_gPedalCourant !== undefined || v_gPedalCourant != -1){

                    // Recuperer le pedal associee au gPedal courant
                    v_pedalCourant = v_gPedalCourant.getPedal();

                    // Deconnecter l'audio qui relie le pedal au pedal courant
                    try{
                        v_pedalCourant.deconnecterAudio(pedal);
                    }
                    catch(err){ console.log(err.toString()); }

                }

            }

        }

    };

    // --- Methode connecterToutesConnexionsAudio
    // --- Description : Permet de connecter tous les flux audio du pedal des flux audio des pedals qui lui sont connectees
    //
    this.connecterToutesConnexionsAudio = function(){

        // Recuperer le preset contenant
        var presetContenant = this.getAncrePreset();

        // Verifier si le preset courant existe
        if(presetContenant === undefined)
            return -1;

        // Recuperer la div du gPedal
        var divGPedal = this.getDiv();

        // Verifier si la pedal existe
        var pExiste = presetContenant.pedalExiste(divGPedal);

        if(pExiste){

            /* Connecter toutes les connexions audio qui relient au successeurs */
            /**********************************************************************/

            // Recuperer le pedal associee au gPedal
            var pedal = this.getPedal();

            // Recuperer la liste des successeurs
            var successeurs = this.m_successeur;

            // Deconnecter les audios de chacun des gPedals
            var v_gPedalCourant, v_pedalCourant;
            for(var i=0; i<successeurs.length; i++){

                // Recuperer le v_gPedalCourant
                v_gPedalCourant = presetContenant.getGPedalFromId(successeurs[i]);

                // Verifier si le v_gPedalCourant a ete recupere
                if(v_gPedalCourant !== undefined || v_gPedalCourant != -1){

                    // Recuperer le pedal associee au gPedal courant
                    v_pedalCourant = v_gPedalCourant.getPedal();

                    // Deconnecter l'audio qui relie le pedal au pedal courant
                    pedal.connecterAudio(v_pedalCourant);

                }

            }

        }

    };

}
GPedal.prototype = new GBasePedal();
GPedal.prototype.ajouterPredecesseur = ajouterPredecesseur;
GPedal.prototype.ajouterSuccesseur = ajouterSuccesseur;
GPedal.prototype.retirerPredecesseur = retirerPredecesseur;
GPedal.prototype.retirerSuccesseur = retirerSuccesseur;



/* Class GPedalDebut */
/* Description : Class representant le graphique de l'entree */
/* Arguments : div - la div qui represente de maniere graphique le pedal d'entree
 pedal - instance de la classe Pedal descrivant le pedal d'entree
 ancrePreset - L'instance du preset contenante pour pouvoir monter dans la hierarchie*/
function GPedalDebut(div, pedal, ancrePreset) {

    // Heritage
    GBasePedal.call(this, div, pedal, ancrePreset);
    this.setNomClass("GPedalDebut");

    // --- Attributs
    //
    this.m_successeur = []; // Les GBasePedal (ou filles) qui suivent

    // --- Methodes
    //

    // --- Methode getSuccesseurs
    //
    this.getSuccesseurs = function(){
        return(this.m_successeur);
    };

    // --- Methode deconnecterToutesConnexionsAudio
    // --- Description : Permet de separer tous les flux audio du pedal des flux audio des pedals qui lui sont connectees /* TODODO */
    //
    this.deconnecterToutesConnexionsAudio = function(){

        // Recuperer le preset contenant
        var presetContenant = this.getAncrePreset();

        // Verifier si le preset courant existe
        if(presetContenant === undefined)
            return -1;

        // Recuperer la div du gPedal
        var divGPedal = this.getDiv();

        // Verifier si la gPedal existe
        var pExiste = presetContenant.pedalExiste(divGPedal);

        if(pExiste){

            /* Deconnecter toutes les connexions audio qui relient au successeurs */
            /**********************************************************************/

            // Recuperer le pedal associee au gPedal
            var pedal = this.getPedal();

            // Recuperer la liste des successeurs
            var successeurs = this.m_successeur;

            // Deconnecter les audios de chacun des gPedals
            var v_gPedalCourant, v_pedalCourant;
            for(var i=0; i<successeurs.length; i++){

                // Recuperer le v_gPedalCourant
                v_gPedalCourant = presetContenant.getGPedalFromId(successeurs[i]);

                // Verifier si le v_gPedalCourant a ete recupere
                if(v_gPedalCourant !== undefined || v_gPedalCourant != -1){

                    // Recuperer le pedal associee au gPedal courant
                    v_pedalCourant = v_gPedalCourant.getPedal();

                    // Deconnecter l'audio qui relie le pedal au pedal courant
                    try{
                        pedal.deconnecterAudio(v_pedalCourant);
                    }
                    catch(err){ console.log(err.toString()); }

                }

            }

        }

    };

    // --- Methode connecterToutesConnexionsAudio
    // --- Description : Permet de connecter tous les flux audio du pedal des flux audio des pedals qui lui sont connectees
    //
    this.connecterToutesConnexionsAudio = function(){

        // Recuperer le preset contenant
        var presetContenant = this.getAncrePreset();

        // Verifier si le preset courant existe
        if(presetContenant === undefined)
            return -1;

        // Recuperer la div du gPedal
        var divGPedal = this.getDiv();

        // Verifier si la pedal existe
        var pExiste = presetContenant.pedalExiste(divGPedal);

        if(pExiste){

            /* Connecter toutes les connexions audio qui relient au successeurs */
            /**********************************************************************/

            // Recuperer le pedal associee au gPedal
            var pedal = this.getPedal();

            // Recuperer la liste des successeurs
            var successeurs = this.m_successeur;

            // Deconnecter les audios de chacun des gPedals
            var v_gPedalCourant, v_pedalCourant;
            for(var i=0; i<successeurs.length; i++){

                // Recuperer le v_gPedalCourant
                v_gPedalCourant = presetContenant.getGPedalFromId(successeurs[i]);

                // Verifier si le v_gPedalCourant a ete recupere
                if(v_gPedalCourant !== undefined || v_gPedalCourant != -1){

                    // Recuperer le pedal associee au gPedal courant
                    v_pedalCourant = v_gPedalCourant.getPedal();

                    // Deconnecter l'audio qui relie le pedal au pedal courant
                    pedal.connecterAudio(v_pedalCourant);

                }

            }

        }

    };

}
GPedalDebut.prototype = new GBasePedal();
GPedalDebut.prototype.ajouterSuccesseur = ajouterSuccesseur;
GPedalDebut.prototype.retirerSuccesseur = retirerSuccesseur;

/* Class GPedalFin */
/* Description : Class representant le graphique d'un pedal de fin */
/* Arguments : div - la div qui represente de maniere graphique le pedal de fin
 pedal - instance de la classe Pedal descrivant le pedal de fin
 ancrePreset - L'instance du preset contenante pour pouvoir monter dans la hierarchie*/
function GPedalFin(div, pedal, ancrePreset) {

    // Heritage
    GBasePedal.call(this, div, pedal, ancrePreset);
    this.setNomClass("GPedalFin");

    // --- Attributs
    //
    this.m_predecesseur = []; // Les GBasePedal (ou filles) qui precedent

    // --- Methodes
    //

    // --- Methode getPredecesseurs
    //
    this.getPredecesseurs = function(){
        return(this.m_predecesseur);
    };

}
GPedalFin.prototype = new GBasePedal();
GPedalFin.prototype.ajouterPredecesseur = ajouterPredecesseur;
GPedalFin.prototype.retirerPredecesseur = retirerPredecesseur;


/* Methodes a ajouter aux classes GBasePedal */
/* ========================================== */

// --- Methode ajouterPredecesseur
//
function ajouterPredecesseur(predecesseur) {
    this.getPredecesseurs().push(predecesseur);
}

// --- Methode ajouterSuccesseur
//
function ajouterSuccesseur(successeur) {
    this.getSuccesseurs().push(successeur);
}

// --- Methode retirerPredecesseur
//
function retirerPredecesseur(predecesseur) {

    // Trouver l'indice du predecesseur
    var index = this.getPredecesseurs().indexOf(predecesseur);

    // Si trouve, retirer de la liste
    if (index > -1) {
        this.getPredecesseurs().splice(index, 1);
    }

}

// --- Methode retirerSuccesseur
//
function retirerSuccesseur(successeur) {

    // Trouver l'indice du predecesseur
    var index = this.getSuccesseurs().indexOf(successeur);
    // Si trouve, retirer de la liste
    if (index > -1) {
        this.getSuccesseurs().splice(index, 1);
    }

}


/* Class Position */
/* Description : Class representant la position d'un element */
/* Arguments : x - la position horizontale
 y - la position verticale */
function Position(x, y){

    // Implementer Serialize
    Serialize.call(this,"Position");

    // --- Attributs
    //
    this.m_x = x;
    this.m_y = y;

    // --- Methodes
    //

    // --- Methode getX
    //
    this.getX = function(){
        return(this.m_x);
    };

    // --- Methode getY
    //
    this.getY = function(){
        return(this.m_y);
    };

    // --- Methode setX
    //
    this.setX = function(x){
        this.m_x = x;
    };

    // --- Methode setY
    //
    this.setY = function(y){
        this.m_y = y;
    };

    // --- Methode setDimension
    //
    this.setPosition = function(x, y){
        this.m_x = x;
        this.m_y = y;
    };
}
Position.prototype = new Serialize();