/**
author : M.D
date : 12/08/2015
**/


var Overdrive = (function () {

    // constructeur
    function Overdrive(audio_context) {
        Stomp.call(this,audio_context);

        this.ws = new WaveShapers();

        this.filter = audio_context.createBiquadFilter();
        this.shaper = audio_context.createWaveShaper();
        this.gain = audio_context.createGain();
        
        
    }

    // méthodes d'instance
    Overdrive.prototype = {

        getParametterTone : function(){
            var param = "0,10,1,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterDrive : function(){
            var param = "0,10,2,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterVolume : function(){
            var param = "0,10,2,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        effectConnect : function(){   
            if(this.etat===true){ 
                this.input.disconnect(this.output);
                this.input.connect(this.shaper);
                this.shaper.connect(this.filter);
                this.filter.connect(this.gain);
                this.gain.connect(this.output);    
            }else{
                this.input.disconnect(this.shaper);
                this.shaper.disconnect(this.filter);
                this.filter.disconnect(this.gain);
                this.gain.disconnect(this.output); 
                this.input.connect(this.output);
            }
        },

        setTone : function(value){  
            value = map(value,0,10,100,10000)
            if (!this.filter) return;
            this.filter.frequency.value = value;
        },

        setDrive: function(value) {  
            if (!this.shaper) return;
            this.shaper.curve = this.ws.distorsionCurves["notSoDistorded"](value);
        },

        setVolume: function(value) {  
            value = map(value,0,10,0.1,1)
            if (!this.gain) return;
            this.gain.gain.value = value;
        },

        removeEffect: function(){ 
             if(this.etat===false){ 
                this.input.disconnect(this.output);   
            }else{
                this.input.disconnect(this.shaper);
                this.shaper.disconnect(this.filter);
                this.filter.disconnect(this.gain);
                this.gain.disconnect(this.output); 
        }
        delete this;
    },
};

$.extend(true,Overdrive.prototype,Stomp.prototype);
return Overdrive;

}());