/**
author : M.D
**/


var GraphicEQ = (function () {
    // constructeur
    function GraphicEQ(audio_context) {
        Stomp.call(this,audio_context);
        this.valueFilter = [60, 170, 350, 1000, 3500, 10000];
        this.filters = [];

        for (var i = 0; i <= this.valueFilter.length - 1; i++) {            
              this.eq = audio_context.createBiquadFilter();
              this.eq.frequency.value = this.valueFilter[i];
              this.eq.type = "peaking";
              this.eq.gain.value = 0;
              this.filters.push(this.eq);
        }; 
    };

    // méthodes d'instance
    GraphicEQ.prototype = {

        getValFilter : function(i){
            return this.filters[i].frequency.value;
        },

        getFilters:function(){
            return this.filters;
        },


        getParametterFilter: function(){
            var param = "-30,30,0,1";
            var listParam = param.split(",");
            return listParam;
        },

        setValueAudioEQ : function(sliderValue,numFilter){
            var value = parseFloat(sliderValue);
            this.filters[this.valueFilter.indexOf(numFilter)].gain.value = value;
        },

        effectConnect : function(){  
            if(this.etat===true){
                this.input.disconnect(this.output);
                this.input.connect(this.filters[0]);
                for(var i = 0; i < this.filters.length - 1; i++) {
                  this.filters[i].connect(this.filters[i+1]);
              }
              this.filters[this.filters.length-1].connect(this.output);
          }else{
               this.input.disconnect(this.filters[0]);
               for(var i = 0; i < this.filters.length - 1; i++) {
                  this.filters[i].disconnect(this.filters[i+1]);
              }
              this.filters[this.filters.length-1].disconnect(this.output);
              this.input.connect(this.output);
          }
  },

  removeEffect: function(){
    if(this.etat===true){ 
        this.input.disconnect(this.filters[0]);
        for(var i = 0; i < this.filters.length - 1; i++) {
            this.filters[i].disconnect(this.filters[i+1]);
        }
        this.filters[this.filters.length-1].disconnect(this.output);
    }else{ 
        this.input.disconnect(this.output);            
    }
    delete this;
}

};


$.extend(true,GraphicEQ.prototype,Stomp.prototype);
return GraphicEQ;

}());