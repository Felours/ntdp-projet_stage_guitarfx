/**
author : M.D
date : 23/03/2015
**/


var Chorus = (function () {

    // constructeur
    function Chorus(audio_context) {
        Stomp.call(this,audio_context);
        this.attenuator = audio_context.createGain();
        this.splitter = audio_context.createChannelSplitter(2);
        this.delayL = audio_context.createDelay();
        this.delayR = audio_context.createDelay();
        this.feedbackGainNodeLR = audio_context.createGain();
        this.feedbackGainNodeRL = audio_context.createGain();
        this.merger = audio_context.createChannelMerger(2);
        this.valueDelay=0.001;        
        this.listMode = ['sine','square','sawtooth','triangle'];
        this.lfo = audio_context.createOscillator();
        this.lfo.type = 'triangle';
        this.lfo.start();
        this.attenuator.gain.value = 0.6934;
    }

    // méthodes d'instance
    Chorus.prototype = {

        getParametterRate: function(){
            var param = "0,10,3,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterFeedback : function(){
            var param = "0,10,7,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterDelay : function(){
            var param = "0.005,0.055,0.01,0.001";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterDepth : function(){
            var param = "0.0005,1,0.0005,0.0005";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterType : function(){
            return this.listMode;
        },

        effectConnect : function(){   
            if(this.etat===true){ 
                this.input.disconnect(this.output);

                this.input.connect(this.attenuator);
                this.attenuator.connect(this.output);
                this.input.connect(this.splitter);
                this.splitter.connect(this.delayL, 0);
                this.splitter.connect(this.delayR, 1);
                this.delayL.connect(this.feedbackGainNodeLR);
                this.delayR.connect(this.feedbackGainNodeRL);
                this.lfo.connect(this.feedbackGainNodeLR.gain);
                this.lfo.connect(this.feedbackGainNodeRL.gain);
                this.feedbackGainNodeRL.connect(this.merger,0,0);
                this.feedbackGainNodeLR.connect(this.merger,0,1);
                this.merger.connect(this.output);

            }else{
                this.input.disconnect(this.attenuator);
                this.attenuator.disconnect(this.output);
                this.input.disconnect(this.splitter);
                this.splitter.disconnect(this.delayL, 0);
                this.splitter.disconnect(this.delayR, 1);
                this.delayL.disconnect(this.feedbackGainNodeLR);
                this.delayR.disconnect(this.feedbackGainNodeRL);
                this.lfo.disconnect(this.feedbackGainNodeLR.gain);
                this.lfo.disconnect(this.feedbackGainNodeRL.gain);
                this.feedbackGainNodeRL.disconnect(this.merger,0,0);
                this.feedbackGainNodeLR.disconnect(this.merger,0,1);
                this.merger.disconnect(this.output);

                this.input.connect(this.output);
            }
        },


        setDelay: function(value) {
            if (!this.delayR || !this.delayL) return;            
            value = map(value,0,10,0.005,0.055)
            this.valueDelay = value;
            this.delayR.delayTime.value = value;
            this.delayL.delayTime.value = value;
        },

        setDepth: function(value) {        
            if (!this.lfo) return; 
            value = map(value,0,10,0.0005,1)
            this.lfo.detune.value  = this.valueDelay*value;
        },

        setRate: function(value) {
            if (!this.lfo) return;
            value = map(value,0,10,0.5,15)
            this.lfo.frequency.value=value;  
        },

        setFeedback : function(value){   
            value = map(value,0,10,0.1,1)         
            this.feedbackGainNodeLR.gain.value = value;
            this.feedbackGainNodeRL.gain.value = value;
        },

        setType : function(value){
            if (!this.lfo) return;
            this.lfo.type = value;
        },

        removeEffect: function(){ 

           if(this.etat===false){ 
               this.input.disconnect(this.output);   
           }else{
               this.input.disconnect(this.attenuator);
               this.attenuator.disconnect(this.output);
               this.input.disconnect(this.splitter);
               this.splitter.disconnect(this.delayL, 0);
               this.splitter.disconnect(this.delayR, 1);
               this.delayL.disconnect(this.feedbackGainNodeLR);
               this.delayR.disconnect(this.feedbackGainNodeRL);
               this.lfo.disconnect(this.feedbackGainNodeLR.gain);
               this.lfo.disconnect(this.feedbackGainNodeRL.gain);
               this.feedbackGainNodeRL.disconnect(this.merger,0,0);
               this.feedbackGainNodeLR.disconnect(this.merger,0,1);
               this.merger.disconnect(this.output);  
           }

           this.lfo.stop();
           delete this;
       },
   };

   $.extend(true,Chorus.prototype,Stomp.prototype);
   return Chorus;

}());