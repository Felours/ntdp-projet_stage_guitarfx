/**
author : M.D
**/


var Compressor = (function () {

    // constructeur
    function Compressor(audio_context) {

        Stomp.call(this,audio_context);
        this.compressor = audio_context.createDynamicsCompressor();
        this.compressor.threshold.value = -50;
        this.compressor.knee.value = 40;
        this.compressor.ratio.value = 12;
        this.compressor.reduction.value = -20;
        this.compressor.attack.value = 0;
        this.compressor.release.value = 0.25;
    }

    // méthodes d'instance
    Compressor.prototype = {

        getParametterThreshold : function(){
            var param = "0,10,5,0.1";
            var listParam = param.split(",");
            return listParam;
        },
        getParametterKnee : function(){
            var param = "0,10,4,0.1";
            var listParam = param.split(",");
            return listParam;
        },
        getParametterRatio : function(){
            var param = "0,10,5.1,0.1";
            var listParam = param.split(",");
            return listParam;
        },
        getParametterReduction : function(){
            var param = "0,10,1,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterAttack : function(){
            var param = "0,10,0,0.1";
            var listParam = param.split(",");
            return listParam;
        },
        getParametterRelease: function(){
            var param = "0,10,3,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        setThreshold : function(value){            
            value = map(value,0,10,0,100)
            if (!this.compressor) return;
            this.compressor.threshold.value = value*(-1);
        },
        setKnee : function(value){                  
            value = map(value,0,10,0,100)    
            if (!this.compressor) return;
            this.compressor.knee.value = value;
        },
        setRatio : function(value){          
            value = map(value,0,10,0,20)
            if (!this.compressor) return;
            this.compressor.ratio.value = value;
        },
        setReduction : function(value){  
            value = map(value,0,10,0,20)
            if (!this.compressor) return;
            this.compressor.reduction.value = value*(-1);
        },
        setAttack : function(value){
            if (!this.compressor) return;
            this.compressor.attack.value = value;
        },
        setRelease: function(value){  
            value = map(value,0,10,0,2)
            if (!this.compressor) return;
            this.compressor.release.value = value;
        },

        effectConnect : function(){   
            if(this.etat===true){ 
                this.input.disconnect(this.output);
                this.input.connect(this.compressor);
                this.compressor.connect(this.output);
            }else{
                this.input.disconnect(this.compressor);
                this.compressor.disconnect(this.output);
                this.input.connect(this.output);
            }
        },

        removeEffect: function(){
            if(this.etat===false){
                this.input.disconnect(this.output);        
            }else{                  
                this.input.disconnect(this.compressor);
                this.compressor.disconnect(this.output);        
            }
            delete this;
        },

    };


    $.extend(true,Compressor.prototype,Stomp.prototype);
    return Compressor;

}());