/**
author : M.D
**/


var Pitch = (function () {
 
    // constructeur
    function Pitch(audio_context) {
        Stomp.call(this,audio_context);  
    }
 
    // méthodes d'instance
    Pitch.prototype = {

        effectConnect : function(){   
            /*if(this.etat===true){ 
            }else{
            }*/
            this.input.connect(this.output);
        },

         removeEffect: function(){
            /*if(this.etat===false){ 
            }else{ 
            }*/
            this.input.disconnect(this.output);
            delete this;
        }

    };

    $.extend(true,Pitch.prototype,Stomp.prototype);
    return Pitch;
 
}());