/**
author : M.D
date : 23/03/2015
**/


var Distorsion = (function () {
 
    // constructeur
    function Distorsion(audio_context) {

        Stomp.call(this,audio_context);
        this.filter = audio_context.createBiquadFilter();
        this.shaper = audio_context.createWaveShaper();
        this.gain = audio_context.createGain();
        
    }
 
    // méthodes d'instance
    Distorsion.prototype = {

        getParametterTone : function(){
            var param = "0,10,0.02,0.01";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterDrive : function(){
            var param = "0,10,0.5,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterVolume : function(){
            var param = "0,10,1,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        effectConnect : function(){   
            if(this.etat===true){ 
                this.input.disconnect(this.output);
                this.input.connect(this.shaper);
                this.shaper.connect(this.filter);
                this.filter.connect(this.gain);
                this.gain.connect(this.output);    
            }else{
                this.input.disconnect(this.shaper);
                this.shaper.disconnect(this.filter);
                this.filter.disconnect(this.gain);
                this.gain.disconnect(this.output); 
                this.input.connect(this.output);
            }
        },

        setTone: function(value) {                     
            value = map(value,0,10,2000,5000)
            if (!this.filter) return;
            this.filter.frequency.value = value;
        },

        setDrive: function(value) {         
            value = map(value,0,10,0,20)
            if (!this.shaper) return;
            this.shaper.curve = this.makeDistortionCurve(value);
            //this.shaper.oversample = '4x';
        },

        setVolume: function(value) {  
            value = map(value,0,10,0.1,20)
            if (!this.gain) return;
            this.gain.gain.value = value;
        },


        makeDistortionCurve: function(amount) {
            var k = amount*100;
            var n_samples = 44100; //65536; //22050;     //44100
            curve = new Float32Array(n_samples);
            var deg = Math.PI / 180;
            for (var i = 0; i < n_samples; i += 1) {
                var x = i * 2 / n_samples - 1;
                curve[i] = (3 + k) * x * 20 * deg / (Math.PI + k * Math.abs(x));
            }
            return curve;
        },

        removeEffect: function(){ 
             if(this.etat===false){ 
                this.input.disconnect(this.output);   
            }else{
                this.input.disconnect(this.shaper);
                this.shaper.disconnect(this.filter);
                this.filter.disconnect(this.gain);
                this.gain.disconnect(this.output); 
            }
            delete this;
        },
    };


    $.extend(true,Distorsion.prototype,Stomp.prototype);
    return Distorsion;
 
}());