/**
author : M.D
date : 23/03/2015
**/


var Reverb = (function () {

    // constructeur
    function Reverb(audio_context) {
        Stomp.call(this,audio_context);
        this.audio_context=audio_context;
        this.reverb = audio_context.createConvolver();

        this.listMode = ['reverb1','reverb2','reverb3'];

        this.volume = audio_context.createGain();
        this.volume.gain.volume=1;
        this.root = audio_context.createGain();
        this.root.gain.value = 0;
        
        this.concertHallBuffer;
        this.loadFile(this.listMode[0],this.audio_context);
        
        
    }

    // méthodes d'instance
    Reverb.prototype = {


        loadFile : function(file,audio_context){
            var that = this;
            var request = new XMLHttpRequest();
            request.open('GET','js/Stomp/Reverb/Sounds/'+file+'.wav', true);
            request.responseType = 'arraybuffer';

            request.onload = function() {
                    audio_context.decodeAudioData(/** @type {ArrayBuffer} */(request.response), function(buffer) {
                    that.reverb.buffer = buffer;
                });
            };
            request.send();
        },

        getParametterVolume : function(){
            var param = "0,1,0,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterType : function(){                  
           return this.listMode;
       },


       effectConnect : function(){   
        if(this.etat===true){ 
            this.input.disconnect(this.output);
            this.input.connect(this.reverb);
            this.reverb.connect(this.volume);
            this.input.connect(this.root);
            this.root.connect(this.output);
            this.volume.connect(this.output); 
        }else{
            this.input.disconnect(this.reverb);
            this.reverb.disconnect(this.volume);
            this.volume.disconnect(this.output);
            this.input.disconnect(this.root);
            this.root.disconnect(this.output);
            this.input.connect(this.output);
        }
    },

    setVolume: function(value) {
        if (!this.volume) return;
        this.volume.gain.value = value;
        this.root.gain.value = 1 - this.volume.gain.value;
    },

    setType : function(value){    
        this.loadFile(value,this.audio_context);
    },

    removeEffect: function(){
     if(this.etat===false){ 
        this.input.disconnect(this.output);
    }else{
        this.input.disconnect(this.reverb);
        this.reverb.disconnect(this.volume);
        this.volume.disconnect(this.output);
    }
    delete this;
},

};


$.extend(true,Reverb.prototype,Stomp.prototype);
return Reverb;

}());