//CONSTRUCTOR OF NEW AMP


var NewAmp = (function () {
    
    // constructeur
    function NewAmp(audio_context) {
        Stomp.call(this,audio_context);
        this.modelAmp = new NewAmpModel(audio_context);
        this.presets = this.modelAmp.getPresets();
        this.p = this.modelAmp.getCurrentPresetValues();
/*
                    LCF: lowCutFilter.frequency.value,
            HCF: hiCutFilter.frequency.value,
            K1: getDistorsionValue(0),
            K2: getDistorsionValue(1),
            K3: getDistorsionValue(2),
            K4: getDistorsionValue(3),
            F1: filters[0].frequency.value,
            F2: filters[1].frequency.value,
            F3: filters[2].frequency.value,
            F4: filters[3].frequency.value,
            Q1: filters[0].Q.value.toFixed(1),
            Q2: filters[1].Q.value.toFixed(1),
            Q3: filters[2].Q.value.toFixed(1),
            Q4: filters[3].Q.value.toFixed(1),
            OG: (outputGain.gain.value*10).toFixed(1),
            BF: ((bassFilter.gain.value / 3) + 5).toFixed(1), // bassFilter.gain.value = (value-5) * 3;
            MF: ((midFilter.gain.value / 2) + 5).toFixed(1), // midFilter.gain.value = (value-5) * 2;
            TF: ((trebleFilter.gain.value / 5) + 5).toFixed(1), // trebleFilter.gain.value = (value-5) * 5;
            PF: ((presenceFilter.gain.value / 2) + 5).toFixed(1), // presenceFilter.gain.value = (value-5) * 2;
            EQ: eq.getValues(),
            MV: masterVolume.gain.value.toFixed(1),
            RG: (reverb.getGain()*10).toFixed(1),
            CG: (cabinetSim.getGain()*10).toFixed(1)
*/
    }
    
    // méthodes d'instance
    NewAmp.prototype = {
    	// GETTER


		getParametterVolume : function(){
            var param = "0,10," + this.p.OG + ",0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterMaster : function(){
            var param = "0,10," + this.p.MV + ",0.1";
            var listParam = param.split(",");
            return listParam;
        },

    	getParametterDrive : function(){
            var val1 = Math.max(this.p.K1, this.p.K2);
            var val2 = Math.max(val1, this.p.K3);
            var val3 = Math.max(val2, this.p.K4);

            var param = "0,10," + val3 + ",0.1";								//min , max , def , step
            var listParam = param.split(",");
            return listParam;
        },

        getParametterBass : function(){
            var param = "0,10," + this.p.BF + ",0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterMiddle : function(){
            var param = "0,10," + this.p.MF + ",0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterTreble : function(){
            var param = "0,10," + this.p.TF + ",0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterReverb : function(){
            var param = "0,10,"+ this.p.RG + ",0.1";
            var listParam = param.split(",");
            return listParam;
        },

		getParametterPresence : function(){   
            var param = "0,10," + this.p.PF + ",0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterPreset : function(){        	       	 
        	 return this.presets;
        },

        // SETTER


        setVolume : function(value){
        	this.modelAmp.changeOutputGain(value);
        },

        setMaster : function(value){
        	this.modelAmp.changeMasterVolume(value);
        },

    	setDrive : function(value){
    		this.modelAmp.changeDrive(value);
        },

        setBass : function(value){
        	this.modelAmp.changeBassFilterValue(value);
        },

        setMid : function(value){        	
        	this.modelAmp.changeMidFilterValue(value);
        },

        setTreble : function(value){
        	this.modelAmp.changeTrebleFilterValue(value);
        },

        setReverb : function(value){
        	this.modelAmp.changeReverb(value);
        },

		setPresence : function(value){ 
			this.modelAmp.changePresenceFilterValue(value);
        },

        setPreset : function(value){  
            console.log("dans set Preset de newamp.js")
        	this.modelAmp.setPresetByName(value);
        },

        addPotentiometre: function(name, pot) {
            this.modelAmp.addPotentiometre(name, pot);
        },

        // CONNECT

    	
    	
    	effectConnect : function() {   
            // called when activate button is clicked
            console.log("newamp:effectConnect etat=" +this.etat);

    		if(this.etat===true){  
                //if previous state was disconnected
    			this.input.disconnect(this.output);           	
    			this.input.connect(this.modelAmp.input);
    			this.modelAmp.output.connect(this.output);
    		}else{  
                // if previous state was connected    	
    			this.input.disconnect(this.modelAmp.input);
    			this.modelAmp.output.disconnect(this.output);
    			this.input.connect(this.output);   
    		}
            console.log("newamp end of effect connect");

    	},

    	removeEffect: function(){
    		if(this.etat===false){ 
    			this.input.disconnect(this.output);
    		}else{      	
    			this.input.disconnect(this.modelAmp.input);
    			this.modelAmp.output.disconnect(this.output)
    		} 
    		delete this;
    	}

    };

    $.extend(true,NewAmp.prototype,Stomp.prototype);
    return NewAmp;
    
}());