/**
author : M.D
date : 23/03/2015
**/


var Delay = (function () {
    
    // constructeur
    function Delay(audio_context) {
        
        Stomp.call(this,audio_context);
        this.volume = audio_context.createGain();
        this.delay = audio_context.createDelay();
        this.feedbackGain = audio_context.createGain();
        this.feedbackGain.gain.value=1;
        this.filter = audio_context.createBiquadFilter();
        
    }
 
    // méthodes d'instance
    Delay.prototype = {

        getParametterVolume : function(){
            var param = "0.1,10,3.4,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterDelay : function(){
            var param = "0,1,0.34,0.005";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterFeedback : function(){
            var param = "0.1,0.9,0.3,0.01";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterFilter : function(){
            var param = "0,1000,700,100";
            var listParam = param.split(",");
            return listParam;
        },

        effectConnect : function(){   
            if(this.etat===true){ 
                this.input.disconnect(this.output);
                this.input.connect(this.volume);
                this.delay.connect(this.feedbackGain);
                this.feedbackGain.connect(this.delay);
                this.volume.connect(this.delay);
                this.volume.connect(this.filter);
                this.delay.connect(this.filter);
                this.filter.connect(this.output);     
            }else{
                this.input.disconnect(this.volume);
                this.delay.disconnect(this.feedbackGain);
                this.feedbackGain.disconnect(this.delay);
                this.filter.disconnect(this.output);
                this.volume.disconnect(this.delay);
                this.volume.disconnect(this.filter);
                this.delay.disconnect(this.filter);
                this.input.connect(this.output);
            }
        },

        setVolume: function(value) {
            if (!this.volume) return;
            this.volume.gain.value = value;
        },

        setDelay: function(value) {
            if (!this.delay) return;
            this.delay.delayTime.setValueAtTime(value, 0);
        },

        setFeedback: function(value) {
            if (!this.feedbackGain) return;
            this.feedbackGain.gain.value = value;
        },

        setFilter: function(value) {
            if (!this.filter) return;
            this.filter.frequency.value = value;
        },

        removeEffect: function(){
            if(this.etat===false){ 
                this.input.disconnect(this.output);   
            }else{
                this.input.disconnect(this.volume);
                this.delay.disconnect(this.feedbackGain);
                this.feedbackGain.disconnect(this.delay);
                this.filter.disconnect(this.output);
                this.volume.disconnect(this.delay);
                this.volume.disconnect(this.filter);
                this.delay.disconnect(this.filter);
            }
            delete this;
        },
    };


    $.extend(true,Delay.prototype,Stomp.prototype);
    return Delay;
 
}());