/**
author : M.D
**/


var Gate = (function () {
 
    // constructeur
    function Gate(audio_context) {
        Stomp.call(this,audio_context);
    }
 
    // méthodes d'instance
    Gate.prototype = {

        effectConnect : function(){   
            /*if(this.etat===true){ 
            }else{
            }*/
            this.input.connect(this.output);
        },

         removeEffect: function(){
            /*if(this.etat===false){ 
            }else{ 
            }*/
            this.input.disconnect(this.output);
            delete this;
        }

    };


    $.extend(true,Gate.prototype,Stomp.prototype);
    return Gate;
 
}());