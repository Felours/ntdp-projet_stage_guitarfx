/**
author : M.D
**/


var Amp = (function () {
    
    // constructeur
    function Amp(audio_context) {
        Stomp.call(this,audio_context)
    	this.listMode = ['D.I.', 'traditional', 'small radio', 'small combo 1','small combo 2','large stack 1', 'large stack 2'];
		this.host = new DAWPluginHost(audio_context);
    	this.combo = new MDA.Combo(this.host);
    }
    
    // méthodes d'instance
    Amp.prototype = {
    	getParametterDrive : function(){
            var param = "0,100,0,1";								//min , max , def , step
            var listParam = param.split(",");
            return listParam;
        },

        getParametterBias : function(){
            var param = "0,100,0,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterProcess : function(){
            var param = "0,100,0,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterHPFFreq : function(){
            var param = "0,100,0,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterHPFReso : function(){
            var param = "0,100,0,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterMaster : function(){
            var param = "0,100,0,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterType : function(){        	       	 
        	 return this.listMode;
        },

    	effectConnect : function(){   
    		if(this.etat===true){  
    			this.input.disconnect(this.output);           	
    			this.input.connect(this.combo.input);
    			this.combo.connect(this.output);
    		}else{      	
    			this.input.disconnect(this.combo.input);
    			this.combo.disconnect(this.output);
    			this.input.connect(this.output);   
    		}
    	},

    	setDrive : function(value){
    		this.combo.setParam(1, value/100);
    	},

    	setBias : function(value){
    		this.combo.setParam(2, value/100);
    	},

    	setMaster : function(value){
    		this.combo.setParam(3, value/100);
    	},

    	setProcess : function(value){
    		this.combo.setParam(4, value/100);
    	},

    	setHPFFreq : function(value){
    		this.combo.setParam(5, value/100);
    	},

    	setHPFReso : function(value){
    		this.combo.setParam(6, value/100);
    	},

    	setType : function(value){    
			this.combo.setParam(0, this.listMode.indexOf(value) / 6.9);
    	},

    	removeEffect: function(){
    		if(this.etat===false){ 
    			this.input.disconnect(this.output);
    		}else{      	
    			this.input.disconnect(this.combo.input);
    			this.combo.disconnect(this.output);
    		} 
    		delete this;
    	},

    };

    $.extend(true,Amp.prototype,Stomp.prototype);
    return Amp;
    
}());