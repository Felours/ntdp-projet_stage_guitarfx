var MDA = MDA || {};

MDA.Combo = function (host)
{
	this.type = "effect";
	this.name = "mdacombo";	
	host.createPlugin(this, this.path + "mdacombo_asm.js", 0);	
}

MDA.Combo.prototype = new DAWPlugin();
MDA.Combo.prototype.constructor = MDA.Combo;
MDA.Combo.prototype.base = DAWPlugin.prototype;
MDA.Combo.prototype.path = DAWPlugin.prototype.getPath();
