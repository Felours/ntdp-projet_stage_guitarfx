/**
author : M.D
**/


var Cabinet = (function () {
    
    // constructeur
    function Cabinet(audio_context) {
        Stomp.call(this,audio_context);
    }
 
    // méthodes d'instance
    Cabinet.prototype = {

        effectConnect : function(){   
            /*if(this.etat===true){ 
            }else{
            }*/
            this.input.connect(this.output);
        },

         removeEffect: function(){
            /*if(this.etat===false){ 
            }else{ 
            }*/
            this.input.disconnect(this.output);
            delete this;
        }

    };

    $.extend(true,Cabinet.prototype,Stomp.prototype);
    return Cabinet;
 
}());