/**
author : M.D
**/


var QuadraFuzz = (function () {
    
    // constructeur
    function QuadraFuzz(audio_context) {
        Stomp.call(this,audio_context);
        this.quadraFuzz = new QuadraFuzzModel(audio_context);
    }
    
    // méthodes d'instance
    QuadraFuzz.prototype = {
    	getParametterDrive : function(){
            var param = "0,150,50,1";								//min , max , def , step
            var listParam = param.split(",");
            return listParam;
        },

        getParametterLow : function(){
            var param = "70,300,147,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterMid1: function(){
            var param = "300,1200,587,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterMid2: function(){
            var param = "1200,3500,2490,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterHigh: function(){
            var param = "3500,7000,4980,1";
            var listParam = param.split(",");
            return listParam;
        },


        getParametterOutput : function(){
            var param = "0,10,5,1";
            var listParam = param.split(",");
            return listParam;
        },

    	effectConnect : function(){   
    		if(this.etat===true){  
    			this.input.disconnect(this.output);           	
    			this.input.connect(this.quadraFuzz.input);
    			this.quadraFuzz.output.connect(this.output);
    		}else{      	
    			this.input.disconnect(this.quadraFuzz.input);
    			this.quadraFuzz.output.disconnect(this.output);
    			this.input.connect(this.output);   
    		}
    	},

    	setDrive : function(value){
            this.quadraFuzz.setDrive(value)
    	},


    	setVolume : function(value){
    		this.quadraFuzz.setOutput(value/10);
    	},


    	setFreq : function(value,name){
            switch (name){
                case 'Low':
                    this.quadraFuzz.setFreqValue(value, 0);
                case 'Mid1':
                    this.quadraFuzz.setFreqValue(value, 1);
                case 'Mid2':
                    this.quadraFuzz.setFreqValue(value, 2);
                case 'High':
                    this.quadraFuzz.setFreqValue(value, 3);

            }

    	},


    	removeEffect: function(){
    		if(this.etat===false){ 
    			this.input.disconnect(this.output);
    		}else{      	
    			this.input.disconnect(this.this.quadraFuzz.input);
    			this.quadraFuzz.output.disconnect(this.output);
    		} 
    		delete this;
    	},

    };

    $.extend(true,QuadraFuzz.prototype,Stomp.prototype);
    return QuadraFuzz;
    
}());

// ----------- QUADRAFUZZ ---------------

function QuadraFuzzModel(context) { 
    // input gain
    var inputGain = context.createGain();
    inputGain.gain.value = 1;
    
    // filtres
    var filters = [];
    var lowpassLeft = context.createBiquadFilter();
    lowpassLeft.frequency.value = 147;
    lowpassLeft.type = "lowpass";
    filters[0] = lowpassLeft;

    var bandpass1Left = context.createBiquadFilter();
    bandpass1Left.frequency.value = 587;
    bandpass1Left.type = "bandpass";
    filters[1] = bandpass1Left;

    var bandpass2Left = context.createBiquadFilter();
    bandpass2Left.frequency.value = 2490;
    bandpass2Left.type = "bandpass";
    filters[2] = bandpass2Left;
    
    var highpassLeft = context.createBiquadFilter();
    highpassLeft.frequency.value = 4980;
    highpassLeft.type = "highpass";
    filters[3] = highpassLeft;
    
    // overdrives
    var k = [50, 50, 50, 50]; // array of k initial values
    var od = [];
    var gainsOds = [];
    
    for(var i = 0; i < 4; i++) {
       od[i] = context.createWaveShaper();
       od[i].curve = makeDistortionCurve(k[i]);
       od[i].oversample = '4x';
      
       // gains
       gainsOds[i] = context.createGain();
       gainsOds[i].gain.value = 1;
    }
    
    // Master output gain
    var outputGain = context.createGain();
    outputGain.gain.value = 1;
    
    buildGraph();
    
    function buildGraph() {
      
        for(var i = 0; i < 4; i++) {
            inputGain.connect(filters[i]);
            filters[i].connect(od[i]);
            od[i].connect(gainsOds[i]);
            gainsOds[i].connect(outputGain);
        }
    }
    

    function setDistorsionValue(val, numDisto) {
         k[numDisto] = val;
         od[numDisto].curve = makeDistortionCurve(k[numDisto]);
    }
  
    function setQValue(val, numQ) {
         filters[numQ].Q.value = val;
    }
    
    function setFreqValue(val, numF) {
        filters[numF].frequency.value = val;
    }

     function setDrive(val) {
         for(var i = 0; i < 4; i++) {
            setDistorsionValue(val, i);
        }
     }

     function setOutput(val){
        outputGain.gain.value = val;
     }
   
  
    function makeDistortionCurve(k) {
       var n_samples = context.sampleRate,
           curve = new Float32Array(n_samples),
           deg = Math.PI / 180, i = 0, x;
      
       for ( ; i < n_samples; ++i ) {
          x = i * 2 / n_samples - 1;
          curve[i] = ( 3 + k ) * x * 20 * deg / ( Math.PI + k * Math.abs(x) );
       }
       return curve;
     }

    return {
      input: inputGain,
      output: outputGain,
      setDistorsionValue: setDistorsionValue,
      setQValue: setQValue,
      setFreqValue: setFreqValue,
      setDrive : setDrive,
      setOutput: setOutput
    };
}
