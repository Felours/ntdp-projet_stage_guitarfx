// ----------- QUADRAFUZZ ---------------

function QuadraFuzzModel(context) { 
    // input gain
    var inputGain = context.createGain();
    inputGain.gain.value = 1;
    
    // filtres
    var filters = [];
    var lowpassLeft = context.createBiquadFilter();
    lowpassLeft.frequency.value = 147;
    lowpassLeft.type = "lowpass";
    filters[0] = lowpassLeft;

    var bandpass1Left = context.createBiquadFilter();
    bandpass1Left.frequency.value = 587;
    bandpass1Left.type = "bandpass";
    filters[1] = bandpass1Left;

    var bandpass2Left = context.createBiquadFilter();
    bandpass2Left.frequency.value = 2490;
    bandpass2Left.type = "bandpass";
    filters[2] = bandpass2Left;
    
    var highpassLeft = context.createBiquadFilter();
    highpassLeft.frequency.value = 4980;
    highpassLeft.type = "highpass";
    filters[3] = highpassLeft;
    
    // overdrives
    var k = [50, 50, 50, 50]; // array of k initial values
    var od = [];
    var gainsOds = [];
    
    for(var i = 0; i < 4; i++) {
       od[i] = context.createWaveShaper();
       od[i].curve = makeDistortionCurve(k[i]);
       //od[i].oversample = '4x';
      
       // gains
       gainsOds[i] = context.createGain();
       gainsOds[i].gain.value = 1;
    }
    
    // Master output gain
    var outputGain = context.createGain();
    outputGain.gain.value = 1;
    
    buildGraph();
    
    function buildGraph() {
      
        for(var i = 0; i < 4; i++) {
            inputGain.connect(filters[i]);
            filters[i].connect(od[i]);
            od[i].connect(gainsOds[i]);
            gainsOds[i].connect(outputGain);
        }
    }
    

    function setDistorsionValue(val, numDisto) {
         k[numDisto] = val;
         od[numDisto].curve = makeDistortionCurve(k[numDisto]);
    }
  
    function setQValue(val, numQ) {
         filters[numQ].Q.value = val;
    }
    
    function setFreqValue(val, numF) {
        filters[numF].frequency.value = val;
    }

     function setDrive(val) {
         for(var i = 0; i < 4; i++) {
            setDistorsionValue(val, i);
        }
     }

     function setOutput(val){
        outputGain.gain.value = val;
        console.log("setting quadrafuzz output to " + val)
     }
   
  
    function makeDistortionCurve(k) {
       var n_samples = context.sampleRate,
           curve = new Float32Array(n_samples),
           deg = Math.PI / 180, i = 0, x;
      
       for ( ; i < n_samples; ++i ) {
          x = i * 2 / n_samples - 1;
          curve[i] = ( 3 + k ) * x * 20 * deg / ( Math.PI + k * Math.abs(x) );
       }
       return curve;
     }

    return {
      input: inputGain,
      output: outputGain,
      setDistorsionValue: setDistorsionValue,
      setQValue: setQValue,
      setFreqValue: setFreqValue,
      setDrive : setDrive 
    };
}
