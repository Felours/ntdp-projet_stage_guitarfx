include("js/Stomp/stomp.js");

// Pedales creees par Maxime Demetrio
var listFichier=["Chorus","Delay","Compressor","AutoWah","Distorsion","Rat","Volume","Reverb","Cabinet", "Overdrive", "Amp","NewAmp" , "Filter", "Gate", "GraphicEQ","Pitch","QuadraFuzz"];	// Volume = Gain dans l'application
listFichier.forEach(function(item) {	
	include("js/Stomp/"+item+'/'+item.toLowerCase()+".js");
});

includeAmp();

function include(fileName){
    document.write("<script type='text/javascript' src='"+fileName+"'></script>" );
}

function includeAmp(){	
    document.write("<script type='text/javascript' src='js/Stomp/Amp/host/dawplugin.js'></script>" );

    document.write("<script type='text/javascript' src='js/Stomp/Amp/host/dawpluginhost.js'></script>" );

    document.write("<script type='text/javascript' src='js/Stomp/Amp/mdacombo_asm.js'></script>" );

    document.write("<script type='text/javascript' src='js/Stomp/Amp/mdacombo.js'></script>" );


    document.write("<script type='text/javascript' src='js/Stomp/NewAmp/newampmodel.js'></script>" );

    document.write("<script type='text/javascript' src='js/Stomp/NewAmp/distorsionFactory.js'></script>" );

    document.write("<script type='text/javascript' src='js/Stomp/NewAmp/utils.js'></script>" );
}