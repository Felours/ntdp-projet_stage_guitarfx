/**
author : M.D
date : 23/03/2015
**/


var AutoWah = (function () {

    // constructeur
    function AutoWah(audio_context) {
        Stomp.call(this,audio_context);
        this.volume = audio_context.createGain();
        this.volume.gain.value = 12.9;
        this.waveshaper = audio_context.createWaveShaper();
        this.awFollower = audio_context.createBiquadFilter();
        this.awFollower.type = "lowpass";
        this.awFollower.frequency.value = 10.0;

        this.curve = new Float32Array(65536);
        for (var i=-32768; i<32768; i++)
            this.curve[i+32768] = ((i>0)?i:-i)/32768;
        this.waveshaper.curve = this.curve;

        this.awDepth = audio_context.createGain();
        this.awDepth.gain.value = 11585;

        this.awFilter = audio_context.createBiquadFilter();
        this.awFilter.type = "lowpass";
        this.awFilter.Q.value = 15;
        this.awFilter.frequency.value = 11.65;
        

        this.awDepth.connect(this.awFilter.frequency);  
    }
    
    // méthodes d'instance
    AutoWah.prototype = {

       getParametterFrequency : function(){
        var param = "0.25,15,11.95,0.05";
        var listParam = param.split(",");
        return listParam;
    },

    getParametterVolume : function(){
        var param = "3,10,12.9,0.1";
        var listParam = param.split(",");
        return listParam;
    },

    getParametterQ : function(){
        var param = "0,20,15,0.1";
        var listParam = param.split(",");
        return listParam;
    },

    effectConnect : function(){   
        if(this.etat===true){ 
            this.input.disconnect(this.output);
            this.input.connect(this.volume); 
            this.volume.connect(this.waveshaper);
                  
            this.waveshaper.connect(this.awFollower);   
            this.awFollower.connect(this.awDepth);             
            this.input.connect(this.awFilter);     
            this.awFilter.connect(this.output);
        }else{
            this.input.disconnect(this.volume);  
            this.volume.disconnect(this.waveshaper);               
            this.input.disconnect(this.awFilter);               
            this.waveshaper.disconnect(this.awFollower);
            this.awFollower.disconnect(this.awDepth);
            this.awFilter.disconnect(this.output);
            this.input.connect(this.output);
        }
    },
    
    
    setQ: function(value) {
        //value = map(value,0,10,0,20)
        if (!this.awFilter) return;            
        this.awFilter.Q.value = value;
        console.log("wah set q to " + value);
    },

    setFrequency: function(value) {  
        //value = map(value,0,10,0.25,20)
        if (!this.awFollower) return;
        this.awFollower.frequency.value=value;
        console.log("wah set freq to " + value);
    },

    setVolume: function(value) {
        //value = map(value,1024,16384,0,5)
        if (!this.volume) return;
        this.volume.gain.value = value;
        console.log("wah set v to " + value);
    },

    removeEffect: function(){
        this.awDepth.disconnect(this.awFilter.frequency);
        if(this.etat===false){ 
            this.input.disconnect(this
                .output);
        }else{
            this.input.disconnect(this.volume);
            this.volume.disconnect(this.waveshaper);                 
            this.input.disconnect(this.awFilter);               
            this.waveshaper.disconnect(this.awFollower);
            this.awFollower.disconnect(this.awDepth);
            this.awFilter.disconnect(this.output);
        }
        delete this;
    },

};

$.extend(true,AutoWah.prototype,Stomp.prototype);
return AutoWah;

}());