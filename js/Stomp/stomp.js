/**
author : M.D
**/

var Stomp = (function () {

    // constructeur
    function Stomp(audio_context) {

        this.input = audio_context.createGain();
        this.input.gain.value=1;
        this.output = audio_context.createGain();
        this.output.gain.value=1;
        this.etat = false; 
        this.input.connect(this.output); 

    };

    // méthodes d'instance
    Stomp.prototype = {

        setEtat: function (etat) {
            this.etat = etat;
            this.effectConnect();
        },

        getInput: function(){
            return this.input;
        },

        getOutput: function(){
            return this.output;
        }

    };

    return Stomp;

}());