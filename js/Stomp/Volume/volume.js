/**
author : M.D
date : 23/03/2015
**/


var Volume = (function () {
 
    // constructeur
    function Volume(audio_context) {
        Stomp.call(this,audio_context);
        this.volume = audio_context.createGain();
    }
 
    // méthodes d'instance
    Volume.prototype = {

        getParametterVolume : function(){
            var param = "0.1,10,1,0.1";
            var listParam = param.split(",");
            return listParam;
        },

        effectConnect : function(){   
            if(this.etat===true){ 
				this.input.disconnect(this.output);
                this.input.connect(this.volume);
                this.volume.connect(this.output); 
            }else{
                this.input.disconnect(this.volume);
                this.volume.disconnect(this.output); 
                this.input.connect(this.output);
            }
        },

        setVolume: function(value) {
            if (!this.volume) return;
            this.volume.gain.value = value;
        },

         removeEffect: function(){
            if(this.etat===false){ 
                this.input.disconnect(this.output);
            }else{
                this.input.disconnect(this.volume);
                this.volume.disconnect(this.output); 
            }
            delete this;
        },

    };


    $.extend(true,Volume.prototype,Stomp.prototype);
    return Volume;
 
}());