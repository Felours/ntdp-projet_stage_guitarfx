/**
author : M.D
date : 23/03/2015
**/


var Rat = (function () {
 
    // constructeur
    function Rat(audio_context) {

        Stomp.call(this,audio_context);
        this.filter = audio_context.createBiquadFilter();
        this.shaper = audio_context.createWaveShaper();
        this.gain = audio_context.createGain();
        
        this.lpFilter = audio_context.createBiquadFilter();
        this.lpFilter.type="lowpass";
        this.lpFilter.frequency.value = 16000;

        this.hpFilter1 = audio_context.createBiquadFilter();
        this.hpFilter1.type="highpass";
        this.hpFilter1.frequency.value = 1539;

        this.hpFilter2 = audio_context.createBiquadFilter();
        this.hpFilter2.type="highpass";
        this.hpFilter2.frequency.value = 60;
    }
 
    // méthodes d'instance
    Rat.prototype = {

        getParametterTone : function(){
            var param = "1,10,1,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterDrive : function(){
            var param = "0,20,0,1";
            var listParam = param.split(",");
            return listParam;
        },

        getParametterVolume : function(){
            var param = "0,10,1,1";
            var listParam = param.split(",");
            return listParam;
        },

        effectConnect : function(){   
            if(this.etat===true){ 
                this.input.disconnect(this.output);
                this.input.connect(this.shaper);
                this.shaper.connect(this.lpFilter);
                this.lpFilter.connect(this.hpFilter1);
                this.hpFilter1.connect(this.hpFilter2);
                this.hpFilter2.connect(this.gain);
                this.gain.connect(this.output);    
            }else{
                this.input.disconnect(this.shaper);
                this.shaper.disconnect(this.lpFilter);
                this.lpFilter.disconnect(this.hpFilter1);
                this.hpFilter1.disconnect(this.hpFilter2);
                this.hpFilter2.disconnect(this.gain);
                this.gain.disconnect(this.output);
                this.input.connect(this.output);
            }
        },

        setTone: function(value) {
            if (!this.filter) return;
            var calculFrequency = 1/((2*Math.PI)*((value*10+1.5)*100)*3.3*Math.pow(10,-9));
            this.filter.frequency.value = calculFrequency;
        },

        setDrive: function(value) {
            if (!this.shaper) return;
            this.shaper.curve = this.makeDistortionCurve(value);
            this.shaper.oversample = '4x';
        },

        setVolume: function(value) {
            if (!this.gain) return;
            this.gain.gain.value = value;
        },



        makeDistortionCurve: function(value) {
            var amount =   1 + ((10*this.logdix( value*10 ))/47)+((10*this.logdix( value ))/560);
            var k = amount*1000;
            var n_samples = 44100;
            curve = new Float32Array(n_samples);
            var deg = Math.PI / 180;
            for (var i = 0; i < n_samples; i += 1) {
                var x = i * 2 / n_samples - 1;
                curve[i] = (3 + k) * x * 57 * deg / (Math.PI + k * Math.abs(x));
            }
            return curve;
            
        },

        logdix: function (x){
		    return Math.log(x)/Math.LN10;
		},

        removeEffect: function(){ 
             if(this.etat===false){ 
                this.input.disconnect(this.output);   
            }else{                
                this.input.disconnect(this.shaper);
                this.shaper.disconnect(this.lpFilter);
                this.lpFilter.disconnect(this.hpFilter1);
                this.hpFilter1.disconnect(this.hpFilter2);
                this.hpFilter2.disconnect(this.gain);
                this.gain.disconnect(this.output);
            }
            delete this;
        },
    };


    $.extend(true,Rat.prototype,Stomp.prototype);
    return Rat;
 
}());