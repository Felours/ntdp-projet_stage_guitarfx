/**
author : M.D
**/


var Filter = (function () {
 
    // constructeur
    function Filter(audio_context) {
        Stomp.call(this,audio_context);
    }
 
    // méthodes d'instance
    Filter.prototype = {

        effectConnect : function(){   
            /*if(this.etat===true){ 
            }else{
            }*/
            this.input.connect(this.output);
        },

         removeEffect: function(){
            /*if(this.etat===false){ 
            }else{ 
            }*/
            this.input.disconnect(this.output);
        }

    };


    $.extend(true,Filter.prototype,Stomp.prototype);
    return Filter;
 
}());