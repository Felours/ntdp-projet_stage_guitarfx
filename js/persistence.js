/*
    This file contains all the persistence code (IndexedDV + fallback with localStorage)
 */

/* Classe Serialize */
/* ================ */

/* Description : Class a implementer aux classes meres dont les informations doivent etre sauvegardees et restaurees */
// --- Argument : nomClass - nom de la classe implementant Serialize. Doit etre renseigne au constructeur
//
function Serialize(nomClass){

    this.m_nomClass = nomClass;

    // --- Methode getNomClass
    //
    this.getNomClass = function(){
        return(this.m_nomClass);
    };

    // --- Methode setNomClass
    //
    this.setNomClass = function(nomClass){
        this.m_nomClass = nomClass;
    };

    //this.restaurerObjetsInternes = restaurerClasses;
}
Serialize.prototype.restaurerObjetsInternes = restaurerClasses;

// --- Fonction restaurerClasses
// --- Description : recreer les objets internes de l'objet traite en tant qu'instance de classe dont ils sont definies
// --- Argument : instance - argument optionel contenant un objet non encore recree
//
function restaurerClasses(insta){

    // Verifier si l'argument existe
    if(typeof(insta) === 'undefined')
        insta = this;

    // Verifier si l'argument est un tableau
    if(Array.isArray(insta)){

        // Passer chaque objet au niveau suivant
        for(var i=0; i<insta.length; i++)
            restaurerClasses(insta[i]);

    }
    // Si c'est un objet quelconque
    else{

        // Verifier si l'objet contient m_nomClass (si oui, il s'agit d'un objet issu de classe personnelle)
        var nomClass = insta.m_nomClass;

        if(typeof(nomClass) !== 'undefined'){

            // Indiquer a l'objet sa classe
            insta.__proto__ = Object.create(new window[nomClass]());

            // Passer les attributs de l'objet (en cas de composition)
            Object.keys(insta).forEach(function (key) {

                // Verifier si l'instance peut lancer la restauration
                try{
                    insta.restaurerObjetsInternes(insta[key]);
                }
                catch(err){
                    // Sinon, restaurer par la fonction globale
                    restaurerClasses(insta[key]);
                }

            });

        }

    }
}

/* Class DataStorage */
/* Description : Class permettant de sauvegarder / restaurer des donnees sous format JSON */
/* Argument : db - nom de la database */
function DataStorage(db){

    // --- Attributs
    //
    this.m_db = db;	// Nom de la base de donnees
    this.m_tableBase = "default";	// Nom de la table de base (a laquelle on ajoute les valeurs)
    var v_IDBSupport = false;	// Verifier si IndexedDB est supporte

    // --- Initialisation des variables globales
    //
    var indexedDB, IDBKeyRange, openCopy, IDBTransaction;
    try{
        indexedDB = window.indexedDB || window.webkitIndexedDB || window.msIndexedDB;
        IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange;
        openCopy = indexedDB && indexedDB.open;
        IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;

        if (IDBTransaction)
        {
            IDBTransaction.READ_WRITE = IDBTransaction.READ_WRITE || 'readwrite';
            IDBTransaction.READ_ONLY = IDBTransaction.READ_ONLY || 'readonly';
        }
    }
    catch(err){
        console.log("Probleme avec initialisation des variables globales IndexedDB");
    }

    // --- Verification de l'existance de l'IndexedDB
    //
    if("indexedDB" in window){
        // Indiquer que IDB est supporte
        v_IDBSupport = true;
    }

    // --- Methode getDB
    //
    this.getDB = function(){
        return(this.m_db);
    };

    // --- Methode setDB
    //
    this.setDB = function(db){
        this.m_db = db;
    };

    // --- Methode getTableBase
    //
    this.getTableBase = function(){
        return(this.m_tableBase);
    };

    // --- Methode setTableBase
    //
    this.setTableBase = function(nom){
        this.m_tableBase = nom;
    };

    // --- Methode getIDBSupport
    //
    this.getIDBSupport = function(){
        return(v_IDBSupport);
    };

    // --- Methode addValue
    // --- Description : Methode permettant d'ajouter une valeur associee a une cle dans la base
    // --- Arguments : key - cle de la valeur,
    // 				   value - valeur a ajouter,
    // 				   tableBase - (FACULTATIF) nom de la table de base dans laquelle on ajoutera les valeurs.
    //
    this.addValue = function(key, value, tableBase){

        // Tester les arguments
        if(key === undefined)
            return -1;
        if(value === undefined)
            return -1;

        // Tester la variable facultative
        var nomTable;
        if(tableBase !== undefined)
            nomTable = tableBase;
        else
            nomTable = this.m_tableBase;

        // Verifier si IndexedDB est supporte
        if(this.getIDBSupport()){

            // Ouverture de la DB
            var v_request = indexedDB.open(this.getDB());

            //indexedDB.deleteDatabase(this.getDB());
            // Evenement traitant de la creation
            v_request.onupgradeneeded = function(e)
            {
                // Recuperer le resultat
                var v_result = e.target.result;

                // Tester si la table existe
                if (!v_result.objectStoreNames.contains(nomTable))
                {

                    // Definir la structure de la table
                    //var store = v_result.createObjectStore(nomTable, {keyPath: 'Key', autoIncrement: false});
                    var store = v_result.createObjectStore(nomTable);

                    // Creer la ligne avec la valeur de la cle dans la table
                    //store.createIndex('Value', nomTable, {unique: true, multiEntry: false});

                }
                else {
                    //v_result.deleteObjectStore(nomTable);
                }

            };

            // Evenement traitement de l'ajout
            v_request.onsuccess = function(e) {

                // Recuperer le resultat
                var v_result = e.target.result;
                var trans = v_result.transaction(nomTable, IDBTransaction.READ_WRITE);
                var store = trans.objectStore(nomTable);

                // Tester si la valeur existe deja
                var req = store.openCursor(key);
                req.onsuccess = function(e) {

                    var cursor = e.target.result;

                    if (cursor) { // Cle existante
                        // Mettre a jour la valeur
                        cursor.update(value);	//console.log(cursor.key + cursor.value);
                    } else { // Cle non existante

                        // Ajouter la cle et sa valeur
                        var requestAdd = store.add(value, key);

                        // Evenement ajout reussi
                        requestAdd.onsuccess = function(e) {
                        };

                        // Evenement ajout non reussi
                        requestAdd.onfailure = function(e) {
                        };

                    }

                };

            };

        }

    };

    // --- Methode getValue
    // --- Description : Methode permettant de recuperer une valeur associee a une cle dans la base
    // --- Arguments : key - cle de la valeur,
    // 				   CB - fonction callback qui recuperera le resultat,
    // 				   tableBase - (FACULTATIF) nom de la table de base dans laquelle on ajoutera les valeurs.
    // --- Renvoye : La valeur associee a la cle
    //
    this.getValue = function(key, CB, tableBase){

        // Tester les arguments
        if(key === undefined)
            return -1;
        if(CB === undefined)
            return -1;

        // Tester la variable facultative
        var nomTable;
        if(tableBase !== undefined)
            nomTable = tableBase;
        else
            nomTable = this.m_tableBase;

        // Verifier si IndexedDB est supporte
        if(this.getIDBSupport()){

            // Ouverture de la DB
            var v_request = indexedDB.open(this.getDB());

            //indexedDB.deleteDatabase(this.getDB());
            // Evenement traitant de la creation
            v_request.onupgradeneeded = function(e)
            {
                // Recuperer le resultat
                var v_result = e.target.result;

                // Tester si la table existe
                if (!v_result.objectStoreNames.contains(nomTable))
                {

                    // Definir la structure de la table
                    //var store = v_result.createObjectStore(nomTable, {keyPath: 'Key', autoIncrement: false});
                    var store = v_result.createObjectStore(nomTable);

                    // Creer la ligne avec la valeur de la cle dans la table
                    //store.createIndex('Value', nomTable, {unique: true, multiEntry: false});

                }
                else {
                    //v_result.deleteObjectStore(nomTable);
                }

            };

            // Evenement traitement de l'ajout
            v_request.onsuccess = function(e) {

                // Recuperer le resultat
                var v_result = e.target.result;
                var trans = v_result.transaction(nomTable, IDBTransaction.READ_WRITE);
                var store = trans.objectStore(nomTable);

                // Recuperer le resultat
                var req = store.get(key);

                req.onsuccess = function(e) {

                    // Renvoyer le resultat de retour a la fonction callback
                    CB(e.target.result);

                };

            };

        }
        // Fallback vers localstorage sinon
        else {

            // Verifier si le localhost est reconnu (pour IE sous local)
            if(localStorage !== undefined){

                // Renvoyer le resultat de retour a la fonction callback
                CB(localStorage.getItem(key));

            }

        }

    };

    // --- Methode deleteValue
    // --- Description : Methode permettant de supprimer une cle dans la base
    // --- Arguments : key - cle de la valeur,
    // 				   tableBase - (FACULTATIF) nom de la table de base dans laquelle on ajoutera les valeurs.
    //
    this.deleteValue = function(key, tableBase){

        // Tester les arguments
        if(key === undefined)
            return -1;

        // Tester la variable facultative
        var nomTable;
        if(tableBase !== undefined)
            nomTable = tableBase;
        else
            nomTable = this.m_tableBase;

        // Verifier si IndexedDB est supporte
        if(this.getIDBSupport()){

            // Ouverture de la DB
            var v_request = indexedDB.open(this.getDB());

            //indexedDB.deleteDatabase(this.getDB());
            // Evenement traitant de la creation
            v_request.onupgradeneeded = function(e)
            {
                // Recuperer le resultat
                var v_result = e.target.result;

                // Tester si la table existe
                if (!v_result.objectStoreNames.contains(nomTable))
                {

                    // Definir la structure de la table
                    //var store = v_result.createObjectStore(nomTable, {keyPath: 'Key', autoIncrement: false});
                    var store = v_result.createObjectStore(nomTable);

                    // Creer la ligne avec la valeur de la cle dans la table
                    //store.createIndex('Value', nomTable, {unique: true, multiEntry: false});

                }
                else {
                    //v_result.deleteObjectStore(nomTable);
                }

            };

            // Evenement traitement de l'ajout
            v_request.onsuccess = function(e) {

                // Recuperer le resultat
                var v_result = e.target.result;
                var trans = v_result.transaction(nomTable, IDBTransaction.READ_WRITE);
                var store = trans.objectStore(nomTable);

                // Supprimer la cle et sa valeur
                var requestAdd = store.delete(key);

                // Evenement ajout reussi
                requestAdd.onsuccess = function(e) {
                };

                // Evenement ajout non reussi
                requestAdd.onfailure = function(e) {
                };

            };

        }
        // Fallback vers localstorage sinon
        else {

            // Verifier si le localhost est reconnu
            if(localStorage !== undefined){

                // Supprimer la clef
                localStorage.removeItem(key);

            }

        }

    };

}
