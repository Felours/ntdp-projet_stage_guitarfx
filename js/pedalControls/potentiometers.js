

/* Classes liees aux Potentiometre */
/* =========================== */

/* Class Potentiometre */
/* Description : Class representant un potentiometre d'un pedal (ne sera utilisee que par ses classes filles) */
/* Arguments : nom - le nom du potentiometre
 m_gPotentiometre - la classe contenant le graphique du potentiometre */
function Potentiometre(nom, gPotentiometre){

    // Implementer Serialize
    Serialize.call(this,"Potentiometre");

    // --- Attributs
    //
    this.m_nom = nom;	// Le nom du potentiometre
    this.m_gPotentiometre = gPotentiometre; // Instance de la classe contenant le graphique representant le potentiometre
    var m_ancrePedal;	// Ancre montant vers le pedal qui contient l'instance

    // --- Methodes
    //

    // --- Methode getNom
    //
    this.getNom = function(){
        return(this.m_nom);
    };

    // --- Methode setNom
    //
    this.setNom = function(nom){
        this.m_nom = nom;
    };

    // --- Methode getGPotentiometre
    //
    this.getGPotentiometre = function(){
        return(this.m_gPotentiometre);
    };

    // --- Methode setGPotentiometre
    //
    this.setGPotentiometre = function(gp){
        this.m_gPotentiometre = gp;
    };

    // --- Methode modifierGPId
    // Description : Change l'id du graphique du potentiometre
    //
    this.modifierGPId = function(id){
        if(this.m_gPotentiometre !== undefined)
            this.m_gPotentiometre.setId(id);
    };

    // --- Methode setAncrePedal
    // --- Description : Lie l'instance du potentiometre au pedal qui le contient (pour acceder a l'audio context)
    // -- Argument : ancrePedal - l'instance de l'objet pedal qui contient le potentiometre
    //
    this.setAncrePedal = function(ancrePedal){

        // Indiquer l'ancre
        m_ancrePedal = ancrePedal;

    };

    // --- Methode getAncrePedal
    // --- Description : Renvois l'instance du pedal qui contient le potentiometre (pour acceder a l'audio context)
    //
    this.getAncrePedal = function(){

        // Renvoyer l'ancre du pedal
        return m_ancrePedal;

    };

    // --- Methode lierAncrePedalPotentiometre
    // --- Description : Methode permettant de lier le pedal a l'instance Potentiometre (pour pouvoir interagir avec l'audio)
    // --- Argument : pedal - l'instance du pedal (pour le lier au graphique du potentiometre)
    //
    this.lierAncrePedalPotentiometre = function(pedal){

        // Lier le pedal au potentiometre
        this.setAncrePedal(pedal);

        // Lier le potentiometre au graphique associe
        this.m_gPotentiometre.setAncrePotentiometre(this);

    };


}
Potentiometre.prototype = new Serialize();

/* Class GPotentiometre */
/* Description : Class representant le graphique d'un potentiometre (ne sera utilisee que par ses classes filles) */
/* Arguments : graphique - l'element graphique (DOM, pedalboard, autre..)
 typeValeurs - le type de valeurs du graphique (interval, liste, autre..) */
function GPotentiometre(graphique, typeValeurs){

    // Implementer Serialize
    Serialize.call(this,"GPotentiometre");

    // --- Attributs
    //
    this.m_graphique = graphique;	// Description HTML du graphique (chaine de caractere qui sera generee)
    this.m_typeValeurs = typeValeurs; // Une chaine de caracteres indiquant le type de valeurs
    this.m_id = undefined;	// L'id du graphique (pour connaitre le graphique declanchant les evenements)
    this.m_valeurs = []; // Tableau associatif (chaque instance du tableau contient 2 valeurs, [0] pour la clef --> [1] pour la valeur) contenant les valeurs (en correlation avec le type de valeur)
    var m_ancrePotentiometre;	// L'acre du potentiometre permettant d'interagir avec l'audio (de la classe de Maxime)

    // --- Methodes
    //

    // --- Methode getGraphique (methode a surcharger pour retourner un element graphique genere de m_graphique)
    //
    this.getGraphique = function(){
        return(this.m_graphique);
    };

    // --- Methode setGraphique
    //
    this.setGraphique = function(g){
        this.m_graphique = g;
    };

    // --- Methode getTypeValeurs
    //
    this.getTypeValeurs = function(){
        return(this.m_typeValeurs);
    };

    // --- Methode setTypeValeurs
    //
    this.setTypeValeurs = function(tv){
        this.m_typeValeurs = tv;
    };

    // --- Methode getId
    //
    this.getId = function(){
        return(this.m_id);
    };

    // --- Methode setId
    //
    this.setId = function(id){
        this.m_id = id;
    };

    // --- Methode getValeurs
    //
    this.getValeurs = function(){
        return(this.m_valeurs);
    };

    // --- Methode getValeur
    // --- Argument : clef - Permet de trouver la valeur associee a la clef
    //
    this.getValeur = function(clef){

        // Recuperer les valeurs
        var unRetour = -1;
        var vals = this.m_valeurs;

        // Faire une recherche
        for(var i=0; i<vals.length; i++)
            // Verifier si la cle a ete trouvee
            if(vals[i][0] == clef)
                unRetour = vals[i][1];

        // Renvoyer la valeur
        return(unRetour);
    };

    // --- Methode ajouterValeur
    //
    this.ajouterValeur = function(clef, valeur){

        // Ajouter l'association ([0] pour la clef, [1] pour la valeur)
        this.m_valeurs.push([clef,valeur]);
        //this.m_valeurs[clef] = valeur;
    };

    // --- Methode retirerValeur
    //
    this.retirerValeur = function(clef, valeur){

        // Chercher la clef
        var cle, val;
        for(var i=0; i<this.m_valeurs.length; i++){

            // Recuperer la cle
            cle = this.m_valeurs[i][0];

            // Verifier si la cle correspond
            if(cle === clef){

                // Verifier si on a enseigne la valeur en potentiometre
                if(valeur !== undefined){

                    // Recuperer la valeur
                    val = this.m_valeurs[i][1];

                    // Verifier si la valeur correspond
                    if(val === valeur)
                    // Supprimer l'association
                        this.m_valeurs.splice(i, 1);

                }
                else
                // Supprimer l'association
                    this.m_valeurs.splice(i, 1);

            }

        }


        // for(var ob in tab)
        // Supprimer la clef (puisque tableau associatif)
        //delete this.m_valeurs[clef];
    };

    // --- Methode getAncrePotentiometre
    // --- Description : Methode permettant de retourner l'instance (ancre montant) du potentiometre contenant le controle de l'audio (de la classe de Maxime)
    //
    this.getAncrePotentiometre = function(){

        // Fournir l'ancre
        return m_ancrePotentiometre;

    };

    // --- Methode setAncrePotentiometre
    // --- Description : Methode permettant de fournir l'instance (ancre montant) du potentiometre contenant le controle de l'audio (de la classe de Maxime)
    // --- Argument : potentiometre - l'instance du potentiometre
    //
    this.setAncrePotentiometre = function(potentiometre){

        // Fournir l'ancre
        m_ancrePotentiometre = potentiometre;

    };

    // --- Methode getValeurReelle
    // --- Description : Methode permettant de recuperer la valeur reelle du gPotentiometre (correspond a la valeur 'value' par defaut)
    // --- Retour : La valeur reelle du gPotentiometre ('value' par defaut)
    //
    this.getValeurReelle = function(){

        // Renvoyer la valeur
        return this.getValeur('value');

    };

}
GPotentiometre.prototype = new Serialize();

/* Classes filles de la classe Potentiometre */
/* ===================================== */

/* Class PotentiometreGain */
/* Description : Class representant un potentiometre Gain d'un pedal */
/* Arguments : minVal - la valeur minimale de l'intervalle
 maxVal - la valeur maximale de l'intervalle
 valInit - la valeur initiale
 steps - valeur de saut lors d'un changement  */
function PotentiometreGain(minVal, maxVal, valInit, steps){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreGain");

    // Indiquer le nom du potentiometre
    this.setNom("Gain");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreKnob(minVal, maxVal, valInit, steps);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreGain.prototype = new Potentiometre();
PotentiometreGain.prototype.setValueAudio = setValueAudioGain;

/* Class PotentiometrePan */
/* Description : Class representant un potentiometre Pan d'un pedal */
function PotentiometrePan(){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometrePan");

    // Indiquer le nom du potentiometre
    this.setNom("Pan");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreKnob(-1, 1, 0, 0.01);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometrePan.prototype = new Potentiometre();
PotentiometrePan.prototype.setValueAudio = setValueAudioPan;

/* Class PotentiometreTone */
/* Description : Class representant un potentiometre Tone d'un pedal */
function PotentiometreTone(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreTone");

    // Verifier si l'argument est correct
    if(params === undefined)
        params = 1;

    // Indiquer le nom du potentiometre
    this.setNom("Tone");
    var g;
    if(params === undefined || !$.isArray(params)){
        g = new GPotentiometreKnob(0, 1, params, 0.01);
    }else{

        try{
            // Creer le graphique associe (Instance fille de la classe GPotentiometre)
            g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
        }
        catch(err){}
    }


    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreTone.prototype = new Potentiometre();
PotentiometreTone.prototype.setValueAudio = setValueAudioTone;


/* Class PotentiometreVolume */
/* Description : Class representant un potentiometre Volume d'un pedal */
/* Argument : valInit - la valeur initiale */
function PotentiometreVolume(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreVolume");

    // Verifier si l'argument est correct
    if(params === undefined)
        params = 1;

    // Indiquer le nom du potentiometre
    this.setNom("Volume");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    if(params === undefined || !$.isArray(params)){
        g = new GPotentiometreKnob(0, 1, params, 0.01);
    }else{

        try{
            // Creer le graphique associe (Instance fille de la classe GPotentiometre)
            g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
        }
        catch(err){}
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreVolume.prototype = new Potentiometre();
PotentiometreVolume.prototype.setValueAudio = setValueAudioVolume;

/* Class PotentiometreType */
/* Description : Class representant un potentiometre Type d'un pedal */
/* Argument : tab - tableu contenant les elements de la liste */
function PotentiometreType(tab){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreType");

    // Indiquer le nom du potentiometre
    this.setNom("Type");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreListe(tab);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreType.prototype = new Potentiometre();
PotentiometreType.prototype.setValueAudio = setValueAudioType;

/* Class PotentiometrePreset */
/* Description : Class representant un potentiometre Type d'un pedal */
/* Argument : tab - tableu contenant les elements de la liste */
function PotentiometrePreset(tab){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometrePreset");

    // Indiquer le nom du potentiometre
    this.setNom("Preset");
    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreListe(tab);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometrePreset.prototype = new Potentiometre();
PotentiometrePreset.prototype.setValueAudio = setValueAudioPreset;

/* Class PotentiometreMix */
/* Description : Class representant un potentiometre Mix d'un pedal */
function PotentiometreMix(){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreMix");

    // Indiquer le nom du potentiometre
    this.setNom("Mix");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreKnob(0, 100, 20, 0.1);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreMix.prototype = new Potentiometre();
PotentiometreMix.prototype.setValueAudio = setValueAudioMix;

/* Class PotentiometreFilter */
/* Description : Class representant un Filter pour une pedal d'effet*/
function PotentiometreFilter(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreFilter");

    // Indiquer le nom du potentiometre
    this.setNom("Filter");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreFilter.prototype = new Potentiometre();
PotentiometreFilter.prototype.setValueAudio = setValueAudioFilter;



/* Class PotentiometreRoom */
/* Description : Class representant un potentiometre Room d'un pedal */
function PotentiometreRoom(){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreRoom");

    // Indiquer le nom du potentiometre
    this.setNom("Room");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreKnob(0, 100, 0, 1);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreRoom.prototype = new Potentiometre();
PotentiometreRoom.prototype.setValueAudio = setValueAudioRoom;

/* Class PotentiometreFeedBack */
/* Description : Class representant un potentiometre FeedBack d'un pedal */
function PotentiometreFeedBack(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreFeedBack");

    // Indiquer le nom du potentiometre
    this.setNom("FeedBack");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreFeedBack.prototype = new Potentiometre();
PotentiometreFeedBack.prototype.setValueAudio = setValueAudioFeedBack;

/* Class PotentiometreTime */
/* Description : Class representant un potentiometre Time d'un pedal */
function PotentiometreTime(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreTime");

    // Indiquer le nom du potentiometre
    this.setNom("Time");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreTime.prototype = new Potentiometre();
PotentiometreTime.prototype.setValueAudio = setValueAudioTime;

/* Class PotentiometreDrive */
/* Description : Class representant un potentiometre Drive d'un pedal */
function PotentiometreDrive(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreDrive");

    // Indiquer le nom du potentiometre
    this.setNom("Drive");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreDrive.prototype = new Potentiometre();
PotentiometreDrive.prototype.setValueAudio = setValueAudioDrive;

/* Class PotentiometreBias */
/* Description : Class representant un potentiometre Bass d'un pedal */
function PotentiometreBias(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreBias");

    // Indiquer le nom du potentiometre
    this.setNom("Bias");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreBias.prototype = new Potentiometre();
PotentiometreBias.prototype.setValueAudio = setValueAudioBias;


/* Class PotentiometreBass */
/* Description : Class representant un potentiometre Bass d'un pedal */
function PotentiometreBass(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreBass");

    // Indiquer le nom du potentiometre
    this.setNom("Bass");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreBass.prototype = new Potentiometre();
PotentiometreBass.prototype.setValueAudio = setValueAudioBass;

/* Class PotentiometreMid */
/* Description : Class representant un potentiometre Mid d'un pedal */
function PotentiometreMid(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreMid");

    // Indiquer le nom du potentiometre
    this.setNom("Middle");

     // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }
    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreMid.prototype = new Potentiometre();
PotentiometreMid.prototype.setValueAudio = setValueAudioMid;

/* Class PotentiometreTreb */
/* Description : Class representant un potentiometre Treb d'un pedal */
function PotentiometreTreb(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreTreb");

    // Indiquer le nom du potentiometre
    this.setNom("Treble");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreTreb.prototype = new Potentiometre();
PotentiometreTreb.prototype.setValueAudio = setValueAudioTreb;

/* Class PotentiometreReverb */
/* Description : Class representant un potentiometre Treb d'un pedal */
function PotentiometreReverb(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreReverb");

    // Indiquer le nom du potentiometre
    this.setNom("Reverb");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreReverb.prototype = new Potentiometre();
PotentiometreReverb.prototype.setValueAudio = setValueAudioReverb;

/* Class PotentiometreProcess */
/* Description : Class representant un potentiometre Process d'un pedal */
function PotentiometreProcess(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreProcess");

    // Indiquer le nom du potentiometre
    this.setNom("Process");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreProcess.prototype = new Potentiometre();
PotentiometreProcess.prototype.setValueAudio = setValueAudioProcess;

/* Class PotentiometreEQ */
/* Description : Class representant un Filter pour une pedal d'effet*/
function PotentiometreEQ(params,freq){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreEQ");

    // Indiquer le nom du potentiometres
    this.setNom(freq);

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreEQ.prototype = new Potentiometre();
PotentiometreEQ.prototype.setValueAudio = setValueAudioEQ;

/* Class PotentiometreEQ */
/* Description : Class representant un Filter pour une pedal d'effet*/
function PotentiometreFreq(params,name){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreFreq");

    // Indiquer le nom du potentiometres
    this.setNom(name);

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreFreq.prototype = new Potentiometre();
PotentiometreFreq.prototype.setValueAudio = setValueAudioFreq;


/* Class PotentiometreHPFFreq */
/* Description : Class representant un potentiometre HPFFreq d'un pedal */
function PotentiometreHPFFreq(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreHPFFreq");

    // Indiquer le nom du potentiometre
    this.setNom("HPFFreq");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreHPFFreq.prototype = new Potentiometre();
PotentiometreHPFFreq.prototype.setValueAudio = setValueAudioHPFFreq;

/* Class PotentiometreHPFReso */
/* Description : Class representant un potentiometre HPFReso d'un pedal */
function PotentiometreHPFReso(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreHPFReso");

    // Indiquer le nom du potentiometre
    this.setNom("HPFReso");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreHPFReso.prototype = new Potentiometre();
PotentiometreHPFReso.prototype.setValueAudio = setValueAudioHPFReso;

/* Class PotentiometrePresence */
/* Description : Class representant un potentiometre Presence d'un pedal */
function PotentiometrePresence(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometrePresence");

    // Indiquer le nom du potentiometre
    this.setNom("Presence");

     // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometrePresence.prototype = new Potentiometre();
PotentiometrePresence.prototype.setValueAudio = setValueAudioPresence;

/* Class PotentiometreBoost */
/* Description : Class representant un potentiometre Boost d'un pedal */
/* Argument : valInit - la valeur initiale */
function PotentiometreBoost(valInit){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreBoost");

    // Verifier si l'argument est correct
    if(typeof(valInit) === 'undefined')
        valInit = false;

    // Indiquer le nom du potentiometre
    this.setNom("Boost");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreSwitch(valInit);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreBoost.prototype = new Potentiometre();
PotentiometreBoost.prototype.setValueAudio = setValueAudioBoost;

/* Class PotentiometreBoutonEtat */
/* Description : Class representant le bouton de l'etat de la pedale (on/off)*/
/* Argument : valInit - la valeur initiale */
function PotentiometreBoutonEtat (valInit){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreBoutonEtat");

    // Verifier si l'argument est correct
    if(typeof(valInit) === 'undefined')
        valInit = false;

    // Indiquer le nom du potentiometre
    this.setNom("Etat");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreSwitch(valInit);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreBoutonEtat.prototype = new Potentiometre();
PotentiometreBoutonEtat.prototype.setValueAudio = setValueAudioEtat;

/* Class PotentiometreMaster */
/* Description : Class representant un potentiometre Presence d'un pedal */
function PotentiometreMaster(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreMaster");

    // Verifier si l'argument est correct
    /*if(typeof(valInit) === 'undefined')
        valInit = 500;*/

    // Indiquer le nom du potentiometre
    this.setNom("Master");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreMaster.prototype = new Potentiometre();
PotentiometreMaster.prototype.setValueAudio = setValueAudioMaster;

/* Class PotentiometreFrequency */
/* Description : Class representant un potentiometre Frequency d'un pedal */
/* Argument : valInit - valeur initiale */
function PotentiometreFrequency(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreFrequency");

    // Verifier si l'argument est correct
    if(typeof(valInit) === 'undefined')
        valInit = 500;

    // Indiquer le nom du potentiometre
    this.setNom("Frequency");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreFrequency.prototype = new Potentiometre();
PotentiometreFrequency.prototype.setValueAudio = setValueAudioFrequency;

/* Class PotentiometreQ */
/* Description : Class representant un potentiometre Q d'un pedal */
function PotentiometreQ(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreQ");

    // Indiquer le nom du potentiometre
    this.setNom("Q");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreQ.prototype = new Potentiometre();
PotentiometreQ.prototype.setValueAudio = setValueAudioQ;



/* Class PotentiometreThreshold */
/* Description : Class representant un potentiometre Threshold d'un pedal */
function PotentiometreThreshold(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreThreshold");

    // Indiquer le nom du potentiometre
    this.setNom("Threshold");

    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreThreshold.prototype = new Potentiometre();
PotentiometreThreshold.prototype.setValueAudio = setValueAudioThreshold;

/* Class PotentiometreKnee */
/* Description : Class representant un potentiometre Knee d'un pedal */
function PotentiometreKnee(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreKnee");

    // Indiquer le nom du potentiometre
    this.setNom("Knee");

    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreKnee.prototype = new Potentiometre();
PotentiometreKnee.prototype.setValueAudio = setValueAudioKnee;

/* Class PotentiometreRatio */
/* Description : Class representant un potentiometre Ratio d'un pedal */
function PotentiometreRatio(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreRatio");

    // Indiquer le nom du potentiometre
    this.setNom("Ratio");

    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreRatio.prototype = new Potentiometre();
PotentiometreRatio.prototype.setValueAudio = setValueAudioRatio;

/* Class PotentiometreReduction */
/* Description : Class representant un potentiometre Reduction d'un pedal */
function PotentiometreReduction(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreReduction");

    // Indiquer le nom du potentiometre
    this.setNom("Reduction");

    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreReduction.prototype = new Potentiometre();
PotentiometreReduction.prototype.setValueAudio = setValueAudioReduction;

/* Class PotentiometreAttack */
/* Description : Class representant un potentiometre Attack d'un pedal */
function PotentiometreAttack(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreAttack");

    // Indiquer le nom du potentiometre
    this.setNom("Attack");

    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreAttack.prototype = new Potentiometre();
PotentiometreAttack.prototype.setValueAudio = setValueAudioAttack;

/* Class PotentiometreRelease */
/* Description : Class representant un potentiometre Release d'un pedal */
function PotentiometreRelease(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreRelease");

    // Indiquer le nom du potentiometre
    this.setNom("Release");

    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreRelease.prototype = new Potentiometre();
PotentiometreRelease.prototype.setValueAudio = setValueAudioRelease;

/* Class PotentiometreRate */
/* Description : Class representant un potentiometre Rate d'un pedal */
function PotentiometreRate(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreRate");

    // Indiquer le nom du potentiometre
    this.setNom("Rate");

    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreRate.prototype = new Potentiometre();
PotentiometreRate.prototype.setValueAudio = setValueAudioRate;


/* Class PotentiometreResonance */
/* Description : Class representant un potentiometre Resonance d'un pedal */
function PotentiometreResonance(){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreResonance");

    // Indiquer le nom du potentiometre
    this.setNom("Resonance");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreKnob(2, 7, 4, 0.01);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreResonance.prototype = new Potentiometre();
PotentiometreResonance.prototype.setValueAudio = setValueAudioResonance;

/* Class PotentiometreNum */
/* Description : Class representant un potentiometre dont le nom est un nombre d'un pedal */
function PotentiometreNum(nom){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreNum");

    // Indiquer le nom du potentiometre
    this.setNom(nom);

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreKnob(-40, 40, 0, 1);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreNum.prototype = new Potentiometre();
PotentiometreNum.prototype.setValueAudio = setValueAudioNum;

/* Class PotentiometrePitch */
/* Description : Class representant un potentiometre dont le nom est un nombre d'un pedal */
function PotentiometrePitch(){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometrePitch");

    // Indiquer le nom du potentiometre
    this.setNom("Pitch");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreKnob(0, 100, 0, 1);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometrePitch.prototype = new Potentiometre();
PotentiometrePitch.prototype.setValueAudio = setValueAudioPitch;

/* Class PotentiometreDepth */
/* Description : Class representant un potentiometre Depth d'un pedal */
function PotentiometreDepth(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreDepth");

    // Indiquer le nom du potentiometre
    this.setNom("Depth");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreDepth.prototype = new Potentiometre();
PotentiometreDepth.prototype.setValueAudio = setValueAudioDepth;

/* Class PotentiometreSpeed */
/* Description : Class representant un potentiometre Speed d'un pedal */
function PotentiometreSpeed(params){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreSpeed");

    // Indiquer le nom du potentiometre
    this.setNom("Speed");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g;
    try{
        g = new GPotentiometreKnob(params[0], params[1], params[2], params[3]);
    }
    catch(err){
        g = new GPotentiometreKnob();
    }

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreSpeed.prototype = new Potentiometre();
PotentiometreSpeed.prototype.setValueAudio = setValueAudioSpeed;

/* Class PotentiometreMode */
/* Description : Class representant un potentiometre Mode d'un pedal */
/* Argument : tab - tableu contenant les elements de la liste */
function PotentiometreMode(tab){

    // Heritage
    Potentiometre.call(this);
    this.setNomClass("PotentiometreMode");

    // Indiquer le nom du potentiometre
    this.setNom("Mode");

    // Creer le graphique associe (Instance fille de la classe GPotentiometre)
    var g = new GPotentiometreListe(tab);

    // Indiquer le graphique du potentiometre
    this.setGPotentiometre(g);

}
PotentiometreMode.prototype = new Potentiometre();
PotentiometreMode.prototype.setValueAudio = setValueAudioMode;

/* Methodes a ajouter aux classes Potentiometre */
/* ======================================== */

// --- Methode setValueAudioGain
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioGain(value) {
    /* TODO */
}

// --- Methode setValueAudioPan
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioPan(value) {
    /* TODO */
}

// --- Methode setValueAudioTone
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioTone(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setTone(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioVolume
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioVolume(value) {

    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setVolume(value);
    }
    catch(err){ console.log(err.toString()); }

}

// --- Methode setValueAudioType
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioType(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setType(value);
    }
    catch(err){ console.log(err.toString()); }

}


// --- Methode setValueAudioPreset
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioPreset(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setPreset(value);
    }
    catch(err){ 

        console.log(err.toString()); 
    }

}

// --- Methode setValueAudioMix
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioMix(value) {
    /* TODO */
}

// --- Methode setValueAudioFilter
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioFilter(value) {

    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setFilter(value);
    }
    catch(err){ console.log(err.toString()); }

}

// --- Methode setValueAudioEQ
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioEQ(value) {

    // Verifier que le parametre existe
    if(value === undefined)
        return;
    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{        
        audioPedal.setValueAudioEQ(value,this.getNom());
    }
    catch(err){ console.log(err.toString()); }

}


// --- Methode setValueAudioRoom
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioRoom(value) {
    /* TODO */
}

// --- Methode setValueAudioFeedBack
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioFeedBack(value) {

    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setFeedback(value);
    }
    catch(err){ console.log(err.toString()); }

}

// --- Methode setValueAudioTime
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioTime(value) {

    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setDelay(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioEtat
// --- Description : Methode permettant de modifier l'etat de la pedale
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioEtat(value) {

    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setEtat(value);
        saveStructure();
    }
    catch(err){ console.log(err.toString()); }

}

// --- Methode setValueAudioDrive
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioDrive(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setDrive(value);
    }
    catch(err){ console.log(err.toString()); }
}

function setValueAudioSpeed(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setSpeed(value);
    }
    catch(err){ console.log(err.toString()); }
}

function setValueAudioDepth(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setDepth(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioBass
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioBass(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setBass(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioMid
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioMid(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setMid(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioTreb
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioTreb(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setTreble(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioPresence
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioPresence(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setPresence(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioBoost
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioBoost(value) {
    /* TODO */
}

// --- Methode setValueAudioFreq
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioFreq(value) {
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setFreq(value,this.getNom());
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioHPFFreq
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioHPFFreq(value) {    
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setHPFFreq(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioHPFReso
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioHPFReso(value) {
    
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setHPFReso(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioProcess
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioProcess(value) {    
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setProcess(value);
    }
    catch(err){ console.log(err.toString()); }
}


// --- Methode setValueAudioBias
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioBias(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setBias(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioMaster
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioMaster(value) {
    
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setMaster(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioFrequency
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioFrequency(value) {

    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setFrequency(value);
    }
    catch(err){ console.log(err.toString()); }

}

// --- Methode setValueAudioQ
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioQ(value) {

    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setQ(value);
    }
    catch(err){ console.log(err.toString()); }

}

// --- Methode setValueAudioRelease
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioRelease(value) {    
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setRelease(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioThreshold
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioThreshold(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setThreshold(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioKnee
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioKnee(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setKnee(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioRatio
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioRatio(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setRatio(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioReduction
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioReduction(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setReduction(value);
    }
    catch(err){ console.log(err.toString()); }
}

// --- Methode setValueAudioAttack
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioAttack(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setAttack(value);
    }
    catch(err){ console.log(err.toString()); }
}


// --- Methode setValueAudioRate
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioRate(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setRate(value);
    }
    catch(err){ console.log(err.toString()); }
}


// --- Methode setValueAudioReverb
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioReverb(value) {
    // Verifier que le parametre existe
    if(value === undefined)
        return;

    // Verifier si l'on peut acceder au pedal
    var pedal = this.getAncrePedal();
    if(pedal === undefined)
        return;

    // Verifier si l'in peut acceder au context de l'audio
    var audioPedal = pedal.getAudioPedal();
    if(audioPedal === undefined)
        return;

    // Modifier la valeur
    try{
        audioPedal.setReverb(value);
    }
    catch(err){ console.log(err.toString()); }
}



// --- Methode setValueAudioResonance
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioResonance(value) {
    /* TODO */
}

// --- Methode setValueAudioNum
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioNum(value) {
    /* TODO */
}

// --- Methode setValueAudioPitch
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioPitch(value) {
    /* TODO */
}

// --- Methode setValueAudioMode
// --- Description : Methode permettant de modifier la valeur du potentiometre
// --- Argument : value - Valeur de l'effet audio du potentiometre
//
function setValueAudioMode(value) {
    /* TODO */
}

/* Classes filles de la classe GPotentiometre */
/* ====================================== */

/* Class GPotentiometreKnob */
/* Description : Class representant le graphique issu du plugin Knob */
/* Arguments : valMin - valeur minimal de l'intervalle
 valMax - valeur maximale de l'intervalle
 valInit - la valeur initiale
 step - valeur de saut lors d'un changement */
function GPotentiometreKnob(valMin, valMax, valInit, step){

    // Appeler la classe mere (heritage)
    GPotentiometre.call(this);
    this.setNomClass("GPotentiometreKnob");

    // Creer un element Knob
    var graphique = "<input class='knob gPotentiometre' data-angleOffset=-125 data-angleArc=250 data-rotation=clockwise data-linecap=round>";

    // Indiquer que le type de valeurs est un interval
    var typeValeurs = "Intervalle";

    // Verifier si les parametres sont valides
    if(valMin === undefined)
        valMin = 0;
    if(valMax === undefined)
        valMax = 1;
    if(valMax <= valMin)
        valMax = -1*(valMin-valMax)*10;
    if(valInit === undefined)
        valInit = valMin;
    if(step === undefined)
        step = (valMax-valMin)/10;

    // Ajouter les potentiometres a l'instance (par heritage)
    this.setTypeValeurs(typeValeurs);
    this.setGraphique(graphique);
    this.ajouterValeur("data-min", valMin);
    this.ajouterValeur("data-max", valMax);
    this.ajouterValeur("data-step", step);
    this.ajouterValeur("value", valInit);

    this.ajouterValeur("data-height", 75);
    this.ajouterValeur("data-width", 75);

    // --- Surcharge de la methode getGraphique (pour recuperer l'element graphique)
    //
    this.getGraphique = function(){

        // Recuperer la description html du graphique
        var graphique = $(this.m_graphique);

        // Renseigner l'id du graphique
        graphique.attr('id', this.getId());

        // Recuperer les valeurs du graphique
        var vals = this.getValeurs();

        // Ajouter les valeurs du graphique
        for(var i=0; i<vals.length; i++)
            graphique.attr(vals[i][0], vals[i][1]);

        // Renvoyer le graphique
        return graphique.get()[0];
    };


}
GPotentiometreKnob.prototype = new GPotentiometre();
GPotentiometreKnob.prototype.getValeurTraite = getValeurTraiteKnob;
GPotentiometreKnob.prototype.setValeurTraite = setValeurTraiteKnob;

/* Class GPotentiometreListe */
/* Description : Class representant le graphique d'une liste*/
/* Arguments : tab - tableau contenant la liste des valeurs*/
function GPotentiometreListe(tab){

    // Appeler la classe mere (heritage)
    GPotentiometre.call(this);
    this.setNomClass("GPotentiometreListe");

    // Creer un element select (list)
    var graphique = "<select class='potentiometreList gPotentiometre'></select>";

    // Entrer les valeurs du tableau donne
    if(tab !== undefined){
        for(var i=0; i<tab.length; i++){

            // Indiquer que le 1er de la liste est selectionnee
            if(i === 0)
                this.ajouterValeur("value", tab[i]);

            // Ajouter la valeur dans l'objet GPotentiometreListe
            this.ajouterValeur("option", tab[i]);
        }
    }

    // Indiquer que le type de valeurs est un interval
    var typeValeurs = "Liste";

    // Ajouter les potentiometres a l'instance (par heritage)
    this.setTypeValeurs(typeValeurs);
    this.setGraphique(graphique);

    // --- Surcharge de la methode getGraphique (pour recuperer l'element graphique)
    //
    this.getGraphique = function(){

        // Recuperer la description html du graphique
        var graphique = $(this.m_graphique);

        // Renseigner l'id du graphique
        graphique.attr('id', this.getId());

        // Recuperer les valeurs du graphique
        var vals = this.getValeurs();

        // Trouver l'option selected
        var selected;
        for(var j=0; j<vals.length; j++)
            if(vals[j][0] == "value")
                selected = vals[j][1];

        // Ajouter les valeurs du graphique
        for(var i=0; i<vals.length; i++){

            // Ajouter les potentiometres (si c'est le cas de la valeur actuelle)
            if(vals[i][0] === "option")
            // Si l'option doit etre selectionnee
                if(vals[i][1] == selected && selected !== undefined)
                    $("<option>" + vals[i][1] +"</option>").attr("selected", "selected").appendTo(graphique);
                else
                    $("<option>" + vals[i][1] +"</option>").appendTo(graphique);
            else
            // Ajouter les autres attributs
            if(vals[i][0] != "value")
                graphique.attr(vals[i][0], vals[i][1]);
        }

        // Renvoyer le graphique
        return graphique.get()[0];
    };

}
GPotentiometreListe.prototype = new GPotentiometre();
GPotentiometreListe.prototype.getValeurTraite = getValeurTraiteListe;
GPotentiometreListe.prototype.setValeurTraite = setValeurTraiteListe;

/* Class GPotentiometreSwitch */
/* Description : Class representant le graphique d'un switch */
/* Argument : valInit - la valeur initiale */
function GPotentiometreSwitch(valInit){

    // Appeler la classe mere (heritage)
    GPotentiometre.call(this);
    this.setNomClass("GPotentiometreSwitch");

    // Verifier si l'argument est correct
    if(typeof(valInit) === 'undefined')
        valInit = false;

    // Creer un element switch
    var graphique = "<div class='switch'> <input class='cmn-toggle cmn-toggle-round-flat gPotentiometre' type='checkbox'> <label></label> </div>";

    // Indiquer que le type de valeurs est un interval
    var typeValeurs = "Switch";

    // Ajouter les potentiometres a l'instance (par heritage)
    this.setTypeValeurs(typeValeurs);
    this.setGraphique(graphique);
    this.ajouterValeur("value", valInit);

    // --- Surcharge de la methode getGraphique (pour recuperer l'element graphique)
    //
    this.getGraphique = function(){

        // Recuperer la description html du graphique
        var graphique = $(this.m_graphique);

        // Renseigner l'id du graphique
        graphique.find('input').attr('id', this.getId());
        graphique.find('label').attr('for', this.getId());

        // Recuperer les valeurs du graphique
        var vals = this.getValeurs();

        // Ajouter les valeurs du graphique
        for(var i=0; i<vals.length; i++){
            // Verifier si c'est la valeur choisie
            if(vals[i][0] == 'value')
                graphique.find('input').attr('checked', vals[i][1]);
            // Sinon
            else
                graphique.find('input').attr(vals[i][0], vals[i][1]);
        }

        // Renvoyer le graphique
        return graphique.get()[0];
    };

}
GPotentiometreSwitch.prototype = new GPotentiometre();
GPotentiometreSwitch.prototype.getValeurTraite = getValeurTraiteSwitch;
GPotentiometreSwitch.prototype.setValeurTraite = setValeurTraiteSwitch;


/* Methodes a ajouter aux classes GPotentiometre */
/* ========================================= */

// --- Methode getValeurTraiteKnob
//
function getValeurTraiteKnob() {

    // Recuperer la valeur ('value') du graphique
    var val = $(this.getGraphique()).val();

    // Renvoyer la valeur
    return val;
}

// --- Methode setValeurTraiteKnob
//
function setValeurTraiteKnob(val){

    // Modifier la valeur
    this.retirerValeur('value');
    this.ajouterValeur('value', val);

    // Verifier si l'on peut acceder au potentiometre
    var potentiometre = this.getAncrePotentiometre();
    if(potentiometre === undefined)
        return;

    // Modifier la valeur de l'audio
    potentiometre.setValueAudio(val);

}

// --- Methode getValeurTraiteListe
//
function getValeurTraiteListe() {

    // Recuperer la valeur ('value') du graphique
    var val = $(this.getGraphique()).val();

    // Renvoyer la valeur
    return val;
}

// --- Methode setValeurTraiteKnob
//
function setValeurTraiteListe(val){

    // Modifier la valeur
    this.retirerValeur('value');
    this.ajouterValeur('value', val);

    // Verifier si l'on peut acceder au potentiometre
    var potentiometre = this.getAncrePotentiometre();
    if(potentiometre === undefined)
        return;

    // Modifier la valeur de l'audio
    potentiometre.setValueAudio(val);
}

// --- Methode getValeurTraiteSwitch
//
function getValeurTraiteSwitch() {

    // Recuperer la valeur ('value') du graphique
    var val = $(this.getGraphique()).children('#cmn-toggle-4').val();

    // Renvoyer la valeur
    return val;
}

// --- Methode setValeurTraiteSwitch
//
function setValeurTraiteSwitch(val){

    // Modifier la valeur
    this.retirerValeur('value');
    this.ajouterValeur('value', val);

    // Verifier si l'on peut acceder au potentiometre
    var potentiometre = this.getAncrePotentiometre();
    if(potentiometre === undefined)
        return;

    // Modifier la valeur de l'audio
    potentiometre.setValueAudio(val);
}
