
/* Class CategoriePresets */
/* Description : Class regroupant une categorie contenant des presets */
/* Argument : id- id de la categorie */
function CategoriePresets(id){

    // Implementer Serialize
    Serialize.call(this,"CategoriePresets");

    // --- Attributs
    //
    this.m_id = id;//undefined;	// L'id (unique) d'un preset
    this.m_nom = "New bank, click to rename ->";	// Le nom de la categorie (sera affiche)

    this.m_presets = [];	// Tableau contenant les instances des presets
    this.m_presetsImportes = [];	// Tableau contenant un couple : id de la categorie + id du preset de la categorie

    // --- Methodes
    //

    // --- Methode getId
    //
    this.getId = function(){
        return(this.m_id);
    };

    // --- Methode getNom
    //
    this.getNom = function(){
        return(this.m_nom);
    };

    // --- Methode setNom
    //
    this.setNom = function(nom){
        this.m_nom = nom;
    };

    // --- Methode getPresets
    //
    this.getPresets = function(){
        return(this.m_presets);
    };

    // --- Methode getPresetsImportes
    //
    this.getPresetsImportes = function(){
        return(this.m_presetsImportes);
    };

    // --- Methode getPresetById
    // --- Retour : Instance de preset
    //
    this.getPresetById = function(id){

        // Verifier si l'argument existe
        if(id !== undefined){

            //Trouver le preset
            var preset = -1;
            for(var i=0; i< this.m_presets.length; i++){

                // Verifier si l'id correspond
                if(this.m_presets[i].getId() == id)
                    preset = this.m_presets[i];
            }

            // Renvoyer le resultat
            return preset;
        }
    };

    // --- Methode setPresets
    //
    // this.setPresets = function(TabPresets){
    // 	this.m_presets = TabPresets;
    // };

    // --- Methode creerPreset
    // --- Retour : Instance de preset
    //
    this.creerPreset = function(){

        // Recuperer le tableau
        var tab = this.m_presets;

        // Recuperer un id non existant et minimal
        var id = getIdLibre(tab);

        // Creer un preset a id nouveau si id non trouve
        preset = new Preset(id, this.getId());

        // Ajouter le preset dans la liste
        tab.push(preset);

        // Renvoyer le preset
        return preset;

    };

    // --- Methode supprimerPreset
    //
    this.supprimerPresetById = function(idPreset){

        // Chercher le preset par son id
        for(var i=0; i<this.m_presets.length; i++){

            // Verifier que le preset est le bon
            if(this.m_presets[i].getId() == idPreset)
            // Supprimer le preset
                this.m_presets.splice(i, 1);

        }

    };

    // --- Methode ajouterPresetImporte
    //
    this.ajouterPresetImporte = function(idCategorie, idPreset){

        // Ajouter l'association ([0] pour l'id de la categorie, [1] pour l'id du preset)
        this.m_presetsImportes.push([idCategorie,idPreset]);
    };

    // --- Methode retirerPresetImporte
    //
    this.retirerPresetImporte = function(idCategorie, idPreset){

        // Chercher l'id categorie
        var idCat, val;
        for(var i=0; i<this.m_presetsImportes.length; i++){

            // Recuperer la idCat
            idCat = this.m_presetsImportes[i][0];

            // Verifier si la idCat correspond
            if(idCat === idCategorie){

                // Verifier si on a enseigne l'idPreset en potentiometre
                if(idPreset !== undefined){

                    // Recuperer l'idPreset
                    val = this.m_presetsImportes[i][1];

                    // Verifier si l'idPreset correspond
                    if(val === idPreset)
                    // Supprimer l'association
                        this.m_presetsImportes.splice(i, 1);

                }
                else
                // Supprimer l'association
                    this.m_presetsImportes.splice(i, 1);

            }

        }

    };

}
CategoriePresets.prototype = new Serialize();