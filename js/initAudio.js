

var audio = window.AudioContext || window.webkitAudioContext;
// Initialiser AudioContext API (pour traitements de l'audio)
var audioCtx;

function convertToMono(input) {
    var splitter = audioCtx.createChannelSplitter(2);
    var merger = audioCtx.createChannelMerger(2);

    input.connect(splitter);
    splitter.connect(merger, 0, 0);
    splitter.connect(merger, 0, 1);
    return merger;
}



// --- Fonction initialiseAudioContext
// --- Description : Fonction permettant d'initialiser les flux d'audio par AudioContext API et UserAudio API
//
function initialiseAudioContext(){

    // Initialiser l'Audio Context
    audioCtx = new audio();

    var constraints = {
        audio: {
            mandatory: { echoCancellation: false }
        }
    };

    // Recuperer le micro
    if(!navigator.getUserMedia)
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

    // Initialiser UserMedia API
    if(navigator.getUserMedia)
        navigator.getUserMedia(constraints, successUserMedia, failUserMedia);

}

// --- Fonction successUserMedia
// --- Description : Fonction permettant de traiter le succes de la recuperation du flux audio entrant
// --- Argument : stream - flux audio partant du micro
//
function successUserMedia(stream){

    // Recuperer le flux partant de l'input
    streamEntrant = stream;

    // Lier la source a AudioContext
    input = audioCtx.createMediaStreamSource(streamEntrant);
    output = (audioCtx.destination);

    // Connecter le flux vers le haut parleur
    //input.connect(output);

    // Reconnecter la structure audio interne du preset apres acceptation de l'audio context
    if(presetCourant !== undefined)
        presetCourant.connecterAudioPreset();

}

// --- Fonction failUserMedia
// --- Description : Fonction permettant de traiter l'echeque de la recuperation du micro
//
function failUserMedia(){
    //contenu.innerHTML = "Votre navigateur ne supporte pas l'API User Media";
    console.log("problem getting access to the microphone");
}

//document.querySelector("#testMicro").addEventListener("click", function(){});
