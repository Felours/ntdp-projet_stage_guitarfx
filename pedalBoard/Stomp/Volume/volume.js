/**
author : M.D
date : 23/03/2015
**/


var Volume = (function () {
    // methode de classe
    Volume.construire = function (audio_context,name,num,id) {
        return new Volume(audio_context,name,num,id);
    };
 
    // constructeur
    function Volume(audio_context,name,num,id) {
        this.id=id;
    	this.num =num;
        this.name = name;
    	this.input = audio_context.createGain();
        this.input.gain.value=1;
        this.output = audio_context.createGain();
        this.output.gain.value=1;
        //this.output=output;
        this.etat = false;
        this.volume = audio_context.createGain();
        this.constructModel();
		this.constructModelInput("Volume",0.1,2,0.1,1);
        this.constructModelButton();
        this.constructDeleteButton();
        this.input.connect(this.output); 
    }
 
    // méthodes d'instance
    Volume.prototype = {

        effectConnect : function(){   
            if(this.etat===true){ 
				this.input.disconnect(this.output);
                this.input.connect(this.volume);
                this.volume.connect(this.output); 
            }else{
                this.input.disconnect(this.volume);
                this.volume.disconnect(this.output); 
                this.input.connect(this.output);
            }
        },
 
        changeEtat: function () {
            if(this.etat===true){
                this.etat=false;
            }else{
                this.etat=true;
            }         
            this.effectConnect();
        },

        constructModel: function(){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'Pedale'+this.name+this.id);
            $('#Pedale'+this.name+this.id).append("<b>"+this.num+". "+this.name+"</b>");
        },

        constructModelInput: function (name,valueMin,valueMax,valueStep,valueInit){
			$('#Pedale'+this.name+this.id).append("<form onsubmit='return false' oninput='level" + name + ".value =" + name + ".value'><label for='" + name + "'>" + name + "</label><input name='i_" + name + "' id='" + name + "' type='range' min=" + valueMin + " max=" + valueMax + " value=" + valueInit + " step=" + valueStep + " onchange='pb.listEffet["+this.num+"].change" + name + "(this.value)' ><output for='i_" + name + "' name='level" + name + "'>" + valueInit + "</output></form>");
		},

        constructModelButton: function(){
            $('#Pedale'+this.name+this.id).append("<input id='pedaleBouton"+this.name+"' type='button' value='on/off' onclick='pb.listEffet["+this.num+"].changeEtat()'/>");
        },

        constructDeleteButton: function(){
            $('#Pedale'+this.name+this.id).append("<input id='deleteBouton' type='button' value='delete' onclick='pb.removePedale("+this.num+")'/>");
        },

        changeVolume: function(value) {
            if (!this.volume) return;
            this.volume.gain.value = value;
        },

        getInput: function(){
            return this.input;
        },

        getOutput: function(){
            return this.output;
        },

        setNum: function(newNum){            
            var balise = document.getElementById('Pedale'+this.name+this.id);
            var div = balise.innerHTML;
            div = div.replace(this.num+". "+this.name,newNum+". "+this.name);
            div = div.replace(new RegExp('[['+this.num+']]', 'gi'),newNum+']');
            div = div.replace('pb.removePedale('+this.num+')','pb.removePedale('+newNum+')');
            div = div.replace('pb.switchLeft('+this.num+')','pb.switchLeft('+newNum+')');
            div = div.replace('pb.switchRight('+this.num+')','pb.switchRight('+newNum+')');
            balise.innerHTML = div;
            this.num = newNum;  
        },

         removeEffect: function(){
            if(this.etat===false){ 
                this.input.disconnect(this.output);
            }else{
                this.input.disconnect(this.volume);
                this.volume.disconnect(this.output); 
            }
            var obj = document.body;
            var old = document.getElementById('Pedale'+this.name+this.id);
            obj.removeChild(old);
        },

    };

    return Volume;
 
}());