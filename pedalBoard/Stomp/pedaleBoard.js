/**
author : M.D
date : 23/03/2015
**/


var PedaleBoard = (function () {

    //Constructeur

    PedaleBoard.construire = function (input,output,audio_context) {
        return new PedaleBoard(input,output,audio_context);
    };

    function PedaleBoard(input,output,audio_context) { 

        this.num =0;
        this.id=0;
        this.input = input;
        this.output = output;
        this.audio_context = audio_context;
        this.listEffet = new Array();
        this.createBoutonAdd();  
        this.input.connect(this.output);     
    };

 
    // méthodes d'instance
    PedaleBoard.prototype = {

        addPedale : function(name){  
            var pedale;
            switch(name){
                case "Delay":
                    pedale = new Delay.construire(this.audio_context,'Delay',this.num,this.id);
                    this.createSwitch('Delay');
                    break;
                case "Distorsion":
                    pedale = new Distorsion.construire(this.audio_context,'Distorsion',this.num,this.id);
                    this.createSwitch('Distorsion');
                    break;
                case "Reverb":
                    pedale = new Reverb.construire(this.audio_context,'Reverb',this.num,this.id);
                    this.createSwitch('Reverb');
                    break;
                case "Volume":
                    pedale = new Volume.construire(this.audio_context,'Volume',this.num,this.id);
                    this.createSwitch('Volume');
                    break;
                case "AutoWah":
                    pedale = new AutoWah.construire(this.audio_context,'AutoWah',this.num,this.id);
                    this.createSwitch('AutoWah');
                    break;
                case "Analyser":
                    pedale = new Analyser.construire(this.audio_context,'Analyser',this.num,this.id);
                    this.createSwitch('Analyser');
                    break;
                default : 
                    alert("Sorry this effect does not exist");
                    break;
            }
            if(pedale != null){
                if(this.num>0){                            
                    this.listEffet[this.listEffet.length-1].getOutput().disconnect(this.output);
                }
                this.listEffet.push(pedale);
                if(this.listEffet.length ===1){
                    this.input.disconnect(this.output);
                    this.input.connect(this.listEffet[this.num].getInput());
                }else{
                    this.listEffet[this.num-1].getOutput().connect(this.listEffet[this.num].getInput());
                }
                this.listEffet[this.listEffet.length-1].getOutput().connect(this.output);
                this.num = this.num + 1;
                this.id +=1;
            }
        },

        createSwitch : function(name){
            $('#Pedale'+name+this.id).append("<input id='moveL' type='button' value='<-' onclick='pb.switchLeft("+this.num+")'/>");
            $('#Pedale'+name+this.id).append("<input id='moveR' type='button' value='->' onclick='pb.switchRight("+this.num+")'/>");
        },

        removePedale : function(numEffet){            
            this.listEffet[numEffet].removeEffect();
            if(this.listEffet[numEffet+1]!=null && this.listEffet[numEffet-1]!=null){   
                this.listEffet[numEffet-1].getOutput().disconnect(this.listEffet[numEffet].getInput());  
                this.listEffet[numEffet].getOutput().disconnect(this.listEffet[numEffet+1].getInput());             
                this.listEffet[numEffet-1].getOutput().connect(this.listEffet[numEffet+1].getInput());
            }
            else if(numEffet === this.listEffet.length-1){
                this.listEffet[numEffet].getOutput().disconnect(this.output);
                if(this.listEffet.length-1!=0){
                    this.listEffet[numEffet-1].getOutput().disconnect(this.listEffet[numEffet].getInput());
                    this.listEffet[numEffet-1].getOutput().connect(this.output);
                }else{       
                    this.input.disconnect(this.listEffet[numEffet].getInput());             
                    this.input.connect(this.output);
                }
            }
            else if(numEffet === 0){                
                this.input.disconnect(this.listEffet[numEffet].getInput());
                if(this.listEffet[numEffet+1]!=null){ 
                    this.listEffet[numEffet].getOutput().disconnect(this.listEffet[numEffet+1].getInput()); 
                    this.input.connect(this.listEffet[numEffet+1].getInput());
                }else{   
                    this.listEffet[numEffet].getOutput().disconnect(this.output);          
                    this.input.connect(this.output);
                }
            }
            this.listEffet.splice(numEffet,1);  
            this.num = this.num - 1;
            for(i=0;i<this.listEffet.length;i++){
                this.listEffet[i].setNum(i);
            }
        },

        switchRight : function(numEffet){
            if(numEffet === this.listEffet.length-1){
                alert('Impossible de faire ce déplacement vers la droite');
            }else{
                for(i=0;i<this.listEffet.length;i++){
                    if(i===0){                    
                        this.input.disconnect(this.listEffet[i].getInput());
                    }else if(i === this.listEffet.length-1){  
                        this.listEffet[i-1].getOutput().disconnect(this.listEffet[i].getInput());                  
                        this.listEffet[i].getOutput().disconnect(this.output);
                    }else{                    
                        this.listEffet[i-1].getOutput().disconnect(this.listEffet[i].getInput());
                    }
                }    
                $('#Pedale'+this.listEffet[numEffet].name+this.listEffet[numEffet].id).insertAfter('#Pedale'+this.listEffet[numEffet+1].name+this.listEffet[numEffet+1].id);        
                var tmp = this.listEffet[numEffet];
                this.listEffet[numEffet] = this.listEffet[numEffet+1];
                this.listEffet[numEffet+1] = tmp;
                for(i=0;i<this.listEffet.length;i++){
                    if(i===0){                    
                        this.input.connect(this.listEffet[i].getInput());
                    }else if(i === this.listEffet.length-1){           
                        this.listEffet[i-1].getOutput().connect(this.listEffet[i].getInput());         
                        this.listEffet[i].getOutput().connect(this.output);
                    }else{                    
                        this.listEffet[i-1].getOutput().connect(this.listEffet[i].getInput());
                    }
                    this.listEffet[i].setNum(i);
                }
            }
        },

        switchLeft : function(numEffet){ 
            if(numEffet === 0){
                alert('Impossible de faire ce déplacement vers la gauche');
            }else{
                 for(i=0;i<this.listEffet.length;i++){
                    if(i===0){                    
                        this.input.disconnect(this.listEffet[i].getInput());
                    }else if(i === this.listEffet.length-1){  
                        this.listEffet[i-1].getOutput().disconnect(this.listEffet[i].getInput());                  
                        this.listEffet[i].getOutput().disconnect(this.output);
                    }else{                    
                        this.listEffet[i-1].getOutput().disconnect(this.listEffet[i].getInput());
                    }
                }  
                $('#Pedale'+this.listEffet[numEffet].name+this.listEffet[numEffet].id).insertBefore('#Pedale'+this.listEffet[numEffet-1].name+this.listEffet[numEffet-1].id);                  
                var tmp = this.listEffet[numEffet];
                this.listEffet[numEffet] = this.listEffet[numEffet-1];
                this.listEffet[numEffet-1] = tmp;   
                for(i=0;i<this.listEffet.length;i++){
                    if(i===0){                    
                        this.input.connect(this.listEffet[i].getInput());
                    }else if(i === this.listEffet.length-1){           
                        this.listEffet[i-1].getOutput().connect(this.listEffet[i].getInput());         
                        this.listEffet[i].getOutput().connect(this.output);
                    }else{                    
                        this.listEffet[i-1].getOutput().connect(this.listEffet[i].getInput());
                    }
                    this.listEffet[i].setNum(i);
                }
            }       
        },

        selectEffect : function(){
            var effect = prompt("Please enter your effect name", "Distorsion");
            if (effect != null) {
                this.addPedale(effect);
            }
        },

        setEffect : function(numEffet,nomEffet){
            listEffet[numEffet].setTypeEffet(nomEffet);
        },

        createBoutonAdd : function(){ 
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'controlsAdd');  
            $('#controlsAdd').append("<input id='BoutonAdd' type='button' value='add Effect' onclick='pb.selectEffect()'/>");
        },

    };

    return PedaleBoard;
 
}());

