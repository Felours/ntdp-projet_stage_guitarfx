/**
author : M.D
**/

var Pedale = (function () {
    // methode de classe
    Pedale.construire = function (input,output,audio_context,test,num) {
        return new Pedale(input,output,audio_context,test,num);
    };
 
    // constructeur
    function Pedale(input,output,audio_context,test,num) {
        this.num=num;
        this.input = input;
        this.output = output;
        this.audio_context = audio_context;
        this.test = test;
        if(this.test==='Delay'){            
            this.delay = audio_context.createDelay();
        }else{            
            this.volume = audio_context.createGain();
        }
        this.constructModelImg();
    }
 
    // méthodes d'instance
    Pedale.prototype = {

        effectConnect : function(){   
            if(this.etat===true){                   
                this.input.disconnect(this.output);
                if(this.test==='Delay'){
                    this.input.connect(this.delay);
                    this.delay.connect(this.output);
                }else{
                    this.input.connect(this.volume);
                    this.volume.connect(this.output);
                }
                //Pedale.input.connect(Pedale.output);
            }else{
                if(this.test==='Delay'){
                    this.input.disconnect(this.delay);
                    this.delay.disconnect(this.output);
                }else{
                    this.input.disconnect(this.volume);
                    this.volume.disconnect(this.output);
                }
                this.input.connect(this.output);
                //Pedale.input.disconnect(Pedale.output);
            }
        },
 
        changeEtat: function () {
            if(this.etat===true){
                this.etat=false;
            }else{
                this.etat=true;
            }          
            this.effectConnect();
        },

        constructModelImg: function(){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'Pedale'+this.test);
            $('#Pedale'+this.test).append("<input id='pedaleBouton"+this.test+"' type='button' value='on/off' onclick='PedaleBoard.listEffet["+this.num+"].changeEtat()'/>");
        },

    };

    return Pedale;
 
}());