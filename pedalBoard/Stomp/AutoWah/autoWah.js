/**
author : M.D
date : 23/03/2015
**/


var AutoWah = (function () {
    // methode de classe
    AutoWah.construire = function (audio_context,name,num,id) {
        return new AutoWah(audio_context,name,num,id);
    };
 
    // constructeur
    function AutoWah(audio_context,name,num,id) {

        this.id=id;
    	this.num =num;
        this.name = name;

        this.input = audio_context.createGain();
        this.input.gain.value=1;
        this.output = audio_context.createGain();
        this.output.gain.value=1;

        this.etat = false;

        this.waveshaper = audio_context.createWaveShaper();
        this.awFollower = audio_context.createBiquadFilter();
        this.awFollower.type = "lowpass";
        this.awFollower.frequency.value = 10.0;

        this.curve = new Float32Array(65536);
        for (var i=-32768; i<32768; i++)
            this.curve[i+32768] = ((i>0)?i:-i)/32768;
        this.waveshaper.curve = this.curve;

        this.awDepth = audio_context.createGain();
        this.awDepth.gain.value = 11585;

        this.awFilter = audio_context.createBiquadFilter();
        this.awFilter.type = "lowpass";
        this.awFilter.Q.value = 15;
        this.awFilter.frequency.value = 50;

        this.constructModel();

        this.constructModelInput("Frequency",0.25,20,0.05,0.25);
        this.constructModelInput("Volume",1024,16384,10,1024)
		this.constructModelInput("Q",0,20,0.1,1);

        this.constructModelButton();
        this.constructDeleteButton();
        this.input.connect(this.output);
        
    }
 
    // méthodes d'instance
    AutoWah.prototype = {

        effectConnect : function(){   
            if(this.etat===true){ 
				this.input.disconnect(this.output);
			    this.input.connect(this.waveshaper);       
                this.waveshaper.connect(this.awFollower);   
                this.awFollower.connect(this.awDepth);
                this.awDepth.connect(this.awFilter.frequency);                 
                this.input.connect(this.awFilter);     
                this.awFilter.connect(this.output);
            }else{
                this.input.disconnect(this.waveshaper);                 
                this.input.disconnect(this.awFilter);               
                this.waveshaper.disconnect(this.awFollower);
                this.awFollower.disconnect(this.awDepth);
                this.awDepth.disconnect(this.awFilter.frequency);
                this.awFilter.disconnect(this.output);
                this.input.connect(this.output);
            }
        },
 
        changeEtat: function () {
            if(this.etat===true){
                this.etat=false;
            }else{
                this.etat=true;
            }         
            this.effectConnect();
        },

        constructModel: function(){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'Pedale'+this.name+this.id);
            $('#Pedale'+this.name+this.id).append("<b>"+this.num+". "+this.name+"</b>");
        },

        constructModelInput: function (name,valueMin,valueMax,valueStep,valueInit){
			$('#Pedale'+this.name+this.id).append("<form onsubmit='return false' oninput='level" + name + ".value =" + name + ".value'><label for='" + name + "'>" + name + "</label><input name='i_" + name + "' id='" + name + "' type='range' min=" + valueMin + " max=" + valueMax + " value=" + valueInit + " step=" + valueStep + " onchange='pb.listEffet["+this.num+"].change" + name + "(this.value)' ><output for='i_" + name + "' name='level" + name + "'>" + valueInit + "</output></form>");
		},

        constructModelButton: function(){
            $('#Pedale'+this.name+this.id).append("<input id='pedaleBouton"+this.name+"' type='button' value='on/off' onclick='pb.listEffet["+this.num+"].changeEtat()'/>");
        },

        constructDeleteButton: function(){
            $('#Pedale'+this.name+this.id).append("<input id='deleteBouton' type='button' value='delete' onclick='pb.removePedale("+this.num+")'/>");
        },

        changeQ: function(value) {
            if (!this.awFilter) return;            
                this.awFilter.Q.value = value;
        },

        changeFrequency: function(value) {
            if (!this.awFollower) return;
                this.awFollower.frequency.value=value;
        },

        changeVolume: function(value) {
            if (!this.volume) return;
                this.volume.gain.value = value;
        },

        getInput: function(){
            return this.input;
        },

        getOutput: function(){
            return this.output;
        },

        setNum: function(newNum){            
            var balise = document.getElementById('Pedale'+this.name+this.id);
            var div = balise.innerHTML;
            div = div.replace(this.num+". "+this.name,newNum+". "+this.name);
            div = div.replace(new RegExp('[['+this.num+']]', 'gi'),newNum+']');
            div = div.replace('pb.removePedale('+this.num+')','pb.removePedale('+newNum+')');
            div = div.replace('pb.switchLeft('+this.num+')','pb.switchLeft('+newNum+')');
            div = div.replace('pb.switchRight('+this.num+')','pb.switchRight('+newNum+')');
            balise.innerHTML = div;
            this.num = newNum;  
        },

         removeEffect: function(){
            if(this.etat===false){ 
                this.input.disconnect(this.output);
            }else{
                this.input.disconnect(this.waveshaper);                 
                this.input.disconnect(this.awFilter);               
                this.waveshaper.disconnect(this.awFollower);
                this.awFollower.disconnect(this.awDepth);
                this.awDepth.disconnect(this.awFilter.frequency);
                this.awFilter.disconnect(this.output);
            }
            var obj = document.body;
            var old = document.getElementById('Pedale'+this.name+this.id);
            obj.removeChild(old);
        },

    };

    return AutoWah;
 
}());