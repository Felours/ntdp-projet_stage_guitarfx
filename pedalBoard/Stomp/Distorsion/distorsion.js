/**
author : M.D
date : 23/03/2015
**/


var Distorsion = (function () {
    // methode de classe
    Distorsion.construire = function (audio_context,name,num,id) {
        return new Distorsion(audio_context,name,num,id);
    };
 
    // constructeur
    function Distorsion(audio_context,name,num,id) {
    	
        this.id=id;
        this.num =num;
        this.name = name;

        this.input = audio_context.createGain();
        this.input.gain.value=1;
        this.output = audio_context.createGain();
        this.output.gain.value=1;

        this.etat = false;

        this.filter = audio_context.createBiquadFilter();
        this.shaper = audio_context.createWaveShaper();
        this.gain = audio_context.createGain();
        this.constructModel();
		this.constructModelInput("Tone",2000,5000,100,2000);        
		this.constructModelInput("Drive",0,20,1,0); 
		this.constructModelInput("Gain",0.1,1,0.1,0.1);

        this.constructModelButton();
        this.constructDeleteButton();
        this.input.connect(this.output);
        
    }
 
    // méthodes d'instance
    Distorsion.prototype = {

        effectConnect : function(){   
            if(this.etat===true){ 
				this.input.disconnect(this.output);
			    this.input.connect(this.shaper);
                this.shaper.connect(this.filter);
                this.filter.connect(this.gain);
                this.gain.connect(this.output);    
            }else{
                this.input.disconnect(this.shaper);
                this.shaper.disconnect(this.filter);
                this.filter.disconnect(this.gain);
                this.gain.disconnect(this.output); 
                this.input.connect(this.output);
            }
        },
 
        changeEtat: function () {
            if(this.etat===true){
                this.etat=false;
            }else{
                this.etat=true;
            }         
            this.effectConnect();
        },

        constructModel: function(){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'Pedale'+this.name+this.id);
            $('#Pedale'+this.name+this.id).append("<b>"+this.num+". "+this.name+"</b>");
        },

        constructModelInput: function (name,valueMin,valueMax,valueStep,valueInit){
			$('#Pedale'+this.name+this.id).append("<form onsubmit='return false' oninput='level" + name + ".value =" + name + ".value'><label for='" + name + "'>" + name + "</label><input name='i_" + name + "' id='" + name + "' type='range' min=" + valueMin + " max=" + valueMax + " value=" + valueInit + " step=" + valueStep + " onchange='pb.listEffet["+this.num+"].change" + name + "(this.value)' ><output for='i_" + name + "' name='level" + name + "'>" + valueInit + "</output></form>");
		},

        constructModelButton: function(){
            $('#Pedale'+this.name+this.id).append("<input id='pedaleBouton"+this.name+"' type='button' value='on/off' onclick='pb.listEffet["+this.num+"].changeEtat()'/>");
        },

        constructDeleteButton: function(){
            $('#Pedale'+this.name+this.id).append("<input id='deleteBouton' type='button' value='delete' onclick='pb.removePedale("+this.num+")'/>");
        },

        changeTone: function(value) {
            if (!this.filter) return;
            this.filter.frequency.value = value;
        },

        changeDrive: function(value) {
            if (!this.shaper) return;
            this.shaper.curve = this.makeDistortionCurve(value);
            this.shaper.oversample = '4x';
        },

        changeGain: function(value) {
            if (!this.gain) return;
            this.gain.gain.value = value;
        },

        getInput: function(){
            return this.input;
        },

        getOutput: function(){
            return this.output;
        },


        setNum: function(newNum){            
            var balise = document.getElementById('Pedale'+this.name+this.id);
            var div = balise.innerHTML;
            div = div.replace(this.num+". "+this.name,newNum+". "+this.name);
            div = div.replace(new RegExp('[['+this.num+']]', 'gi'),newNum+']');
            div = div.replace('pb.removePedale('+this.num+')','pb.removePedale('+newNum+')');
            div = div.replace('pb.switchLeft('+this.num+')','pb.switchLeft('+newNum+')');
            div = div.replace('pb.switchRight('+this.num+')','pb.switchRight('+newNum+')');
            balise.innerHTML = div;
            this.num = newNum;  
        },

        makeDistortionCurve: function(amount) {
            var k = amount;
            var n_samples = 65536; //22050;     //44100
            curve = new Float32Array(n_samples);
            var deg = Math.PI / 180;
            for (var i = 0; i < n_samples; i += 1) {
                var x = i * 2 / n_samples - 1;
                curve[i] = (3 + k) * x * 20 * deg / (Math.PI + k * Math.abs(x));
            }
            return curve;
        },

        removeEffect: function(){ 
             if(this.etat===false){ 
                this.input.disconnect(this.output);   
            }else{
                this.input.disconnect(this.shaper);
                this.shaper.disconnect(this.filter);
                this.filter.disconnect(this.gain);
                this.gain.disconnect(this.output); 
            }
            var obj = document.body;
            var old = document.getElementById('Pedale'+this.name+this.id);
            obj.removeChild(old);
        },
    };

    return Distorsion;
 
}());