/**
author : M.D
date : 23/03/2015
**/


var Delay = (function () {
    // methode de classe
    Delay.construire = function (audio_context,name,num,id) {
        return new Delay(audio_context,name,num,id);
    };
 
    // constructeur
    function Delay(audio_context,name,num,id) {
        this.id=id;
    	this.num =num;
        this.name = name;
    	this.etat=false;
    	this.lastEffect=true;

        this.input = audio_context.createGain();
        this.input.gain.value=1;
        this.output = audio_context.createGain();
        this.output.gain.value=1;

	    this.volume = audio_context.createGain();
		this.delay = audio_context.createDelay();
		this.feedbackGain = audio_context.createGain();
		this.feedbackGain.gain.value=0.1;
		this.filter = audio_context.createBiquadFilter();
		this.constructModel();
		this.constructModelInput("Volume",0.1,1,0.1,0.1);
		this.constructModelInput("Delay",0,1,0.005,0);
		this.constructModelInput("Feedback",0.1,0.9,0.01,0.1);
		this.constructModelInput("Filter",0,1000,100,0);
        this.constructModelButton();
        this.constructDeleteButton();
        this.input.connect(this.output);
        
    }
 
    // méthodes d'instance
    Delay.prototype = {

        effectConnect : function(){   
            if(this.etat===true){ 
				this.input.disconnect(this.output);
			    this.input.connect(this.volume);
			    this.delay.connect(this.feedbackGain);
                this.feedbackGain.connect(this.delay);
			    this.volume.connect(this.delay);
			    this.volume.connect(this.filter);
                this.delay.connect(this.filter);
                this.filter.connect(this.output);     
            }else{
				this.input.disconnect(this.volume);
			    this.delay.disconnect(this.feedbackGain);
			    this.feedbackGain.disconnect(this.delay);
			    this.filter.disconnect(this.output);
			    this.volume.disconnect(this.delay);
			    this.volume.disconnect(this.filter);
			    this.delay.disconnect(this.filter);
				this.input.connect(this.output);
            }
        },
 
        changeEtat: function () {
            if(this.etat===true){
                this.etat=false;
            }else{
                this.etat=true;
            } 
            this.effectConnect();
        },

        constructModel: function(){
        	var $div = $('<div />').appendTo('body');
            $div.attr('id', 'Pedale'+this.name+this.id);
            $('#Pedale'+this.name+this.id).append("<b>"+this.num+". "+this.name+"</b>");
        },

        constructModelInput: function (name,valueMin,valueMax,valueStep,valueInit){            
			$('#Pedale'+this.name+this.id).append("<form onsubmit='return false' oninput='level" + name + ".value =" + name + ".value'><label for='" + name + "'>" + name + "</label><input name='i_" + name + "' id='" + name + "' type='range' min=" + valueMin + " max=" + valueMax + " value=" + valueInit + " step=" + valueStep + " onchange='pb.listEffet["+this.num+"].change" + name + "(this.value)' ><output for='i_" + name + "' name='level" + name + "'>" + valueInit + "</output></form>");
		},

		constructModelButton: function(){
            $('#Pedale'+this.name+this.id).append("<input id='pedaleBouton"+this.name+"' type='button' value='on/off' onclick='pb.listEffet["+this.num+"].changeEtat()'/>");
        },

        constructDeleteButton: function(){
            $('#Pedale'+this.name+this.id).append("<input id='deleteBouton' type='button' value='delete' onclick='pb.removePedale("+this.num+")'/>");
        },

        changeVolume: function(value) {
		    if (!this.volume) return;
		    this.volume.gain.value = value;
		},

		changeDelay: function(value) {
		    if (!this.delay) return;
			this.delay.delayTime.value = value;
		},

		changeFeedback: function(value) {
		    if (!this.feedbackGain) return;
		    this.feedbackGain.gain.value = value;
		},

		changeFilter: function(value) {
		    if (!this.filter) return;
			this.filter.frequency.value = value;
		},

        getInput: function(){
            return this.input;
        },

        getOutput: function(){
        	return this.output;
        },


        setNum: function(newNum){            
            var balise = document.getElementById('Pedale'+this.name+this.id);
            var div = balise.innerHTML;
            div = div.replace(this.num+". "+this.name,newNum+". "+this.name);
            div = div.replace(new RegExp('[['+this.num+']]', 'gi'),newNum+']');
            div = div.replace('pb.removePedale('+this.num+')','pb.removePedale('+newNum+')');
            div = div.replace('pb.switchLeft('+this.num+')','pb.switchLeft('+newNum+')');
            div = div.replace('pb.switchRight('+this.num+')','pb.switchRight('+newNum+')');
            balise.innerHTML = div;
            this.num = newNum;  
        },

         removeEffect: function(){
            if(this.etat===false){ 
                this.input.disconnect(this.output);   
            }else{
                this.input.disconnect(this.volume);
                this.delay.disconnect(this.feedbackGain);
                this.feedbackGain.disconnect(this.delay);
                this.filter.disconnect(this.output);
                this.volume.disconnect(this.delay);
                this.volume.disconnect(this.filter);
                this.delay.disconnect(this.filter);
            }
            var obj = document.body;
            var old = document.getElementById('Pedale'+this.name+this.id);
            obj.removeChild(old);
        },
    };

    return Delay;
 
}());