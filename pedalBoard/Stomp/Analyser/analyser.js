/**
author : M.D
date : 23/03/2015
**/


var Analyser = (function () {
    // methode de classe
    Analyser.construire = function (audio_context,name,num,id) {
        return new Analyser(audio_context,name,num,id);
    };
 
    // constructeur
    function Analyser(audio_context,name,num,id) {
        this.id=id;
        this.num =num;
        this.name = name;
        this.input = audio_context.createGain();
        this.input.gain.value=1;
        this.output = audio_context.createGain();
        this.output.gain.value=1;

        this.analyser = audio_context.createAnalyser();
        this.constructModel();
        this.constructModelCanvas();
        this.constructDeleteButton();

        this.WIDTH = 100;
        this.HEIGHT = 40;
        this.effectConnect();
        this.drawVisual;
        this.draw();
    }
 
    // méthodes d'instance
    Analyser.prototype = {

        effectConnect : function(){   
            this.input.connect(this.analyser);
            this.analyser.connect(this.output);
        },

        draw: function() {

                var obj = this;

                obj.canvas = document.getElementById('canvas'+obj.name+obj.num);
                obj.canvasCtx = obj.canvas.getContext('2d');

                obj.bufferLength = obj.analyser.frequencyBinCount;
                obj.dataArray = new Uint8Array(obj.bufferLength);

                // On efface le contenu en dessinant un gros rectangle gris
                obj.canvasCtx.fillStyle = 'rgb(200, 200, 200)';
                obj.canvasCtx.fillRect(0, 0, obj.WIDTH, obj.HEIGHT);
                
                // remplissage du tableau de frequences
                obj.analyser.getByteTimeDomainData(obj.dataArray);
               
                // On dessine la forme d'onde

                obj.canvasCtx.lineWidth = 2;
                obj.canvasCtx.strokeStyle = 'rgb(0, 0, 0)';
                obj.canvasCtx.beginPath();
                var sliceWidth = obj.WIDTH * 1.0 / obj.bufferLength;
                var x = 0;
                for(var i = 0; i < obj.bufferLength; i++) {
           
                    var v = obj.dataArray[i] / 30.0;
                    var y = v * obj.HEIGHT/8;

                    if(i === 0) {
                      obj.canvasCtx.moveTo(x, y);
                    } else {
                      obj.canvasCtx.lineTo(x, y);
                    }

                    x += sliceWidth;
                }
                obj.canvasCtx.lineTo(obj.canvas.width, obj.canvas.height/2);
                obj.canvasCtx.stroke();
                obj.drawLine = requestAnimationFrame(function() { obj.draw(); }) || webkitRequestAnimationFrame(function() { obj.draw(); });
            },

        constructModel: function(){
            var $div = $('<div />').appendTo('body');
            $div.attr('id', 'Pedale'+this.name+this.id);
            $('#Pedale'+this.name+this.id).append("<b>"+this.num+". "+this.name+"</b>");
        },

        constructModelCanvas: function(){
            $('#Pedale'+this.name+this.id).append("</br><canvas id='canvas"+this.name+this.num+"' width='100' height='40' ></canvas></br>");
        },

        constructDeleteButton: function(){
            $('#Pedale'+this.name+this.id).append("<input id='deleteBouton' type='button' value='delete' onclick='pb.removePedale("+this.num+")'/>");
        },

        getInput: function(){
            return this.input;
        },

        getOutput: function(){
            return this.output;
        },

        setNum: function(newNum){        
            var balise = document.getElementById('Pedale'+this.name+this.id);
            var div = balise.innerHTML;
            div = div.replace(this.num+". "+this.name,newNum+". "+this.name);
            div = div.replace(this.num+". "+this.name,newNum+". "+this.name);
            div = div.replace("canvas"+this.name+this.num, "canvas"+this.name+newNum);
            div = div.replace('pb.removePedale('+this.num+')','pb.removePedale('+newNum+')');
            div = div.replace('pb.switchLeft('+this.num+')','pb.switchLeft('+newNum+')');
            div = div.replace('pb.switchRight('+this.num+')','pb.switchRight('+newNum+')');
            balise.innerHTML = div;
            this.num = newNum;  
            this.draw();
        },

         removeEffect: function(){          
            this.input.disconnect(this.analyser);
            this.input.disconnect(this.output);
            var obj = document.body;
            var old = document.getElementById('Pedale'+this.name+this.id);
            obj.removeChild(old);
        },

    };

    return Analyser;
 
}());